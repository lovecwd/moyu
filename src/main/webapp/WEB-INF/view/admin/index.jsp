<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../common/header.jsp"></jsp:include>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/bootstrap.min.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/head.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/main.css"/>
<script type="text/javascript" src="${ctx }/resources/js/echarts.js"></script>
<title>魔鱼后台主页</title>
<style type="text/css">
#main{
	padding-top:3%;
    margin: 0 auto;
}
#weatherMsg{
	width:47%;
	margin: 0 auto;
	padding-top:3%;
}
.musicPlay{
	margin:0 auto;
	width: 90%;
	margin-top: 10px;
}
audio{
	width: 100%;
}
</style>
</head>
<body>
	<!-- <div class="head">
		<div class="headDivIn">
			<span class="headTitleFont">新版魔鱼后台</span>
		</div>
	</div> -->
	<div class="all">
		<div class="main">
			<div class="iconDiv"><div class="imageDetail"><img src="${ctx }/resources/images/logo.png"></div></div>
			<%-- <ul class="list-group">
			    <li class="list-group-item"><a href="${ctx }/admin/material/categoryList" >素材类别列表</a></li>
			    <li class="list-group-item"><a href="${ctx }/admin/material/update">素材更新</a></li>
			    <li class="list-group-item"><a href="${ctx }/admin/material/list">素材相关数据统计</a></li>
			    <li class="list-group-item">
			      <a href="${ctx }/admin/filter/list">滤镜相关数据统计</a>
			    </li>
			    <li class="list-group-item"><a href="${ctx }/admin/client/list">用户行为相关数据统计</a></li>
			    <li class="list-group-item"><a href="${ctx }/admin/client/shareData">用户分享数据统计</a></li>
			</ul> --%>
			<div class="ulContent">
				<jsp:include page="../common/menu.jsp"></jsp:include>
			</div>
			<div class="musicPlay">
				<audio id="music1" controls="controls" src="http://ws.stream.qqmusic.qq.com/105575471.m4a?fromtag=46" 
				 loop="loop">
				Your browser does not support the audio tag.
				</audio>
			</div>
		</div>
		<div class="body">
			<jsp:include page="../common/bodyHead.jsp"></jsp:include>
			<div style="text-align: center;">
				<h1>欢迎来到魔鱼后台v1.0.1</h1>
			</div>
			<div id="weatherMsg">
			
			</div>
			<div id="main" style="width: 600px;height:400px;"></div>
		</div>
	</div>
	<script type="text/javascript">
		$(function(){
			$("#music1").paused;
			
		});
	</script>
	<script type="text/javascript">
        // 基于准备好的dom，初始化echarts实例
        /*var myChart = echarts.init(document.getElementById('main'));
        var categoryData = new Array();
        $.ajax({
			url:"${ctx }/weather/query",
			type:"get",
			async:true,
			success:function (weatherData) {
				var result = weatherData.result;

				console.log(result);
				var lifeInfo = result.life.info;
				var realtime = result.realtime;
				var weatherInfo = result.weather;
				var fiveDayMsg = result.weather;
				var dayTem = new Array();
				var nightTem = new Array();
				$("#weatherMsg").html("今天日期："+ result.life.date+"  农历："+result.realtime.moon+"<br>穿衣建议："+lifeInfo.chuanyi[0]+","+lifeInfo.chuanyi[1]+
						"<br>感冒建议："+lifeInfo.ganmao[0]+","+lifeInfo.ganmao[1]+
						"<br>空调建议："+lifeInfo.kongtiao[0]+","+lifeInfo.kongtiao[1]+
						"<br>运动建议："+lifeInfo.yundong[0]+","+lifeInfo.yundong[1]+
						"<br>紫外线建议："+lifeInfo.ziwaixian[0]+","+lifeInfo.ziwaixian[1]+
						"<br>风："+realtime.wind.direct+realtime.wind.power+
						"<br>天气情况："+realtime.weather.info+" 温度是"+realtime.weather.temperature+"度");
				for(var i=0;i<fiveDayMsg.length;i++){
					categoryData[i]='周'+fiveDayMsg[i].week;
					dayTem[i] = fiveDayMsg[i].info.day[2];
					nightTem[i] = fiveDayMsg[i].info.night[2];
				}
				myChart.setOption({
					title: {
						text: '苏州未来五天气温变化',
						subtext: 'msg from 阿凡达云数据平台'
					},
					tooltip: {
						trigger: 'axis'
					},
					legend: {
						data:['白天气温','晚上气温']
					},
					toolbox: {
						show: true,
						feature: {
							dataZoom: {
								yAxisIndex: 'none'
							},
							dataView: {readOnly: false},
							magicType: {type: ['line', 'bar']},
							restore: {},
							saveAsImage: {}
						}
					},
					xAxis:  {
						type: 'category',
						boundaryGap: false,
						data:  categoryData//['周一','周二','周三','周四','周五']
					},
					yAxis: {
						type: 'value',
						axisLabel: {
							formatter: '{value} °C'
						}
					},
					series: [
						{
							name:'白天气温',
							type:'line',
							data:dayTem,//[11, 11, 15, 13, 12],
							markPoint: {
								data: [
									{type: 'max', name: '最大值'},
									{type: 'min', name: '最小值'}
								]
							},
							markLine: {
								data: [
									{type: 'average', name: '平均值'}
								]
							}
						},
						{
							name:'晚上气温',//'最低气温',
							type:'line',
							data:nightTem,//[1, -2, 2, 5, 3],
							markPoint: {
								data: [
									{name: '周最低', value: -2, xAxis: 1, yAxis: -1.5}
								]
							},
							markLine: {
								data: [
									{type: 'average', name: '平均值'},
									[{
										symbol: 'none',
										x: '90%',
										yAxis: 'max'
									}, {
										symbol: 'circle',
										label: {
											normal: {
												position: 'start',
												formatter: '最大值'
											}
										},
										type: 'max',
										name: '最高点'
									}]
								]
							}
						}
					]
				});
			}

		});*/

		/*$.get("${ctx }/weather/query").done(function (weatherData) {
            var result = weatherData.result;
            
        	console.log(result);
        	var lifeInfo = result.life.info;
        	var realtime = result.realtime;
        	var weatherInfo = result.weather;
        	var fiveDayMsg = result.weather;
        	var dayTem = new Array();
        	var nightTem = new Array();
        	$("#weatherMsg").html("今天日期："+ result.life.date+"  农历："+result.realtime.moon+"<br>穿衣建议："+lifeInfo.chuanyi[0]+","+lifeInfo.chuanyi[1]+
        			"<br>感冒建议："+lifeInfo.ganmao[0]+","+lifeInfo.ganmao[1]+
        			"<br>空调建议："+lifeInfo.kongtiao[0]+","+lifeInfo.kongtiao[1]+
        			"<br>运动建议："+lifeInfo.yundong[0]+","+lifeInfo.yundong[1]+
        			"<br>紫外线建议："+lifeInfo.ziwaixian[0]+","+lifeInfo.ziwaixian[1]+
        			"<br>风："+realtime.wind.direct+realtime.wind.power+
        			"<br>天气情况："+realtime.weather.info+" 温度是"+realtime.weather.temperature+"度");
        	for(var i=0;i<fiveDayMsg.length;i++){
        		categoryData[i]='周'+fiveDayMsg[i].week;
        		dayTem[i] = fiveDayMsg[i].info.day[2];
        		nightTem[i] = fiveDayMsg[i].info.night[2];
        	}
        	myChart.setOption({
        	    title: {
        	        text: '苏州未来五天气温变化',
        	        subtext: 'msg from 阿凡达云数据平台'
        	    },
        	    tooltip: {
        	        trigger: 'axis'
        	    },
        	    legend: {
        	        data:['白天气温','晚上气温']
        	    },
        	    toolbox: {
        	        show: true,
        	        feature: {
        	            dataZoom: {
        	                yAxisIndex: 'none'
        	            },
        	            dataView: {readOnly: false},
        	            magicType: {type: ['line', 'bar']},
        	            restore: {},
        	            saveAsImage: {}
        	        }
        	    },
        	    xAxis:  {
        	        type: 'category',
        	        boundaryGap: false,
        	        data:  categoryData//['周一','周二','周三','周四','周五']
        	    },
        	    yAxis: {
        	        type: 'value',
        	        axisLabel: {
        	            formatter: '{value} °C'
        	        }
        	    },
        	    series: [
        	        {
        	            name:'白天气温',
        	            type:'line',
        	            data:dayTem,//[11, 11, 15, 13, 12],
        	            markPoint: {
        	                data: [
        	                    {type: 'max', name: '最大值'},
        	                    {type: 'min', name: '最小值'}
        	                ]
        	            },
        	            markLine: {
        	                data: [
        	                    {type: 'average', name: '平均值'}
        	                ]
        	            }
        	        },
        	        {
        	            name:'晚上气温',//'最低气温',
        	            type:'line',
        	            data:nightTem,//[1, -2, 2, 5, 3],
        	            markPoint: {
        	                data: [
        	                    {name: '周最低', value: -2, xAxis: 1, yAxis: -1.5}
        	                ]
        	            },
        	            markLine: {
        	                data: [
        	                    {type: 'average', name: '平均值'},
        	                    [{
        	                        symbol: 'none',
        	                        x: '90%',
        	                        yAxis: 'max'
        	                    }, {
        	                        symbol: 'circle',
        	                        label: {
        	                            normal: {
        	                                position: 'start',
        	                                formatter: '最大值'
        	                            }
        	                        },
        	                        type: 'max',
        	                        name: '最高点'
        	                    }]
        	                ]
        	            }
        	        }
        	    ]
        	}); 
        });*/
        
        // 使用刚指定的配置项和数据显示图表。
        //myChart.setOption(option);
    </script>
</body>
</html>