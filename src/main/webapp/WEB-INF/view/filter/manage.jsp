<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>滤镜的管理</title>
<jsp:include page="../common/header.jsp"></jsp:include>
<script type="text/javascript" src="${ctx }/resources/js/WdatePicker.js"></script>
<style type="text/css">
*{margin:0;padding:0;}
.all{
	float:left;
	width:100%;
}
/*.menu{
	float:left;
	width:326px;
	margin-left: 200px;
	margin-top:70px;
}
 .body{
	float:left;
	width:800px;
	margin-left: 100px;
	margin-top:72px;
} */
</style>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/body.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/main.css"/>
</head>
<body>
<%-- <jsp:include page="../common/head.jsp"></jsp:include> --%>
	<div class="all">
		<div class="main">
			<div class="iconDiv">
				<div class="imageDetail"><img src="${ctx }/resources/images/logo.png"></div>
			</div>
			<div class="ulContent">
				<jsp:include page="../common/menu.jsp"></jsp:include>
			</div>
		</div>
	<div class="body">
	<jsp:include page="../common/bodyHead.jsp"></jsp:include>
	
	<div class="formDiv">
	<a class="btn btn-default" href="${ctx }/admin/filter/add">添加滤镜</a>
	<form action="${ctx }/admin/filter/list" method="post">
		<div class="form-group">
			<label for="firstname" class="col-sm-2 control-label">设备版本信息</label>
			<div class="col-sm-4">
				<select class="form-control" name="versionType">
								<option <c:if test="${versionType == 'ios' }">selected</c:if> value="ios">IOS系统</option>
								<option <c:if test="${versionType == 'android' }">selected</c:if> value="android">安卓系统</option>
				</select>
			</div>
		</div>
		<div class="form-group">
				<label for="firstname" class="col-sm-2 control-label">APP版本号</label>
				 <div class="col-sm-4">
				<select class="form-control" name="versionNumber">
						<option value="">请选择</option>
						<option <c:if test="${versionNumber == '1.0.1' }">selected</c:if> value="1.0.1">1.0.1版本</option>
						<option <c:if test="${versionNumber == '1.0.0' }">selected</c:if> value="1.0.0">1.0.0版本</option>
				</select>
				</div>
		</div>
		<%--<div class="form-group">
			从 <input name="from" class="Wdate" type="text" onClick="WdatePicker()">
			到<input name="to" class="Wdate" type="text" onClick="WdatePicker()">
			<input type="submit" class="btn btn-default" value="查询"/>
		</div>--%>
		
	</form>
	</div>
	
	<div class="tableDiv">
	<c:if test="${! empty filters }">
	<table class="table table-bordered">
			<tr>
				<td>滤镜名称</td>
				<td>设备类型</td>
				<td>APP版本号</td>
				<td>操作</td>
			</tr>
		
		<c:forEach items="${filters }" var="filter">
		<tr>
			<td>${filter.filterName }</td>
			<td>${filter.versionType }</td>
			<td>${filter.versionNumber }</td>
			<td></td>
		</tr>
		</c:forEach>	
	</table>
	</div>
</c:if>
</div>
</div>
</body>
</html>