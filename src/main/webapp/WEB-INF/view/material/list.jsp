<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>素材数据统计</title>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/WdatePicker.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/bootstrap.min.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/datepicker.css"/>
<link rel="shortcut icon" href="${ctx }/resources/images/favicon.png">
<script type="text/javascript" src="${ctx }/resources/js/WdatePicker.js"></script>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/main.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/body.css"/>
<script type="text/javascript" src="${ctx }/resources/js/jquery-3.1.0.min.js"></script>
<style type="text/css">
*{margin:0;padding:0;}
.all{
	float:left;
	width:100%;
}
.table{
width:50%;}
/* .menu{
	float:left;
	width:326px;
	margin-left: 200px;
	margin-top:70px;
} */
/* .body{
	float:left;
	width:800px;
	margin-left: 100px;
	margin-top:72px;
} */
</style>
</head>
<body>
<%-- <jsp:include page="../common/head.jsp"></jsp:include> --%>
<div class="all">
		<%-- <div class="menu">
			<jsp:include page="../common/menu.jsp"></jsp:include>
		</div> --%>
		<div class="main">
			<div class="iconDiv">
				<div class="imageDetail"><img src="${ctx }/resources/images/logo.png"></div>
			</div>
			<div class="ulContent">
				<jsp:include page="../common/menu.jsp"></jsp:include>
			</div>
		</div>
	<div class="body">
	<%-- <div class="bodyHead">
		<div class="logoutDiv pull-right">
			<a class="btn btn-primary" href="${ctx }/logout">用户登出</a>
		</div>
	</div> --%>
	<jsp:include page="../common/bodyHead.jsp"></jsp:include>
	<div class="formDiv">
		<form id="materialForm" action="${ctx }/admin/material/list" class="form-horizontal" method="post">
			<div class="form-group">
				 <label for="firstname" class="col-sm-2 control-label">素材统计信息</label>
				 <div class="col-sm-4">
					<select class="form-control" name="materialRecords">
						<option <c:if test="${action == 'download' }">selected</c:if> value="download">素材的下载量</option>
						<option <c:if test="${action == 'photograph' }">selected</c:if> value="photograph">素材拍照数量</option>
						<option <c:if test="${action == 'picShare' }">selected</c:if> value="picShare">素材图片的分享量</option>
						<option <c:if test="${action == 'picDownload' }">selected</c:if> value="picDownload">素材图片的下载本地量</option>
						<option <c:if test="${action == 'video' }">selected</c:if> value="video">素材录像次数</option>
						<option <c:if test="${action == 'videoShare' }">selected</c:if> value="videoShare">素材的视频的分享量</option>
						<option <c:if test="${action == 'videoDownload' }">selected</c:if> value="videoDownload">素材录像后下载本地的次数</option>
					</select>
				</div>
				
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">设备版本信息</label>
				 <div class="col-sm-4">
				<select class="form-control" name="versionType">
								<option <c:if test="${versionType == 'ios' }">selected</c:if> value="ios">IOS系统</option>
								<option <c:if test="${versionType == 'android' }">selected</c:if> value="android">安卓系统</option>
				</select>
				</div>
			</div>
			
			<div class="form-group">
				<label for="firstname" class="col-sm-2 control-label">APP版本号</label>
				<div class="col-sm-4">
					<input class="form-control" name="versionNumber"/>
				</div>
				<%--<label for="firstname" class="col-sm-2 control-label">APP版本号</label>
				 <div class="col-sm-4">
				<select class="form-control" name="versionNumber">
						<option value="">请选择</option>
						<c:forEach items="${versionNumbers }" var="versionNumber1">
							<option <c:if test="${versionNumber == versionNumber1 }">selected</c:if> value="${ versionNumber1}">${ versionNumber1}版本</option>
						</c:forEach>
						&lt;%&ndash; <option <c:if test="${versionNumber == '1.1.1' }">selected</c:if> value="1.1.1">1.1.1版本</option>
						<option <c:if test="${versionNumber == '1.1.0' }">selected</c:if> value="1.1.0">1.1.0版本</option>
						<option <c:if test="${versionNumber == '1.0.1' }">selected</c:if> value="1.0.1">1.0.1版本</option>
						<option <c:if test="${versionNumber == '1.0.0' }">selected</c:if> value="1.0.0">1.0.0版本</option> &ndash;%&gt;
				</select>
				</div>--%>
			</div>

			<div class="form-group">
				<label for="firstname" class="col-sm-2 control-label">数量升降序</label>
				<div class="col-sm-4">
					<select class="form-control" name="order">
						<option value="">请选择</option>
						<option <c:if test="${order == 'asc' }">selected</c:if> value="asc">升序</option>
						<option <c:if test="${order == 'desc' }">selected</c:if> value="desc">降序</option>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
				从 <input name="from" class="Wdate " type="text" value="${from }" onClick="WdatePicker()">
				</div>
				<div class="col-sm-2">
				到<input name="to" class="Wdate" type="text" value="${to }" onClick="WdatePicker()">
				</div>
				<div class="col-sm-2">
				<input id="queryBtn" type="button" class="btn btn-default" value="查询"/>
				</div>
				<div class="col-sm-2">
				<input id="exportBtn" type="button" class="btn btn-default" value="导出"/>
				</div>
			</div>
		</form>
	</div>


	<div class="tableDiv">
	<c:if test="${! empty materialRecords }">
	<table class="table table-bordered">
			<tr>
				<td>素材uuid</td>
				<td>设备类型</td>
				<td>APP版本号</td>
				<td>数量</td>
			</tr>
		
		<c:forEach items="${materialRecords }" var="count">
		<tr>
			<td>${count[0] }</td>
			<td>${count[1] }</td>
			<td>${count[2] }</td>
			<td>${count[3] }</td>
		</tr>
		</c:forEach>	
	</table>
	</c:if>
	<%--<c:if test="${! empty shareMapData }">
	<table class="table table-bordered">
			<tr>
				<td>素材uuid</td>
				<td>设备类型</td>
				<td>APP版本号</td>
				<td>总数量</td>
				<td>微信渠道的数量</td>
				<td>QQ渠道的数量</td>
				<td>新浪微博渠道的数量</td>
				<td>微信朋友圈渠道的数量</td>
			</tr>
		
		&lt;%&ndash; <c:forEach items="${shareMapData }" var="count"> &ndash;%&gt;
		
			&lt;%&ndash; <td rowspan="${fn:length(count.value)}">${count.key }</td> &ndash;%&gt;
			<c:forEach items="${shareMapData }" var="record">
			<tr >
				<td>${record.uuid }</td>
				<td>${record.versionType }</td>
				<td>${record.versionNum }</td>
				<td>${record.totalNum }</td>
				<td>${record.weiXinNum }</td>
				<td>${record.qqNum }</td>
				<td>${record.weiBoNum }</td>
				<td>${record.pyqNum }</td>
				&lt;%&ndash;<td>${record.key }</td>
				<td>${record.value.versionType }</td>
				<td>${record.value.versionNum }</td>
				<td>${record.value.totalNum }</td>
				<td>${record.value.weiXinNum }</td>
				<td>${record.value.qqNum }</td>
				<td>${record.value.weiBoNum }</td>
				<td>${record.value.pyqNum }</td>&ndash;%&gt;
			</tr>
			</c:forEach>

		
		&lt;%&ndash; </c:forEach>	 &ndash;%&gt;
	</table>
	</c:if>--%>
	</div>

</div>
</div>
<script type="text/javascript">
$(function(){
	$(".main").css("height",$(document).height()+"px");
	$("#exportBtn").click(function(){
		$("#materialForm").attr("action","${ctx }/admin/material/exportExcel");
		$("#materialForm").submit();
	});
	
	$("#queryBtn").click(function(){
		$("#materialForm").attr("action","${ctx }/admin/material/list");
		$("#materialForm").submit();
	});
});
</script>
</body>
</html>