<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>素材更新管理</title>
<script type="text/javascript" src="${ctx}/resources/js/jquery-3.1.0.min.js"></script>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/bootstrap.min.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/main.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/body.css"/>
<script type="text/javascript" src="${ctx }/resources/js/jquery-3.1.0.min.js"></script>
<style type="text/css">
*{margin:0;padding:0;}
#updateForm{
	
}
 .formDiv{
 float:left;
	margin-left: 32px;
	margin-top:50px;
}
#submitBtn{
	margin-left: 28px;
}
.tableDiv{
	margin-left: 32px;
	margin-top: 22px;
}
table{
	background-color: #fff;
	border-radius:17px;
	table-layout: fixed;
}
td {
    word-break:break-all; word-wrap:break-word;
    color:#434d63;
}
/* td:HOVER {
	overflow: visible;
	width: auto;
} */
.headTr > td{
	color:#b1bcc0;
}
input{
	border-radius:10px;
	height:35px;
}
.lastUpdateMsg{
	float:left;
	margin-left: 32px;
	margin-top: 22px;
}
</style>
</head>
<body>
<%-- <jsp:include page="../common/head.jsp"></jsp:include> --%>
	<div class="all">
		<div class="main">
			<div class="iconDiv">
				<div class="imageDetail"><img src="${ctx }/resources/images/logo.png"></div>
			</div>
			<div class="ulContent">
				<jsp:include page="../common/menu.jsp"></jsp:include>
			</div>
		</div>
	<div class="body">
	<jsp:include page="../common/bodyHead.jsp"></jsp:include>
		<div class="formDiv">
		<form action="${ctx }/admin/material/add" class="form-horizontal" role="form" method="post">
			 <div class="form-group">
			 	<label for="lastname" class="col-sm-2 control-label">APP版本类型</label>
			    <div class="col-sm-2">
			      <input name="versionType" type="text" class="form-control" id="lastname" placeholder="APP版本类型"/>
			    </div>
			    <label for="lastname" class="col-sm-2 control-label">APP版本号</label>
			    <div class="col-sm-2">
			      <input name="versionNumber" type="text" class="form-control" id="lastname" placeholder="APP版本号"/>
			    </div>
			    <label for="lastname" class="col-sm-2 control-label">素材的性别取向</label>
			    <div class="col-sm-2">
			      <select name="gender" class="form-control" id="lastname">
			      	<option value="1">男</</option>
			      	<option value="0">女</option>
			      </select>
			    </div>
			 </div>
			 <div class="form-group">
			    <label for="firstname" class="col-sm-1 control-label">素材uuid</label>
			    <div class="col-sm-1">
			      <input name="uuid"  class="form-control" id="firstname" placeholder="素材uuid"/>
			    </div>
			    <label for="lastname" class="col-sm-1 control-label">图标文件名</label>
			    <div class="col-sm-1">
			      <input name="iconName" type="text" class="form-control" id="lastname" placeholder="图标文件名"/>
			    </div>
			    <label for="lastname" class="col-sm-1 control-label">是否是推荐素材</label>
			    <div class="col-sm-1">
			      <select  name="isRecommend" class="form-control" id="lastname">
			      	<option value="y"  >是</option>
			      	<option value="n"  >不是</option>
			      </select>
			    </div>
			    <label for="lastname" class="col-sm-1 control-label">推荐顺序</label>
			    <div class="col-sm-1">
			      <input name="recommendOrder" type="text" class="form-control" id="lastname" placeholder="推荐顺序"/>
			    </div>
			    <label for="lastname" class="col-sm-1 control-label">类别</label>
			    <div class="col-sm-1">
			      <input name="category"  class="form-control" id="lastname" placeholder="类别"/>
			    </div>
			    <label for="lastname" class="col-sm-1 control-label">类别顺序</label>
			    <div class="col-sm-1">
			      <input name="categoryOrder"  class="form-control" id="lastname" placeholder="类别顺序"/>
			    </div>
			  </div>
			  
			  <div class="form-group">
			    <div class="col-sm-10">
			      <button type="submit" class="btn btn-default">保存</button>
			    </div>
			  </div>
		</form>
		</div>
	</div>
	</div>
</body>
<script type="text/javascript">
$(function(){
	$("#submitBtn").click(function(){
		var versionType = $("#versionType").val();
		var versionNumber = $("#versionNumber").val();
		if(versionType == ""){
			alert("版本类型不能为空！");
			return false;
		}
		if(versionNumber == ""){
			alert("版本号不能为空");
			return false;
		}
		$("#updateForm").submit();
	});
});
</script>
</html>