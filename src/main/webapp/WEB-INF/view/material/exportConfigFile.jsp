<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<html>
<head>
<jsp:include page="../common/header.jsp"></jsp:include>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>导出配置文件</title>
<script type="text/javascript" src="${ctx}/resources/js/jquery-3.1.0.min.js"></script>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/bootstrap.min.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/main.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/body.css"/>
<script type="text/javascript" src="${ctx }/resources/js/jquery-3.1.0.min.js"></script>
<style type="text/css">
*{margin:0;padding:0;}
#updateForm{
	
}
 .formDiv{
 float:left;
	margin-left: 32px;
	margin-top:50px;
}
#submitBtn{
	margin-left: 28px;
}
.tableDiv{
	margin-left: 32px;
	margin-top: 22px;
}
table{
	background-color: #fff;
	border-radius:17px;
	table-layout: fixed;
}
td {
    word-break:break-all; word-wrap:break-word;
    color:#434d63;
}
/* td:HOVER {
	overflow: visible;
	width: auto;
} */
.headTr > td{
	color:#b1bcc0;
}
input{
	border-radius:10px;
	height:35px;
}
.lastUpdateMsg{
	float:left;
	margin-left: 32px;
	margin-top: 22px;
}
</style>
</head>
<body>
<%-- <jsp:include page="../common/head.jsp"></jsp:include> --%>
	<div class="all">
		<div class="main">
			<div class="iconDiv">
				<div class="imageDetail"><img src="${ctx }/resources/images/logo.png"></div>
			</div>
			<div class="ulContent">
				<jsp:include page="../common/menu.jsp"></jsp:include>
			</div>
		</div>
	<div class="body">
	<jsp:include page="../common/bodyHead.jsp"></jsp:include>
		<div class="formDiv">
		<form action="${ctx }/admin/material/exportConfigFile" class="form-horizontal" role="form" method="post">
			  <div class="form-group">
			  	 <label for="firstname" class="col-sm-2 control-label">版本类型</label>
			  	 <div class="col-sm-6">
			  		<input class="form-control" type="text" name="versionType"/>
			  	 </div>
			  </div>
			  <div class="form-group">
			  	<label for="firstname" class="col-sm-2 control-label">APP版本号</label>
			  	<div class="col-sm-6">
			  		<input class="form-control" type="text" name="versionNumber"/>
			  	</div>
			  </div>
			  <div class="form-group">
			    <div class="col-sm-6">
			      <button type="submit" class="btn btn-default pull-right">导出配置文件</button>
			    </div>
			  </div>
		</form>
		</div>
	</div>
	</div>
</body>
<script type="text/javascript">
$(function(){
});
</script>
</html>