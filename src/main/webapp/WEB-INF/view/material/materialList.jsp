<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ page import="java.util.*"%>
<%@ page import="com.lidx.back.statistics.entity.*"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>素材列表管理</title>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/WdatePicker.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/bootstrap.min.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/datepicker.css"/>
<script type="text/javascript" src="${ctx }/resources/js/WdatePicker.js"></script>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/main.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/body.css"/>
<link rel="shortcut icon" href="${ctx }/resources/images/favicon.png">
<script type="text/javascript" src="${ctx }/resources/js/jquery-3.1.0.min.js"></script>
<style type="text/css">
*{margin:0;padding:0;}
.all{
	float:left;
	width:100%;
}

/* .menu{
	float:left;
	width:326px;
	margin-left: 200px;
	margin-top:70px;
} */
/* .body{
	float:left;
	width:800px;
	margin-left: 100px;
	margin-top:72px;
} */
</style>
</head>
<body>
<%-- <jsp:include page="../common/head.jsp"></jsp:include> --%>
<div class="all">
		<%-- <div class="menu">
			<jsp:include page="../common/menu.jsp"></jsp:include>
		</div> --%>
		<div class="main">
			<div class="iconDiv">
				<div class="imageDetail"><img src="${ctx }/resources/images/logo.png"></div>
			</div>
			<div class="ulContent">
				<jsp:include page="../common/menu.jsp"></jsp:include>
			</div>
		</div>
	<div class="body">
	<%-- <div class="bodyHead">
		<div class="logoutDiv pull-right">
			<a class="btn btn-primary" href="${ctx }/logout">用户登出</a>
		</div>
	</div> --%>
	<jsp:include page="../common/bodyHead.jsp"></jsp:include>
		<div style="padding-left: 33px;">

			<a class="btn btn-default" href="${ctx }/admin/material/latestMaterials">最新素材列表</a>
			<%--<a class="btn btn-default" href="${ctx }/admin/material/hotUpdate">热更新</a>
			<a class="btn btn-default" href="${ctx }/admin/material/uploadConfigFile">上传配置文件</a>
			<a class="btn btn-default" href="${ctx }/admin/material/exportConfigFile">导出配置文件</a> --%>
			 <a class="btn btn-default" href="${ctx }/admin/material/addMaterials">批量添加素材</a>
		</div>
		<div style="padding-left: 33px;">
		<a class="btn btn-default" href="${ctx }/admin/material/add">添加素材</a>
		</div>
		<div class="formDiv">
		<form id="materialForm" action="${ctx }/admin/material/listMaterials" class="form-horizontal" method="post">
			<%--<div class="form-group">
				 <label for="firstname" class="col-sm-2 control-label">设备信息</label>
				 <div class="col-sm-4">
					<select class="form-control" name="versionType">
						<option <c:if test="${material.versionType == 'ios' }">selected</c:if> value="ios">IOS系统</option>
						<option <c:if test="${material.versionType == 'android' }">selected</c:if> value="android">安卓系统</option>
					</select>
				</div>
				<label for="firstname" class="col-sm-2 control-label">app版本号</label>
				 <div class="col-sm-4">
				<select class="form-control" name="versionNum">
								<option <c:if test="${material.versionNumber == '1.0.0' }">selected</c:if> value="1.0.0">1.0.0版本</option>
								<option <c:if test="${material.versionNumber == '1.0.1' }">selected</c:if> value="1.0.1">1.0.1版本</option>
								<option <c:if test="${material.versionNumber == '1.1.0' }">selected</c:if> value="1.1.0">1.1.0版本</option>
								<option <c:if test="${material.versionNumber == '1.1.1' }">selected</c:if> value="1.1.1">1.1.1版本</option>
				</select>
				</div>
			</div>--%>
			<div class="form-group">
				<label for="firstname" class="col-sm-2 control-label">素材对应的分类</label>
				 <div class="col-sm-2">
					 <select class="form-control" name="categoryId">
						<option  value="">请选择</option>
						 <c:forEach items="${categoryList }" var="category">
							 <option
									 <c:if test="${material.categoryId == category.categoryId }">selected</c:if>
									 value="${category.categoryId }">${category.categoryName }</option>
						</c:forEach>
					</select>
				</div>
				<div class="col-sm-4">
					<input type="submit" value="查询" class="btn btn-default"/>
				</div>
			</div>
			<div class="form-group">
				

			</div>
		</form>
	</div>

		<div class="tableDiv">
		<c:if test="${ ! empty materials  }">
			<form id="idsForm" action="${ctx}/admin/material/batchDelete" method="post">
			<table class="table table-borded">
				<tr class="headTr">
					<td><input type="checkbox" id="ckAll" class="checkbox"></td>
					<td>序号</td>
					<td>素材uuid</td>
					<td>APP版本类型</td>
					<td>素材版本号</td>
					<td>图标文件名</td>
					<td>素材文件名</td>
					<td>素材文件的类别</td>
					<td>类别顺序</td>
					<td>素材是否有声音</td>
					<td>插入时间</td>
					<td>更新时间</td>
					<td>操作</td>
				</tr>

				<c:forEach items="${materials }" var="po" varStatus="status">
				<tr>
					<td><input type="checkbox" name="id" value="${po.id }"></td>
					<td>${status.count }</td>
					<td>${po.uuid}</td>
					<td>
							<c:choose>
								<c:when test="${po.versionType eq 'ios'}">IOS版本</c:when>
								<c:when test="${po.versionType eq 'android'}">安卓版本</c:when>
							</c:choose>
					</td>
					<td>${po.versionNumber}</td>
					<td>${po.iconName}</td>
					<td>${po.materialName}</td>
					<td>
						<c:forEach items="${categoryList }" var="category">
							<c:if test="${po.categoryId == category.categoryId }">
								${category.categoryName }
							</c:if>
						</c:forEach>
					</td>
					<td>${po.categoryOrder}</td>
					<td>
						<c:choose>
							<c:when test="${po.audio.equals('true')}">有</c:when>
							<c:when test="${po.audio.equals('false')}">没有</c:when>
						</c:choose>

					</td>
					<td>${po.createTime}</td>
					<td>${po.updateTime}</td>

					<td>
						<a onclick="return confirm('是否确认删除？');" class="btn btn-default" href="${ctx }/admin/material/delete?id=${po.id}">删除</a>
						<a class="btn btn-default" href="${ctx }/admin/material/editMaterial?id=${po.id}">编辑</a>
					</td>
				</tr>
				</c:forEach>

			</table>
			<div style="padding: 10px 0 40px 0px;">
				<button onclick="return confirm('是否确认批量删除？');" class="btn btn-default" type="submit">批量删除</button>
			</div>
			</form>
		</c:if>
	</div>
</div>
<script type="text/javascript">

$(function(){
	$(".main").css("height",$(document).height()+50+"px");
	$("#exportBtn").click(function(){
		$("#materialForm").attr("action","${ctx }/admin/material/exportExcel");
		$("#materialForm").submit();
	});
	
	$("#queryBtn").click(function(){
		$("#materialForm").attr("action","${ctx }/admin/material/list");
		$("#materialForm").submit();
	});
	$("#ckAll").click(function () {

		$("input[name='id']").each(function(){
			if($(this).attr("checked"))
			{
				$(this).removeAttr("checked");
			}else {
				$(this).attr("checked","true");
			}
		});
	});
});
</script>
</body>
</html>