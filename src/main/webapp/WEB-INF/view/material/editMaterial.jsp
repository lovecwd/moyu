<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>素材编辑</title>
<script type="text/javascript" src="${ctx}/resources/js/jquery-3.1.0.min.js"></script>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/bootstrap.min.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/main.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/body.css"/>
<script type="text/javascript" src="${ctx }/resources/js/jquery-3.1.0.min.js"></script>
<style type="text/css">
*{margin:0;padding:0;}
#updateForm{
	
}
 .formDiv{
 float:left;
	margin-left: 32px;
	margin-top:50px;
}
#submitBtn{
	margin-left: 28px;
}
.tableDiv{
	margin-left: 32px;
	margin-top: 22px;
}
table{
	background-color: #fff;
	border-radius:17px;
	table-layout: fixed;
}
td {
    word-break:break-all; word-wrap:break-word;
    color:#434d63;
}
/* td:HOVER {
	overflow: visible;
	width: auto;
} */
.headTr > td{
	color:#b1bcc0;
}
input{
	border-radius:10px;
	height:35px;
}
.lastUpdateMsg{
	float:left;
	margin-left: 32px;
	margin-top: 22px;
}
</style>
</head>
<body>
<%-- <jsp:include page="../common/head.jsp"></jsp:include> --%>
	<div class="all">
		<div class="main">
			<div class="iconDiv">
				<div class="imageDetail"><img src="${ctx }/resources/images/logo.png"></div>
			</div>
			<div class="ulContent">
				<jsp:include page="../common/menu.jsp"></jsp:include>
			</div>
		</div>
	<div class="body">
	<jsp:include page="../common/bodyHead.jsp"></jsp:include>
		<div class="formDiv">
		<a href="javascript:history.go(-1);" class="btn btn-default">返回</a>
		<form:form enctype="multipart/form-data" commandName="material" modelAttribute="material" action="${ctx }/admin/material/editMaterial" class="form-horizontal" role="form" method="post">
			  <form:input path="id"  type="hidden"/>
			<form:input path="iconMD5" type="hidden"/>
			<form:input path="materialMD5" type="hidden"/>
			<form:input path="createTime" type="hidden"/>
			<form:input path="updateTime" type="hidden"/>

			  <div class="form-group">
			    <label for="firstname" class="col-sm-2 control-label">素材uuid</label>
			    <div class="col-sm-10">
			      <form:input path="uuid"  class="form-control" id="firstname" placeholder="素材uuid"/>
			    </div>
			  </div>
			<div class="form-group">
				<label for="lastname" class="col-sm-2 control-label">素材文件的类别</label>
				<div class="col-sm-10">
					<form:select path="categoryId" class="form-control">
						<c:forEach items="${categoryList }" var="category">
							<form:option value="${category.categoryId }">${category.categoryName }</form:option>
						</c:forEach>
					</form:select>
				</div>
			</div>
			  <div class="form-group">
			    <label for="lastname" class="col-sm-2 control-label">素材顺序</label>
			    <div class="col-sm-10">
			      <form:input path="categoryOrder"  class="form-control" id="lastname" placeholder="素材顺序"/>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="lastname" class="col-sm-2 control-label">APP版本类型</label>
			    <div class="col-sm-10">
			      <form:select path="versionType" type="text" class="form-control" id="versionType" >
				  	<form:option value="ios">IOS版本</form:option>
				  	<form:option value="android">安卓版本</form:option>
				  </form:select>
			    </div>
			  </div>
			<div class="form-group">
				<label for="lastname" class="col-sm-2 control-label">素材版本号</label>
				<div class="col-sm-10">
					<form:input path="versionNumber" type="text" class="form-control" placeholder="素材版本号"/>
				</div>
			</div>
			<%--<div class="form-group">
              <label for="lastname" class="col-sm-2 control-label">APP版本号</label>
              <div class="col-sm-10">
                <form:input path="versionNumber" type="text" class="form-control" id="versionNumber" placeholder="APP版本号"/>
              </div>
            </div>--%>
			  <div class="form-group">
			    <label for="lastname" class="col-sm-2 control-label">图标文件名</label>
			    <div class="col-sm-10">
			      <form:input path="iconName" type="text" class="form-control" id="iconName" placeholder="图标文件名"/>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="lastname" class="col-sm-2 control-label">素材文件名</label>
			    <div class="col-sm-10">
			      <form:input path="materialName" type="text" class="form-control" id="materialName" placeholder="素材文件名"/>
			    </div>
			  </div>
			<div class="form-group">
				<label for="lastname" class="col-sm-2 control-label">图标文件</label>
				<div class="col-sm-4">
					<input type="file" name="iconFile" class="file"/>
				</div>
			</div>
			<div class="form-group">
				<label for="lastname" class="col-sm-2 control-label">素材文件</label>
				<div class="col-sm-4">
					<input type="file" name="materialFile" class="file"/>
				</div>
			</div>

			  <%--<div class="form-group">
			    <label for="lastname" class="col-sm-2 control-label">图标文件MD5</label>
			    <div class="col-sm-4">
			      <form:input path="iconMD5" type="text" class="form-control" id="iconMD5" placeholder="图标文件MD5"/>
			    </div>
			    <label for="lastname" class="col-sm-2 control-label">最新图标文件MD5</label>
			    <div id="iconMD5Real" class="col-sm-4">

			    </div>
			  </div>--%>
			  <div class="form-group">
			    <label for="lastname" class="col-sm-2 control-label">图标文件下载路径</label>
			    <div class="col-sm-10">
			      <form:input path="iconDownloadUrl" type="text" class="form-control" id="lastname" placeholder="图标文件下载路径"/>
			    </div>
			  </div>
			  <%--<div class="form-group">
			    <label for="lastname" class="col-sm-2 control-label">素材文件MD5</label>
			    <div class="col-sm-4">
			      <form:input path="materialMD5" type="text" class="form-control" id="materialMD5" placeholder="素材文件MD5"/>
			    </div>
			    <label for="lastname" class="col-sm-2 control-label">最新图标文件MD5</label>
			    <div id="materialMD5Real" class="col-sm-4">

			    </div>
			  </div>--%>
			  <div class="form-group">
			    <label for="lastname" class="col-sm-2 control-label">素材文件下载路径</label>
			    <div class="col-sm-10">
			      <form:input path="materialDownloadUrl" type="text" class="form-control" id="lastname" placeholder="素材文件下载路径"/>
			    </div>
			  </div>
			<div class="form-group">
				<label for="lastname" class="col-sm-2 control-label">素材是否有声音</label>
				<div class="col-sm-10">
					<form:select path="audio" class="form-control">
						<form:option value="true">有声音</form:option>
						<form:option value="false">没有声音</form:option>
					</form:select>
				</div>
			</div>
			  <div class="form-group">
			    <div class="col-sm-offset-2 col-sm-10">
			      <button type="submit" class="btn btn-default">保存</button>
			    </div>
			  </div>
		</form:form>
		</div>
		
		
	
	
	</div>
	</div>
</body>
<script type="text/javascript">
$(function(){
	var iconName = $("#iconName").val();
	var materialName = $("#materialName").val();
	var versionType = $("#versionType").val();
	var versionNumber = $("#versionNumber").val();
	/*$.getJSON("${ctx}/admin/material/getMD5s?iconName="+iconName+"&materialName="+materialName+
			"&versionType="+versionType+"&versionNumber="+versionNumber, function(data){
		var msg = data.msg;
		if(msg == "success"){
			$("#iconMD5Real").html(data.iconMd5);
			$("#materialMD5Real").html(data.materialMd5);
		}else{
			$("#iconMD5Real").val("未查询到MD5值");
			$("#materialMD5Real").val("未查询到MD5值");
		}
		
	});*/
	
	$("#submitBtn").click(function(){
		var versionType = $("#versionType").val();
		var versionNumber = $("#versionNumber").val();
		if(versionType == ""){
			alert("版本类型不能为空！");
			return false;
		}
		if(versionNumber == ""){
			alert("版本号不能为空");
			return false;
		}
		$("#updateForm").submit();
	});
});
</script>
</html>