<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<jsp:include page="../common/header.jsp"></jsp:include>
<script type="text/javascript" src="${ctx }/resources/js/WdatePicker.js"></script>
<style type="text/css">
*{margin:0;padding:0;}
.all{
	float:left;
	width:100%;
}
.table{
	width:50%;
}
/* .menu{
	float:left;
	width:326px;
	margin-left: 200px;
	margin-top:70px;
}
.body{
	float:left;
	width:800px;
	margin-left: 100px;
	margin-top:72px;
} */
</style>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/body.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/main.css"/>
</head>
<body>
<%-- <jsp:include page="../common/head.jsp"></jsp:include> --%>
	<div class="all">
		<div class="main">
			<div class="iconDiv">
				<div class="imageDetail"><img src="${ctx }/resources/images/logo.png"></div>
			</div>
			<div class="ulContent">
				<jsp:include page="../common/menu.jsp"></jsp:include>
			</div>
		</div>
	<div class="body">
	<jsp:include page="../common/bodyHead.jsp"></jsp:include>
	<div class="formDiv">
		<form id="clientForm" action="${ctx }/admin/client/list" method="post">
		<div class="form-group">
			 <label for="firstname" class="col-sm-2 control-label">用户行为</label>
			  <div class="col-sm-4">
			<select class="form-control" name="clientRecords">
				<%--<option <c:if test="${action == 'closeSound' }">selected</c:if> value="closeSound">用户关闭声音的次数</option>
				<option <c:if test="${action == 'openFlashLight' }">selected</c:if> value="openFlashLight">用户开启闪光灯的次数</option>
				<option <c:if test="${action == 'switchCamera' }">selected</c:if> value="switchCamera">用户切换摄像头的次数</option>
				<option <c:if test="${action == 'switchFilter' }">selected</c:if> value="switchFilter">用户使用滤镜快速切换的次数</option>
				<option <c:if test="${action == 'recordVideo' }">selected</c:if> value="recordVideo">用户录制视频的平均时长</option>
				<option <c:if test="${action == 'linkShare' }">selected</c:if> value="linkShare">用户分享推荐链接数统计</option>
				<option <c:if test="${action == 'linkOpen' }">selected</c:if> value="linkOpen">推荐链接的打开量</option>
				<option <c:if test="${action == 'saveVideo' }">selected</c:if> value="saveVideo">用户保存视频到本地的次数</option>
				<option <c:if test="${action == 'savePicture' }">selected</c:if> value="savePicture">用户保存照片到本地的次数</option>
				<option <c:if test="${action == 'sexPercentage' }">selected</c:if> value="sexPercentage">用户男女比例</option>--%>
				<option>用户数量</option>
			</select>
			</div>
			
		</div>
		<div class="form-group">
			<label for="firstname" class="col-sm-2 control-label">设备版本类型</label>
			<div class="col-sm-4">
			<select class="form-control" name="versionType">
				<option value="">请选择</option>
				<option <c:if test="${versionType == 'ios' }">selected</c:if> value="ios">IOS系统</option>
				<option <c:if test="${versionType == 'android' }">selected</c:if> value="android">安卓系统</option>
			</select>
			</div>
		</div>
		<div class="form-group">
				<label for="firstname" class="col-sm-2 control-label">APP版本号</label>
				 <div class="col-sm-4">
				<input class="form-control" name="versionNumber">
				 </div>
					<%--<option value="">请选择</option>
                    <c:forEach items="${versionNumbers }" var="versionNumber1">
                        <option <c:if test="${versionNumber == versionNumber1 }">selected</c:if> value="${ versionNumber1}">${ versionNumber1}版本</option>
                    </c:forEach>
                    &lt;%&ndash; <option <c:if test="${versionNumber == '1.1.1' }">selected</c:if> value="1.1.1">1.1.1版本</option>
                    <option <c:if test="${versionNumber == '1.1.0' }">selected</c:if> value="1.1.0">1.1.0版本</option>
                    <option <c:if test="${versionNumber == '1.0.1' }">selected</c:if> value="1.0.1">1.0.1版本</option>
                    <option <c:if test="${versionNumber == '1.0.0' }">selected</c:if> value="1.0.0">1.0.0版本</option> &ndash;%&gt;
            </select>
            </div>--%>

		</div>
		<div class="form-group">
			从 <input name="from" class="Wdate" type="text" value="${from }" onClick="WdatePicker()">&nbsp;&nbsp;
			到<input name="to" class="Wdate" type="text" value="${to }" onClick="WdatePicker()">
			<input id="queryBtn" type="button" class="btn btn-default" value="查询"/>
			<%--<input id="exportBtn" type="button" class="btn btn-default" value="导出"/>--%>
		</div>
		</form>
	</div>
	<div class="tableDiv">
		<table class="table table-bordered">
			<tr>
				<td>用户数量</td>
			</tr>
			<tr>
				<td>${number}</td>
			</tr>
		</table>
		<%--<c:if test="${! empty records }">
		<table class="table table-bordered">
				<tr>
					<td>用户行为</td>
					<td>设备类型</td>
					<td>APP版本号</td>
					<td>数量</td>
				</tr>
			
			<c:forEach items="${records }" var="count">
			<tr>
				<td>${count[0] }</td>
				<td>${count[1] }</td>
				<td>${count[2] }</td>
				<td>${count[3] }</td>
			</tr>
			</c:forEach>	
		</table>
		</c:if>
		<c:if test="${! empty sexInfo }">
		<table class="table table-bordered">
				<tr>
					<td>用户性别</td>
					<td>APP版本号</td>
					<td>数量</td>
				</tr>
			
			<c:forEach items="${sexInfo }" var="info">
			<tr>
				<td>${info[0] }</td>
				<td>${info[2] }</td>
				<td>${info[3] }</td>
			</tr>
			</c:forEach>	
		</table>
		</c:if>
		<c:if test="${! empty videoRecordInfo }">
		<table class="table table-bordered">
				<tr>
					<td>平均时长(单位秒)</td>
					<td>设备类型</td>
					<td>APP版本号</td>
				</tr>
			
			<tr>
				<td>${videoRecordInfo[1] }</td>
				<td>${videoRecordInfo[2] }</td>
				<td>${videoRecordInfo[3] }</td>
			</tr>
		</table>
		</c:if>
		--%>
	</div>


	
</div>
</div>
<script type="text/javascript">
$(function(){
	$("#exportBtn").click(function(){
		$("#clientForm").attr("action","${ctx }/admin/client/exportClientActionExcel");
		$("#clientForm").submit();
	});
	
	$("#queryBtn").click(function(){
		$("#clientForm").attr("action","${ctx }/admin/client/list");
		$("#clientForm").submit();
	});
});
</script>
</body>
</html>