<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<jsp:include page="../common/header.jsp"></jsp:include>
<script type="text/javascript" src="${ctx }/resources/js/WdatePicker.js"></script>
<style type="text/css">
*{margin:0;padding:0;}
.all{
	float:left;
	width:100%;
}
/* .menu{
	float:left;
	width:326px;
	margin-left: 200px;
	margin-top:70px;
}
.body{
	float:left;
	width:800px;
	margin-left: 100px;
	margin-top:72px;
} */
</style>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/main.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/body.css"/>
</head>
<body>
<%-- <jsp:include page="../common/head.jsp"></jsp:include> --%>
	<div class="all">
		<div class="main">
			<div class="iconDiv">
				<div class="imageDetail"><img src="${ctx }/resources/images/logo.png"></div>
			</div>
			<div class="ulContent">
				<jsp:include page="../common/menu.jsp"></jsp:include>
			</div>
		</div>
	<div class="body">
	<jsp:include page="../common/bodyHead.jsp"></jsp:include>
	<div class="bodyTitle">用户分享数据：</div>
	<div class="formDiv">
		<form id="cShareForm" action="${ctx }/admin/client/shareData" method="post">
		<div class="form-group">
			 <label for="firstname" class="col-sm-2 control-label">用户分享</label>
			  <div class="col-sm-4">
			<select class="form-control" name="clientRecords">
				<option <c:if test="${action == 'picShare' }">selected</c:if> value="picShare">用户分享照片数统计</option>
				<option <c:if test="${action == 'videoShare' }">selected</c:if> value="videoShare">用户分享视频数统计</option>
			</select>
			</div>
			<div class="col-sm-6"></div>
		</div>
		<div class="form-group">
			<label for="firstname" class="col-sm-2 control-label">设备版本类型</label>
			<div class="col-sm-4">
			<select class="form-control" name="versionType">
							<option <c:if test="${versionType == 'ios' }">selected</c:if> value="ios">IOS系统</option>
							<option <c:if test="${versionType == 'android' }">selected</c:if> value="android">安卓系统</option>
			</select>
			</div>
			<div class="col-sm-6"></div>
		</div>
		<div class="form-group">
				<%--<label for="firstname" class="col-sm-2 control-label">APP版本号</label>
				 <div class="col-sm-4">
				<select class="form-control" name="versionNumber">
						<option value="">请选择</option>
						<c:forEach items="${versionNumbers }" var="versionNumber1">
							<option <c:if test="${versionNumber == versionNumber1 }">selected</c:if> value="${ versionNumber1}">${ versionNumber1}版本</option>
						</c:forEach>
						&lt;%&ndash; <option <c:if test="${versionNumber == '1.1.1' }">selected</c:if> value="1.1.1">1.1.1版本</option>
						<option <c:if test="${versionNumber == '1.1.0' }">selected</c:if> value="1.1.0">1.1.0版本</option>
						<option <c:if test="${versionNumber == '1.0.1' }">selected</c:if> value="1.0.1">1.0.1版本</option>
						<option <c:if test="${versionNumber == '1.0.0' }">selected</c:if> value="1.0.0">1.0.0版本</option> &ndash;%&gt;
				</select>
				</div>--%>
			<label for="firstname" class="col-sm-2 control-label">APP运行环境</label>
			<div class="col-sm-4">
				<select class="form-control" name="online">
					<option <c:if test="${online == 'y' }">selected</c:if> value="y">线上环境</option>
					<option <c:if test="${online == 'n' }">selected</c:if> value="n">非线上环境</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			从 <input name="from" class="Wdate" type="text" value="${from }" onClick="WdatePicker()">
			到<input name="to" class="Wdate" type="text" value="${to }" onClick="WdatePicker()">
			<input id="queryBtn" type="submit" class="btn btn-default" value="查询"/>
			<input id="exportBtn" type="button" class="btn btn-default" value="导出"/>
		</div>
		</form>
	</div>
	<c:if test="${! empty records }">
	<div class="tableDiv">
	<table class="table table-bordered">
			<tr>
				<td>分享渠道</td>
				<td>版本类型</td>
				<td>APP版本号</td>
				<td>分享数量</td>
			</tr>
		
		<c:forEach items="${records }" var="count">
		<tr>
			<td>${count[0] }</td>
			<td>${count[1] }</td>
			<td>${count[2] }</td>
			<td>${count[3] }</td>
		</tr>
		</c:forEach>	
	</table>
	</div>
</c:if>

	
</div>
</div>
<script type="text/javascript">
$(function(){
	$("#exportBtn").click(function(){
		$("#cShareForm").attr("action","${ctx }/admin/client/exportClientShareData");
		$("#cShareForm").submit();
	});
	
	$("#queryBtn").click(function(){
		$("#cShareForm").attr("action","${ctx }/admin/client/shareData");
		$("#cShareForm").submit();
	});
});
</script>
</body>
</html>