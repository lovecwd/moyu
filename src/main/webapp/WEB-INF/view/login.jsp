<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="org.apache.shiro.web.filter.authc.FormAuthenticationFilter"%>
<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>魔鱼后台登录</title>
	<meta name="renderer" content="webkit">	
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">	
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">	
	<meta name="apple-mobile-web-app-capable" content="yes">	
	<meta name="format-detection" content="telephone=no">	
	<link rel="shortcut icon" href="${ctx }/resources/images/favicon.png">
	<!-- load css -->
	<link rel="stylesheet" type="text/css" href="${ctx }/resources/css/layui.css" media="all">
	<link rel="stylesheet" type="text/css" href="${ctx }/resources/css/login.css" media="all">
</head>
<body>
<div class="layui-canvs"></div>
<div class="layui-layout layui-layout-login">
	<div id="messageBox" class="alert alert-error ${empty message ? 'hide' : ''}"><!-- <button data-dismiss="alert" class="close">×</button> -->
			<label id="loginError" class="error">${message}</label>
	</div>
	<h1>
		 <strong>魔鱼管理系统后台</strong>
		 <em>Management System</em>
	</h1>
	<form id="loginForm"  action="${ctx }/login" class="form-horizontal" method="post">
	<div class="layui-user-icon larry-login">
		 <input type="text" name="username" placeholder="用户名" class="login_txtbx"/>
	</div>
	<div class="layui-pwd-icon larry-login">
		 <input name="password" type="password" placeholder="密码" class="login_txtbx"/>
	</div>
    
    <div class="layui-val-icon ">
    	<div class="layui-code-box ui checkbox">
            <input class="cbInput"  type="checkbox" name="rememberMe" value="true" />
            	<label>记住我</label>
    	</div>
    </div>
    <div class="layui-submit larry-login">
    	<input type="submit" value="立即登录" class="submit_btn"/>
    </div>
    </form>
    <div class="layui-login-text">
    	<p>© 2016-2017 Larry 版权所有</p>
        <p> <a href="#" title="">magicjoy</a></p>
    </div>
</div>
<script type="text/javascript" src="${ctx }/resources/js/layui.all.js"></script>
<script type="text/javascript" src="${ctx }/resources/js/login.js"></script>
<script type="text/javascript" src="${ctx }/resources/js/jparticle.jquery.js"></script>
<script type="text/javascript">
$(function(){
	$(".layui-canvs").jParticle({
		background: "#141414",
		color: "#E6E6E6"
	});
	//登录链接测试，使用时可删除
	/* $(".submit_btn").click(function(){
	   $("#loginForm").submit();
	}); */
});
</script>
</body>
</html>