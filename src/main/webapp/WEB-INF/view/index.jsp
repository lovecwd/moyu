<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>前台用户首页</title>
    <link rel="shortcut icon" href="${ctx }/resources/images/favicon.png">
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/bootstrap.min.css"/>
    <script src="${ctx}/resources/js/vue.js"></script>
    <script type="text/javascript" src="${ctx }/resources/js/jquery-3.1.0.min.js"></script>

</head>

<body>
<div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name">MAGICJOY</h1>

            </div>
            <h3>Welcome to MAGICJOY</h3>
            <!-- <p>Perfectly designed and precisely prepared admin theme with over 50 pages with extra new web app views.
                Continually expanded and constantly improved Inspinia Admin Them (IN+)
            </p> -->
            <p>Login in. To see it in action.</p>
            <h1><a href="${ctx }/admin/index.html">进入后台</a></h1>
            <!-- <form class="m-t" role="form" action="index.html">
                <div class="form-group">
                    <input type="email" class="form-control" placeholder="Username" required="">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Password" required="">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

                <a href="#"><small>Forgot password?</small></a>
                <p class="text-muted text-center"><small>Do not have an account?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="register.html">Create an account</a>
            </form> -->
            <p class="m-t"> <small>Inspinia we app framework base on Bootstrap 3 &copy; 2016</small> </p>
        </div>
</div>
<div id="forVue" class="text-center">
    <div id="app">
        {{ message }}
    </div>
    <script type="text/javascript">
    var app = new Vue({
    el: '#app',
    data: {
    message: 'Hello Vue!'
    }
    })
    </script>
</div>
	
	
	
	
	
<script type="text/javascript">
</script>
</body>
</html>