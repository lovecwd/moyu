<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../common/header.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>用户添加</title>
</head>
<body>

	<div class="all">
		<div class="main">
			<div class="iconDiv">
				<div class="imageDetail"><img src="${ctx }/resources/images/logo.png"></div>
			</div>
			<div class="ulContent">
				<jsp:include page="../common/menu.jsp"></jsp:include>
			</div>
		</div>
	<div class="body">
	<jsp:include page="../common/bodyHead.jsp"></jsp:include>
	<div class="tableDiv">
		<div>
			<div><a class="btn btn-default" href="${ctx }/admin/user/add">用户添加</a></div>
		</div>
		<c:if test="${! empty userList }">
		<table class="table table-borded">
			<tr>
				<th>登录名</th>
				<th>真实姓名</th>
			</tr>
			<c:forEach items="${userList }" var="user">
			<tr>
				<td>${user.loginName }</td>
				<td>${user.name }</td>
				<td>
					<a class="btn btn-default" href="${ctx }/admin/user/delete?id=${user.id}">删除用户</a>
					<a class="btn btn-default" href="${ctx }/admin/user/edit?id=${user.id}">编辑用户</a>
				</td>
			</tr>
			</c:forEach>
		</table>
	</c:if>
	</div>


	
</div>
</div>
</body>
</html>