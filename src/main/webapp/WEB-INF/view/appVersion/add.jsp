<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>添加APP版本</title>
<jsp:include page="../common/header.jsp"></jsp:include>
<script type="text/javascript" src="${ctx }/resources/js/WdatePicker.js"></script>
<style type="text/css">
*{margin:0;padding:0;}
.all{
	float:left;
	width:100%;
}
.table{
	width:50%;
}
/* .menu{
	float:left;
	width:326px;
	margin-left: 200px;
	margin-top:70px;
}
.body{
	float:left;
	width:800px;
	margin-left: 100px;
	margin-top:72px;
} */
</style>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/body.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/main.css"/>
</head>
<body>
<%-- <jsp:include page="../common/head.jsp"></jsp:include> --%>
	<div class="all">
		<div class="main">
			<div class="iconDiv">
				<div class="imageDetail"><img src="${ctx }/resources/images/logo.png"></div>
			</div>
			<div class="ulContent">
				<jsp:include page="../common/menu.jsp"></jsp:include>
			</div>
		</div>
	<div class="body">
	<jsp:include page="../common/bodyHead.jsp"></jsp:include>
	<div class="formDiv">
		<form id="clientForm" action="${ctx }/admin/appVersion/add" method="post">
		<div class="form-group">
			<label for="firstname" class="col-sm-2 control-label">设备版本类型</label>
			<div class="col-sm-4">
			<select class="form-control" name="versionType">
							<option <c:if test="${versionType == 'ios' }">selected</c:if> value="ios">IOS系统</option>
							<option <c:if test="${versionType == 'android' }">selected</c:if> value="android">安卓系统</option>
			</select>
			</div>
		</div>
		<div class="form-group">
				<label for="firstname" class="col-sm-2 control-label">APP版本号</label>
				 <div class="col-sm-4">
				 	<input type="text" name="versionNumber" class="form-control"/>
				</div>
		</div>
		<div class="form-group">
			<label for="firstname" class="col-sm-2 control-label">APP最新版本号</label>
			<div class="col-sm-4">
				<input type="text" name="latestVersionNum" class="form-control"/>
			</div>
		</div>
		<div class="form-group">
			<label for="firstname" class="col-sm-2 control-label">版本描述</label>
			<div class="col-sm-4">
				<input type="text" name="versionDesc" class="form-control"/>
			</div>
		</div>
		<div class="form-group">
			<label for="firstname" class="col-sm-2 control-label">是否可获得</label>
			<div class="col-sm-4">
				<input type="text" name="isAvailable" class="form-control"/>
			</div>
		</div>
		<div class="form-group">
			<input id="queryBtn" type="button" class="btn btn-default" value="添加"/>
		</div>
		</form>
	</div>
	<div class="tableDiv">
		<c:if test="${! empty versions }">
		<table class="table table-bordered">
				<tr>
					<td>设备类型</td>
					<td>APP版本号</td>
					<td>APP最新版本号</td>
					<td>版本描述</td>
					<td>此版本是否可获得</td>
				</tr>
			
			<c:forEach items="${versions }" var="version">
			<tr>
				<td>${version.versionType }</td>
				<td>${version.versionNum }</td>
				<td>${version.latestVersionNum }</td>
				<td>${version.versionDesc }</td>
				<td>${version.isAvailable }</td>
			</tr>
			</c:forEach>	
		</table>
		</c:if>
	</div>


	
</div>
</div>
<script type="text/javascript">
$(function(){
	
	$("#queryBtn").click(function(){
		$("#clientForm").submit();
	});
});
</script>
</body>
</html>