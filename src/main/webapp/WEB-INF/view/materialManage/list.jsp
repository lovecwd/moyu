<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/WdatePicker.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/bootstrap.min.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/datepicker.css"/>
<script type="text/javascript" src="${ctx }/resources/js/WdatePicker.js"></script>
</head>
<body>
<div>
	<form class="form-horizontal" action="${ctx }/admin/materialUpdate/list" role="form">
  <div class="form-group">
    <label for="firstname" class="col-sm-2 control-label">版本类型</label>
    <div class="col-sm-10">
      <input type="text" name="versionType" class="form-control" id="firstname" placeholder="">
    </div>
  </div>
  <div class="form-group">
    <label for="lastname" class="col-sm-2 control-label">版本号</label>
    <div class="col-sm-10">
      <input type="text" name="versionNum" class="form-control" id="lastname" placeholder="">
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">查询</button>
    </div>
  </div>
</form>
</div>
<div>
	
</div>
</body>
</html>