<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="body-full-height">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />

<title>魔鱼后台登录</title>
 
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<link rel="shortcut icon" href="${ctx }/resources/images/favicon.png">
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/bootstrap.min.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/theme-default.css"/>
<style type="text/css">
.ckbxDiv{
	float:left;
}
.cbInput{
	min-height: 35px;
	width: 15px;
	border: 1px;
}
.ck{
	width: 20%;
	float:left;
}
.remem{
	line-height: 35px;
	font-size: 20px;
}
</style>
</head>
<body>
	 <div class="login-container">

            <div class="login-box animated fadeInDown">
                <div class="login-logo">魔鱼后台</div>
                <div class="login-body">
                    <div class="login-title"><strong>Welcome</strong>, Please login</div>
                    <form  action="${ctx }/login" class="form-horizontal" method="post">
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="text" name="username" class="form-control" placeholder="Username"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="password" name="password" class="form-control" placeholder="Password"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6  ckbxDiv">
                           <div class="ck"><input class="cbInput"  type="checkbox" name="rememberMe" value="true" /></div>
                           	<div class="remem">记住我</div>
                        </div>
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-info btn-block">登录</button>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="login-footer">
                    <div class="pull-left">
                        &copy; 2016 moyu
                    </div>
                    <div class="pull-right">
                        <a href="#">About</a> |
                        <a href="#">Privacy</a> |
                        <a href="#">Contact Us</a>
                    </div>
                </div>
            </div>

        </div>

   
</body>
</html>