<%--
  Created by IntelliJ IDEA.
  User: chen
  Date: 2017/2/21
  Time: 11:51
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" pageEncoding="utf-8"%>
<style type="text/css">
    *{margin:0;padding:0;}
    .all{
        float:left;
        width:100%;
    }
    .table{
        width:100%;}
    /* .menu{
        float:left;
        width:326px;
        margin-left: 200px;
        margin-top:70px;
    } */
    /* .body{
        float:left;
        width:800px;
        margin-left: 100px;
        margin-top:72px;
    } */
    .pageSizeSelect{
        width:100px;
        height:30px;
        border:1px solid #ccc;
        border-radius:4px;
    }
    .pageDiv{
        float:right;
        width: 100%;
    }
    .tableBottom{
        float:left;
        width:100%;
    }
    .table-desc{
        margin:20px 0 0 0;
        float:left;
        width:50%;
    }
    .pagination > li{
        float:left;
        margin:0 1px;
        border:1px solid #ddd;
    }
</style>
<div class="tableBottom">
    <div class="table-desc form-group">
        <label class="control-label">每页</label>
        <select class="pageSizeSelect" name="pageSize">
            <option <c:if test="${pageInfo.pageSize == '10' }">selected</c:if> value="10">10条</option>
            <option <c:if test="${pageInfo.pageSize == '20' }">selected</c:if> value="20">20条</option>
            <option <c:if test="${pageInfo.pageSize == '50' }">selected</c:if> value="50">50条</option>
        </select>
        <label class="control-label">条记录 第${pageInfo.pageNo }页 共${pageInfo.totalPage }页 总共${pageInfo.allRow }条记录</label>
    </div>
    <div id="pageDiv" class="pageDiv">
        <%-- <ul class="pagination pagination-sm">
            <c:forEach begin="1" end="${pageInfo.totalPage }"  var="i">
                <li><a class="hrefClass" href="#">${i }</a></li>
            </c:forEach>
        </ul> --%>

    </div>
</div>

