<%@ page import="com.lidx.back.statistics.security.shiro.session.JedisSessionDAO" %>
<%@ page import="com.lidx.back.statistics.commons.SpringContextHolder" %>
<%@ page import="org.apache.shiro.session.Session" %>
<%@ page import="java.util.Set" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<div class="bodyHead">
		<div class="logoutDiv">
			欢迎[<shiro:principal property="loginName"/>]登录，<a class="btn btn-primary" href="${ctx }/logout">用户登出</a>  
			<%
				JedisSessionDAO jedisSessionDAO = SpringContextHolder.getBean("sessionDAO");
				Set<Session> sessions = (Set<Session>) jedisSessionDAO.getActiveSessions();
				int sessionsSize = sessions.size();
			%>
			在线人数：<%=sessionsSize%>
		</div>
</div>
