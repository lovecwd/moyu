<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../common/header.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>团队资源管理</title>
	<style type="text/css">
		*{margin:0;padding:0;}
		.table{
			width:100%;}
		.pageSizeSelect{
			width:100px;
			height:30px;
			border:1px solid #ccc;
			border-radius:4px;
		}
		.pageDiv{
			float:right;
			width: 100%;
		}
		.tableBottom{
			float:left;
			width:100%;
		}
		.table-desc{
			margin:20px 0 0 0;
			float:left;
			width:50%;
		}
		.pagination > li{
			float:left;
			margin:0 1px;
			border:1px solid #ddd;
		}
	</style>
<link rel="stylesheet" type="text/css" href="${ctx }/resources/css/layui.css" media="all">

</head>
<body>

	<div class="all">
		<div class="main">
			<div class="iconDiv">
				<div class="imageDetail"><img src="${ctx }/resources/images/logo.png"></div>
			</div>
			<div class="ulContent">
				<jsp:include page="../common/menu.jsp"></jsp:include>
			</div>
		</div>
	<div class="body">
	<jsp:include page="../common/bodyHead.jsp"></jsp:include>

		<div class="formDiv ">
			<div>
				<a href="${ctx}/admin/teamResource/add" class="btn btn-default">上传资源</a>
			</div>
			<form id="teamResourceForm" action="${ctx }/admin/teamResource/list" class="form-horizontal" method="post">
				<div class="form-group layui-hide">
					<div class="col-sm-2"></div>
					<input id="pageNo" name="pageNo" type="hidden" value="${pageInfo.pageNo}"/>
					<input id="pageSize" name="pageSize" type="hidden" value="${pageInfo.pageSize}"/>
				</div>
				<div class="form-group">
					<label for="resourceCategory" class="col-sm-2 control-label">资源类别</label>
					<div class="col-sm-4">
						<select class="form-control" name="resourceCategory">
							<option value="">请选择</option>
							<option <c:if test="${resourceCategory == 'business' }">selected</c:if> value="business">商务类(负责人桃子)</option>
							<option <c:if test="${resourceCategory == 'plan' }">selected</c:if> value="plan">策划类(负责人绘艺)</option>
							<option <c:if test="${resourceCategory == 'art' }">selected</c:if> value="art">美术类(负责人方方)</option>
							<option <c:if test="${resourceCategory == 'program' }">selected</c:if> value="program">程序类(负责人老姜)</option>
							<option <c:if test="${resourceCategory == 'algorithm' }">selected</c:if> value="algorithm">算法类(负责人三石)</option>
							<option <c:if test="${resourceCategory == 'other' }">selected</c:if> value="other">其他</option>
						</select>
					</div>
					<div class="col-sm-4">
						<button class="btn btn-default" type="submit">查询</button>
					</div>
				</div>

			</form>
		</div>

	<div class="tableDiv">

		<hr>
		<div><p>团队资源列表</p></div>

		<table class="table table-borded">
			<tr>
				<th>序号</th>
				<th>上传资源者的姓名</th>
				<th>资源名称</th>
				<th>资源描述</th>
				<th>资源类型</th>
				<%--<th>资源URL</th>--%>
				<th>资源上传时间</th>
				<th>资源更新时间</th>
				<th>操作</th>
			</tr>
			<c:forEach items="${pageInfo.list }" var="teamResource" varStatus="varstatus">
				<tr>
					<td>${varstatus.count}</td>
					<td>${teamResource.submitterName }</td>
					<td>${teamResource.resourceName }</td>
					<td>${teamResource.resourceDesc }</td>
					<td>
						<c:choose>
							<c:when test="${teamResource.resourceCategory eq 'business'}">商务类</c:when>
							<c:when test="${teamResource.resourceCategory eq 'plan'}">策划类</c:when>
							<c:when test="${teamResource.resourceCategory eq 'art'}">美术类</c:when>
							<c:when test="${teamResource.resourceCategory eq 'program'}">程序类</c:when>
							<c:when test="${teamResource.resourceCategory eq 'algorithm'}">算法类</c:when>
							<c:when test="${teamResource.resourceCategory eq 'other'}">其他</c:when>

						</c:choose>
					</td>
					<%--<td><a class="btn btn-default" href="${ctx}${teamResource.resourceUrl}">下载资源</a> </td>--%>
					<td>${teamResource.saveDate}</td>
					<td>${teamResource.updateDate}</td>
					<td>
						<a class="btn btn-default" href="${ctx}/admin/teamResource/view?id=${teamResource.id}">查看明细</a>
						<a class="btn btn-default" href="${ctx}/admin/teamResource/delete?id=${teamResource.id}">删除</a>
						<a class="btn btn-default" href="${ctx}/admin/teamResource/update?id=${teamResource.id}">更新资源</a>
						<a class="btn btn-default" href="${ctx}${teamResource.resourceUrl}">下载资源</a>
					</td>
				</tr>
			</c:forEach>

		</table>
		<div class="tableBottom">
			<div class="table-desc form-group">
				<label class="control-label">每页</label>
				<select class="pageSizeSelect" name="pageSize">
					<option <c:if test="${pageInfo.pageSize == '10' }">selected</c:if> value="10">10条</option>
					<option <c:if test="${pageInfo.pageSize == '20' }">selected</c:if> value="20">20条</option>
					<option <c:if test="${pageInfo.pageSize == '50' }">selected</c:if> value="50">50条</option>
				</select>
				<label class="control-label">条记录 第${pageInfo.pageNo }页 共${pageInfo.totalPage }页 总共${pageInfo.allRow }条记录</label>
			</div>
			<%--<div id="pageDiv" class="pageDiv">
				 <ul class="pagination pagination-sm">
                    <c:forEach begin="1" end="${pageInfo.totalPage }"  var="i">
                        <li><a class="hrefClass" href="#">${i }</a></li>
                    </c:forEach>
                </ul>

			</div>--%>
		</div>
		<div id="pageTest">

		</div>

	</div>

	</div>
</div>
	<script type="text/javascript">
		$(function () {
			$(".hrefClass").click(function(){
				var pageNo = $(this).text();
				var pageSize= $(".pageSizeSelect").find("option:selected").val();
				$("#pageNo").val(pageNo);
				$("#pageSize").val(pageSize);
				$("#teamResourceForm").attr("action","${ctx }/admin/teamResource/list");
				$("#teamResourceForm").submit();
			});
		});
	</script>
	<script type="text/javascript" src="${ctx }/resources/js/layui.all.js"></script>
	<script type="text/javascript">
		layui.use(['laypage', 'layer'], function(){
			var laypage = layui.laypage
					,layer = layui.layer;

			//调用分页
			laypage({
				cont: 'pageTest'
				,pages: ${pageInfo.totalPage } //总页数
				,curr: ${pageInfo.pageNo }
				,skin: '#5FB878' //自定义选中色值
				,skip: true //开启跳页
				,jump: function(obj, first){
					if(!first){
						layer.msg('第'+ obj.curr +'页');
						var pageNo = obj.curr;
						var pageSize= $(".pageSizeSelect").find("option:selected").val();
						$("#pageNo").val(pageNo);
						$("#pageSize").val(pageSize);
						$("#teamResourceForm").attr("action","${ctx }/admin/teamResource/list");
						$("#teamResourceForm").submit();
					}
				}
			});

		});
	</script>
</body>
</html>