package com.lidx.back.statistics.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Maps;
import com.lidx.back.statistics.dao.IAppInitDao;
import com.lidx.back.statistics.dao.IDictionaryDao;
import com.lidx.back.statistics.entity.ClientInfo;
import com.lidx.back.statistics.service.IAppInitService;
import com.lidx.back.statistics.utils.JedisUtils;
@Service
@Transactional
public class AppInitServiceImpl implements IAppInitService {

	@Autowired
	private IAppInitDao appInitDao;
	@Autowired
	private IDictionaryDao dictionaryDao;
	
	@Transactional(readOnly=false)
	public void saveClientInfo(ClientInfo info) throws Exception {
		//info.setRecordTime(new Date());
		appInitDao.insertClientInfo(info);

	}

	@Transactional
	public boolean checkInfoIfExists(ClientInfo info) {
		/*String sexCode = dictionaryDao.getCodeByValue(info.getSex());
		info.setSex(sexCode);*/
		ClientInfo info2 = appInitDao.getClientInfo(info);
		if(null != info2)
			return true;
		return false;
	}

	public ClientInfo getClientInfoByDeviceId(String deviceId) {
 		return appInitDao.getClientInfo(deviceId);
	}

	@Transactional(readOnly=true)
	public String getClientSexInfoByDeviceId(String deviceId) {
		/*ClientInfo ci = appInitDao.getClientInfo(deviceId);
		if(null != ci){
			return ci.getSex();
		}else{
			return null;
		}*/
		/*Map<String, String> map = JedisUtils.getMap(JedisUtils.KEY_PREFIX+"clientsSexMap");
		if(null == map){
			Map<String, String> newMap = Maps.newHashMap();
			
		}else{
			if(map.containsKey(deviceId)){
				return map.get(deviceId);
			}else{
				ClientInfo ci = appInitDao.getClientInfo(deviceId);
				if(null != ci){
					map.put(deviceId, ci.getSex());
					JedisUtils.setMap(JedisUtils.KEY_PREFIX+"clientsSexMap", map, 60*60);
					return ci.getSex();
				}
				return null;
			}
		}*/
		return null;
	}

}
