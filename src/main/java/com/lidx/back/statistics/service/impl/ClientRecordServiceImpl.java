package com.lidx.back.statistics.service.impl;

import com.lidx.back.statistics.dao.IClientRecordDao;
import com.lidx.back.statistics.entity.ClientRecord;
import com.lidx.back.statistics.service.IClientRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
@Service
@Transactional(readOnly=true)
public class ClientRecordServiceImpl implements IClientRecordService {

	@Autowired
	private IClientRecordDao recordDao;
	@Transactional(readOnly=false)
	public void saveClientRecord(ClientRecord record) {
		record.setRecordTime(new Date());
		recordDao.insertClientRecord(record);
	}

	public List<ClientRecord> getClientRecords() {
		return recordDao.getRecords();
	}

	public List<Object[]> getRecordsWithParams(String action,
			String versionNumber,String versionType, String from, String to,String online) {
		// TODO Auto-generated method stub
		return recordDao.getRecordsWithParams(action,versionType,versionNumber,from,to,online);
	}

	public List<Object[]> getSexPercentage(String versionType, String versionNumber,
			String from,String to,String online) {
		return recordDao.getSexPercentage(versionType,versionNumber,from,to,online);
	}

	public Object[] getVideoRecordInfo(String versionType, String versionNumber,
			String from,String to,String online) {
		return recordDao.getVideoRecordInfo(versionType,versionNumber,from,to,online);
	}

	public List<ClientRecord> getClientRecords(String versionType, String versionNumber, String from, String to) {
		return recordDao.getRecords(versionType,versionNumber,from,to);
	}

	public int getClientRecordsNumber(String versionType, String versionNumber, String from, String to) {
		return getClientRecords(versionType,versionNumber,from,to).size();
	}

}
