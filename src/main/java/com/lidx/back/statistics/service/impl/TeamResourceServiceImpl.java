package com.lidx.back.statistics.service.impl;

import com.lidx.back.statistics.commons.PageInfo;
import com.lidx.back.statistics.dao.ITeamResourceDao;
import com.lidx.back.statistics.entity.TeamResource;
import com.lidx.back.statistics.service.ITeamResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by chen on 2017/2/17.
 */
@Service
@Transactional
public class TeamResourceServiceImpl implements ITeamResourceService {

    @Autowired
    private ITeamResourceDao teamResourceDao;
    @Override
    public void saveTeamResource(TeamResource teamResource) {
        teamResource.setSaveDate(new Date());
        teamResourceDao.insertTeamResource(teamResource);
    }

    @Override
    public List<TeamResource> findTeamResourceList() {
        return teamResourceDao.findTeamResourceList();
    }

    @Override
    public PageInfo findTeamResourcePage(int pageNo, int pageSize,String resourceCategory) {
        return teamResourceDao.findTeamResourcePage(pageNo,pageSize,resourceCategory);
    }

    @Override
    public TeamResource getTeamResourceById(int id) {
        return teamResourceDao.getTeamResourceById(id);
    }

    @Override
    public void deleteTeamResource(int id) {
        teamResourceDao.deleteTeamResourceById(id);
    }

    @Override
    public void updateTeamResource(TeamResource teamResource) {
        teamResource.setUpdateDate(new Date());
        teamResourceDao.updateTeamResource(teamResource);
    }
}
