package com.lidx.back.statistics.service;

import java.util.List;

import com.lidx.back.statistics.entity.Role;
import com.lidx.back.statistics.entity.SysUser;

public interface ISysUserService {
	
	SysUser getUserByLoginName(String loginName);

	SysUser getUserByName(String name);

	SysUser getUserById(int id);

	int saveSysUser(SysUser user);
	
	List<Role> getUserRoles();
	
	List<Role> getUserRolesByUserName(String loginName);
	
	List<SysUser> getUserList();
	
	int updateUserLoginInfo(int userId);

	void deleteUser(int userId);

	List<String> getUserNames();

	void updateSysUser(SysUser user);
}
