package com.lidx.back.statistics.service;

import com.lidx.back.statistics.entity.ClientInfo;

import java.util.List;

public interface IClientService {
	void callCloseSound(ClientInfo clientInfo);
	void callOpenFlashLight(ClientInfo clientInfo);
	void callSwitchCamera(ClientInfo clientInfo);
	void callSwitchFilter(ClientInfo clientInfo);
	void callRecordVideo(ClientInfo clientInfo);
	void callLinkShare(ClientInfo clientInfo);
	void callSaveVideo(ClientInfo clientInfo);
	void callSavePicture(ClientInfo clientInfo);
	void callShareVideo(ClientInfo clientInfo);
	void callSharePic(ClientInfo clientInfo);
	
	List<ClientInfo> getClientInfos(String versionType,String versionNumber);
	
}
