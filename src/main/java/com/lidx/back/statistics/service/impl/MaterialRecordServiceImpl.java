package com.lidx.back.statistics.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.lidx.back.statistics.commons.Constants;
import com.lidx.back.statistics.entity.record.MaterialShareRecord;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.lidx.back.statistics.dao.IMaterialDao;
import com.lidx.back.statistics.dao.IMaterialRecordDao;
import com.lidx.back.statistics.entity.MaterialRecord;
import com.lidx.back.statistics.service.IMaterialRecordService;
@Service
@Transactional
public class MaterialRecordServiceImpl implements IMaterialRecordService {

	@Autowired
	private IMaterialRecordDao materialRecordDao;
	
	@Transactional(readOnly=false)
	public void saveRecord(MaterialRecord record) {
		record.setRecordTime(new Date());
		materialRecordDao.insertRecord(record);

	}

	public List<MaterialRecord> getMaterialRecords() {
		return materialRecordDao.getRecords();
	}

	public List<Object[]> getRecordsWithParams(String action, String versionType,
			String versionNumber, String from, String to,String online,String order) {
		return materialRecordDao.getRecordsWithParams(action, versionType, versionNumber,from, to,online,order);
	}

	public List<Object[]> getRecordsWithChannel(String action,
			String versionType,String versionNumber, String from, String to,String online) {
		return materialRecordDao.getRecordsWithChannel(action, versionType,versionNumber, from, to,online);
	}

	/*public Map<String, List<Object[]>> getRecordsWithMaterialChannel(String action,
			String versionType, String versionNumber, String from, String to) {
		List<Object[]> list = materialRecordDao.getRecordsWithMaterialChannel(action, versionType,versionNumber, from, to);
		Map<String, List<Object[]>> map = Maps.newHashMap();
		String uuid = "";
		List<Object[]> objects = null;
		for(Object[] o : list){
			uuid = o[0].toString();
			if(map.get(uuid) == null){
				objects = Lists.newArrayList();
				objects.add(o);
				map.put(uuid, objects);
			}else{
				map.get(uuid).add(o);
			}
		}
		return map;
	}*/

	public List<MaterialShareRecord> getRecordsWithMaterialChannel(String action,
			String versionType, String versionNumber, String from, String to,String online,String order) {

		List<Object[]> materialRecords = materialRecordDao.getRecordsWithMaterialChannel(action,
				versionType,versionNumber, from, to,online);
		//List<MaterialShareRecord> shareRecords = Lists.newArrayList();
		MaterialShareRecord shareRecord = null;
		Map<String,MaterialShareRecord> uuidRecordMap = Maps.newHashMap();
		for(int i=0;i<materialRecords.size();i++){
			Object[] record = materialRecords.get(i);
			String uuid = record[0].toString();
			String channel = record[3].toString();
			String channelNum = record[4].toString();

			if(uuidRecordMap.containsKey(uuid)){
				shareRecord = uuidRecordMap.get(uuid);
				if(!StringUtils.isEmpty(channelNum)){
					shareRecord.setTotalNum(shareRecord.getTotalNum() + Integer.parseInt(channelNum));
					shareRecord = buidShareRecord(channel,channelNum,shareRecord);
					uuidRecordMap.put(uuid,shareRecord);
				}
                continue;
			}
			shareRecord = new MaterialShareRecord();
			shareRecord.setUuid(record[0].toString());
			shareRecord.setVersionType(record[1].toString());
			shareRecord.setVersionNum(record[2].toString());

			shareRecord = buidShareRecord(channel,channelNum,shareRecord);

			if(!StringUtils.isEmpty(channelNum)){
				shareRecord.setTotalNum(shareRecord.getTotalNum()+Integer.parseInt(channelNum));
			}
			uuidRecordMap.put(uuid,shareRecord);
		}
		List<MaterialShareRecord> sortedShareRecords = Lists.newArrayListWithExpectedSize(uuidRecordMap.size());
		for(MaterialShareRecord msr : uuidRecordMap.values()){
			sortedShareRecords.add(msr);
		}
		if(StringUtils.isNotBlank(order)){
			for(int i=0;i<sortedShareRecords.size();i++){
				MaterialShareRecord ms = sortedShareRecords.get(i);
				int num = ms.getTotalNum();
				for(int j=i+1;j<sortedShareRecords.size();j++){
					MaterialShareRecord ms2 = sortedShareRecords.get(j);
					int num2 = ms2.getTotalNum();
					if(order.equals("asc")){
						if(num > num2){
							sortedShareRecords.set(i,ms2);
							sortedShareRecords.set(j,ms);
						}
					}else if(order.equals("desc")){
						if(num < num2){
							sortedShareRecords.set(i,ms2);
							sortedShareRecords.set(j,ms);
						}
					}
				}
			}
		}
		return sortedShareRecords;
	}

	private MaterialShareRecord buidShareRecord(String channel,String channelNum,MaterialShareRecord shareRecord){
		if(Constants.SHARE_CHANNEL_WEIXIN.equals(channel)){
			if(StringUtils.isEmpty(channelNum)){
				shareRecord.setWeiXinNum(0);
			}else{
				shareRecord.setWeiXinNum(Integer.parseInt(channelNum));
			}
		}else if(Constants.SHARE_CHANNEL_QQ.equals(channel)){
			if(StringUtils.isEmpty(channelNum)){
				shareRecord.setQqNum(0);
			}else{
				shareRecord.setQqNum(Integer.parseInt(channelNum));
			}
		}else if(Constants.SHARE_CHANNEL_WEIBO.equals(channel)){
			if(StringUtils.isEmpty(channelNum)){
				shareRecord.setWeiBoNum(0);
			}else{
				shareRecord.setWeiBoNum(Integer.parseInt(channelNum));
			}
		}else if(Constants.SHARE_CHANNEL_WXPYQ.equals(channel)){
			if(StringUtils.isEmpty(channelNum)){
				shareRecord.setPyqNum(0);
			}else{
				shareRecord.setPyqNum(Integer.parseInt(channelNum));
			}
		}else{
			//do nothing
		}
		return shareRecord;
	}

}
