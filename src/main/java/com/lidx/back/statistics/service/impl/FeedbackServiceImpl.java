package com.lidx.back.statistics.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lidx.back.statistics.commons.PageInfo;
import com.lidx.back.statistics.dao.IFeedbackDao;
import com.lidx.back.statistics.entity.FeedBack;
import com.lidx.back.statistics.service.IFeedbackService;
@Service
@Transactional
public class FeedbackServiceImpl implements IFeedbackService {

	@Autowired
	private IFeedbackDao feedbackDao;
	
	public void saveFeedback(FeedBack feedBack) throws Exception {
		feedBack.setFeedbackTime(new Date());
		feedbackDao.insertFeedback(feedBack);
	}

	public List<FeedBack> getFeedBacks() {
		return feedbackDao.getFeedBacks();
	}

	public PageInfo queryFeedbacks(int pageNo, int pageSize) {
		// TODO Auto-generated method stub
		return feedbackDao.queryFeedbackPage(pageNo,pageSize);
	}

}
