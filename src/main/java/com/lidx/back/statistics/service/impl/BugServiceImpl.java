package com.lidx.back.statistics.service.impl;

import com.lidx.back.statistics.commons.PageInfo;
import com.lidx.back.statistics.dao.IBugDao;
import com.lidx.back.statistics.entity.Bug;
import com.lidx.back.statistics.service.IBugService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by chen on 2017/2/18.
 */
@Service
@Transactional
public class BugServiceImpl implements IBugService {

    @Autowired
    private IBugDao bugDao;

    @Override
    public void saveBug(Bug bug) {
        bug.setSubmitDate(new Date());
        bug.setBugStatus("0");
        bugDao.insertBug(bug);
    }

    @Override
    public List<Bug> findBugs() {
        return bugDao.findBugs();
    }

    @Override
    public boolean dealedBug(int id,String operator) {
        Bug bug = bugDao.getBugById(id);
        if(!bug.getReceiverName().equals(operator)){
            throw new RuntimeException("只有受理bug的人才有权限解决bug，受理bug的人的姓名："+bug.getReceiverName());
        }
        bug.setBugStatus("1");
        bug.setDealedDate(new Date());
        return bugDao.updateBug(bug);
    }

    @Override
    public Bug getBugById(int id) {
        return bugDao.getBugById(id);
    }

    @Override
    public PageInfo findBugsPage(int pageNo, int pageSize) {
        return bugDao.findBugsPage(pageNo,pageSize);
    }

    @Override
    public boolean deleteBug(int id) {
        Bug bug = getBugById(id);
        if(bug == null){
            return false;
        }
        bugDao.deleteBug(bug);
        return true;
    }
}
