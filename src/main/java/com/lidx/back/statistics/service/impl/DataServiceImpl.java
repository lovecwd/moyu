package com.lidx.back.statistics.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lidx.back.statistics.dao.IDataDao;
import com.lidx.back.statistics.entity.FeedBack;
import com.lidx.back.statistics.service.IDataService;

@Service
public class DataServiceImpl implements IDataService{
	@Autowired
	private IDataDao dataDao;

	public List<FeedBack> getBacks() {
		// TODO Auto-generated method stub
		return dataDao.getBacks();
	}
	
}
