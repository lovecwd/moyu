package com.lidx.back.statistics.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lidx.back.statistics.dao.ISysUserDao;
import com.lidx.back.statistics.entity.Role;
import com.lidx.back.statistics.entity.SysUser;
import com.lidx.back.statistics.service.ISysUserService;
@Service
@Transactional
public class SysUserServiceImpl implements ISysUserService {

	@Autowired
	private ISysUserDao userDao;
	
	@Transactional(readOnly=false)
	public int saveSysUser(SysUser user) {
		user.setCreateDate(new Date());
		return userDao.saveSysUser(user);
	}

	public List<Role> getUserRoles() {
		return userDao.getRoles();
	}

	public List<SysUser> getUserList() {
		return userDao.getUserList();
	}

	public SysUser getUserByLoginName(String loginName) {
		return userDao.getUserByLoginName(loginName);
	}

	@Override
	public SysUser getUserByName(String name) {
		return userDao.getUserByName(name);
	}

	@Override
	public SysUser getUserById(int id) {
		return userDao.getUserById(id);
	}

	public List<Role> getUserRolesByUserName(String loginName) {
		return userDao.getUserRoleList(loginName);
	}

	public int updateUserLoginInfo(int userId) {
		return userDao.updateLoginInfo(SecurityUtils.getSubject().getSession().getHost(), new Date(), userId);
	}

	@Transactional(readOnly=false)
	public void deleteUser(int userId) {
		userDao.deleteUser(userId);
	}

	@Override
	public List<String> getUserNames() {
		return userDao.findUserNames();
	}

	@Override
	public void updateSysUser(SysUser user) {
		SysUser userDB = getUserById(user.getId());
		userDB.setLoginName(user.getLoginName());
		userDB.setName(user.getName());
		userDB.setPassword(user.getPassword());
		userDao.updateSysUser(userDB);
	}

}
