package com.lidx.back.statistics.service;

import com.lidx.back.statistics.commons.PageInfo;
import com.lidx.back.statistics.entity.Bug;

import java.util.List;

/**
 * Created by chen on 2017/2/18.
 */
public interface IBugService {

    void saveBug(Bug bug);

    List<Bug> findBugs();

    boolean dealedBug(int id, String operator);

    Bug getBugById(int id);

    PageInfo findBugsPage(int pageNo,int pageSize);

    boolean deleteBug(int id);
}
