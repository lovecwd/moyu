package com.lidx.back.statistics.commons;

public class MaterialAction {
	
	public static final String MATERIAL_DOWNLOAD = "download";
	
	public static final String MATERIAL_MAKE_VIDEO = "makeVideo";
	
	public static final String MATERIAL_PHOTOGRAPH = "photograph";
	
	public static final String MATERIAL_PIC_SHARE = "picShare";
	public static final String MATERIAL_PIC_DOWNLOAD = "picDownload";

	public static final String MATERIAL_VIDEO_SHARE = "videoShare";
	public static final String MATERIAL_VIDEO = "video";
	public static final String MATERIAL_VIDEO_DOWNLOAD = "videoDownload";

	
}
