package com.lidx.back.statistics.commons;

public class MaterialUpdateConfig {
	private String localWorkspace;

	private String svnUserName;
	private String svnPassword;

	private String configFileName;

	private String iisFtpHost;

	private String iisFtpRoot;
	private String ftpUserName;
	private String ftpPassword;

	private String iisURLRoot;
	
	private String ftpLocalRoot;
	
	public String getFtpLocalRoot() {
		return ftpLocalRoot;
	}

	public void setFtpLocalRoot(String ftpLocalRoot) {
		this.ftpLocalRoot = ftpLocalRoot;
	}

	public String getLocalWorkspace() {
		return localWorkspace;
	}

	public void setLocalWorkspace(String localWorkspace) {
		this.localWorkspace = localWorkspace;
	}

	public String getSvnUserName() {
		return svnUserName;
	}

	public void setSvnUserName(String svnUserName) {
		this.svnUserName = svnUserName;
	}

	public String getSvnPassword() {
		return svnPassword;
	}

	public void setSvnPassword(String svnPassword) {
		this.svnPassword = svnPassword;
	}

	public String getConfigFileName() {
		return configFileName;
	}

	public void setConfigFileName(String configFileName) {
		this.configFileName = configFileName;
	}

	public String getIisFtpHost() {
		return iisFtpHost;
	}

	public void setIisFtpHost(String iisFtpHost) {
		this.iisFtpHost = iisFtpHost;
	}

	public String getIisFtpRoot() {
		return iisFtpRoot;
	}

	public void setIisFtpRoot(String iisFtpRoot) {
		this.iisFtpRoot = iisFtpRoot;
	}

	public String getFtpUserName() {
		return ftpUserName;
	}

	public void setFtpUserName(String ftpUserName) {
		this.ftpUserName = ftpUserName;
	}

	public String getFtpPassword() {
		return ftpPassword;
	}

	public void setFtpPassword(String ftpPassword) {
		this.ftpPassword = ftpPassword;
	}

	public String getIisURLRoot() {
		return iisURLRoot;
	}

	public void setIisURLRoot(String iisURLRoot) {
		this.iisURLRoot = iisURLRoot;
	}

}
