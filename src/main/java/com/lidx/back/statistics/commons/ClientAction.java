package com.lidx.back.statistics.commons;

public class ClientAction {

	public static final String CLIENT_CLOSE_SOUND = "closeSound";
	
	public static final String CLIENT_OPEN_FLASH_LIGHT = "openFlashLight";
	
	public static final String CLIENT_SWITCH_CAMERA = "switchCamera";
	
	public static final String CLIENT_SWITCH_FILTER = "switchFilter";
	
	public static final String CLIENT_RECORD_VIDEO = "recordVideo";
	
	public static final String CLIENT_LINK_SHARE = "linkShare";
	
	public static final String CLIENT_LINK_OPEN = "linkOpen";
	
	public static final String CLIENT_SAVE_VIDEO = "saveVideo";
	
	public static final String CLIENT_SAVE_PIC ="savePicture";
	
	public static final String CLIENT_SHARE_VIDEO = "shareVideo";
	
	public static final String CLIENT_SHARE_PIC = "sharePicture";
	
	public static final String CLIENT_SEX_PERCENTAGE = "sexPercentage";
}
