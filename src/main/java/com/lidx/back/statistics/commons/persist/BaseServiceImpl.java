package com.lidx.back.statistics.commons.persist;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.lidx.back.statistics.entity.MaterialPO;

@Repository
public class BaseServiceImpl implements IBaseService {

	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("deprecation")
	public void clearSession() {
		sessionFactory.evict(MaterialPO.class);
	}

}
