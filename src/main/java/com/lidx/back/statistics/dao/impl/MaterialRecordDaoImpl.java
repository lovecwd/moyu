package com.lidx.back.statistics.dao.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.FlushMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.lidx.back.statistics.dao.IMaterialRecordDao;
import com.lidx.back.statistics.entity.MaterialRecord;
@Repository
public class MaterialRecordDaoImpl implements IMaterialRecordDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public Session getSession(){
		return sessionFactory.getCurrentSession();
	}
	
	public void insertRecord(MaterialRecord record) {
		getSession().save(record);
		getSession().flush();
		//getSession().clear();
	}

	public List<MaterialRecord> getRecords() {
		return getSession().createQuery("from MaterialRecord").list();
	}

	public List<Object[]> getRecordsWithParams(String action, String versionType,
			String versionNumber,String from, String to,String online,String order) {
		StringBuffer sb = new StringBuffer();
		sb.append("select mr.uuid,mr.versionType,mr.versionNum,count(1) as recordNum from MaterialRecord mr where 1=1");
		if(StringUtils.isNoneBlank(action)){
			sb.append(" and mr.action = '").append(action).append("'");
		}
		if(StringUtils.isNotBlank(versionType)){
			sb.append(" and mr.versionType = '").append(versionType).append("'");
		}
		if(StringUtils.isNotBlank(versionNumber)){
			sb.append(" and mr.versionNum = '").append(versionNumber).append("'");
		}
		if(StringUtils.isNotBlank(online)){
			sb.append(" and mr.online= '").append(online).append("'");
		}
		if(StringUtils.isNotBlank(from) && StringUtils.isNotBlank(to)){
			sb.append(" and mr.recordTime between '").append(from).append("' and '").append(to).append("'");
		}
		sb.append("group by mr.uuid");
        if(StringUtils.isNotBlank(order)){
            sb.append(" order by recordNum " + order);
        }
		Query query = getSession().createQuery(sb.toString());
		return query.list();
	}

	public List<Object[]> getRecordsWithChannel(String action,
			String versionType,String versionNumber, String from, String to,String online) {
		StringBuffer sb = new StringBuffer();
		sb.append("select mr.channel,mr.versionType,mr.versionNum,count(1) from MaterialRecord mr where 1=1");
		if(StringUtils.isNoneBlank(action)){
			sb.append(" and mr.action = '").append(action).append("'");
		}
		if(StringUtils.isNotBlank(versionType)){
			sb.append(" and mr.versionType = '").append(versionType).append("'");
		}
		if(StringUtils.isNotBlank(versionNumber)){
			sb.append(" and mr.versionNum = '").append(versionNumber).append("'");
		}
		if(StringUtils.isNotBlank(online)){
			sb.append(" and mr.online= '").append(online).append("'");
		}
		if(StringUtils.isNotBlank(from) && StringUtils.isNotBlank(to)){
			sb.append(" and mr.recordTime between '").append(from).append("' and '").append(to).append("'");
		}
		sb.append("group by mr.channel");
		Query query = getSession().createQuery(sb.toString());
		return query.list();
	}

	public List<Object[]> getRecordsWithMaterialChannel(String action,
			String versionType, String versionNumber, String from, String to,String online) {
		StringBuffer sb = new StringBuffer();
		sb.append("select mr.uuid,mr.versionType,mr.versionNum,mr.channel,count(1) as recordNum " +
                "from MaterialRecord mr where 1=1");
		if(StringUtils.isNoneBlank(action)){
			sb.append(" and mr.action = '").append(action).append("'");
		}
		if(StringUtils.isNotBlank(versionType)){
			sb.append(" and mr.versionType = '").append(versionType).append("'");
		}
		if(StringUtils.isNotBlank(versionNumber)){
			sb.append(" and mr.versionNum = '").append(versionNumber).append("'");
		}
		if(StringUtils.isNotBlank(online)){
			sb.append(" and mr.online= '").append(online).append("'");
		}
		if(StringUtils.isNotBlank(from) && StringUtils.isNotBlank(to)){
			sb.append(" and mr.recordTime between '").append(from).append("' and '").append(to).append("'");
		}
		//sb.append("group by mr.uuid,mr.channel");
		sb.append("group by mr.uuid,mr.channel");
		Query query = getSession().createQuery(sb.toString());
		return query.list();
	}

}
