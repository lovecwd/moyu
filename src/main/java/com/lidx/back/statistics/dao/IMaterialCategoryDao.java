package com.lidx.back.statistics.dao;

import com.lidx.back.statistics.entity.MaterialCategory;

import java.util.List;

public interface IMaterialCategoryDao {

	void insertMaterialCategory(MaterialCategory category) throws Exception;
	
	void deleteMaterialCategory(MaterialCategory category);
	
	List<MaterialCategory> findCategories(String sex, String versionType, String versionNum);

	void deleteCategory(int id);
	
	void clearSecondQuery();

	List<String> getCategoryNames();

    MaterialCategory getCategoryById(int id);

    void updateCategory(MaterialCategory materialCategory);
}
