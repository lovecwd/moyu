package com.lidx.back.statistics.dao;

import java.util.List;

import com.lidx.back.statistics.entity.MaterialCount;
import com.lidx.back.statistics.entity.MaterialDownload;
import com.lidx.back.statistics.entity.MaterialMakeVideo;


public interface IMaterialCountDao {

	void updateMaterialDownloadCount(MaterialCount count);
	
	void updateMakeVideoNum(MaterialCount count);
	
	void updatePhotographNum(MaterialCount count);
	
	void updatePicShareNum(MaterialCount count);
	
	void updateVideoShareNum(MaterialCount count);
	
	List<MaterialCount> getCounts(String versionType);
	
	void saveMaterialDownload(MaterialDownload materialDownload);
	
	void insertMaterialMakeVideo(MaterialMakeVideo makeVideo);
	
}
