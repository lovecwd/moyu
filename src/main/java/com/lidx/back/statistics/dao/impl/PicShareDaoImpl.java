package com.lidx.back.statistics.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.lidx.back.statistics.dao.IPicShareDao;
import com.lidx.back.statistics.entity.PictureShare;
@Repository
public class PicShareDaoImpl implements IPicShareDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public Session getSession(){
		return sessionFactory.getCurrentSession();
	}
	
	public void insert(PictureShare ps) {
		getSession().save(ps);
	}

	public Map<String, Integer> getSharePicChannel(String versionType) {
		Query query = getSession().createSQLQuery("select channel,count(*) from pic_share where versionType = :versionType group by channel ")
				.setString("versionType", versionType);
		List<Object[]> list = query.list();
		Map<String, Integer> map = new HashMap<String, Integer>();
		for(int i = 0;i<list.size();i++){
			Object[] array = list.get(i);
			map.put(String.valueOf(array[0]), Integer.parseInt(String.valueOf(array[1])));
		}
		return map;
	}

}
