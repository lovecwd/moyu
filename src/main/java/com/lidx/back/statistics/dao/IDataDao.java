package com.lidx.back.statistics.dao;

import java.util.List;

import com.lidx.back.statistics.entity.FeedBack;

public interface IDataDao {

	List<FeedBack> getBacks();
}
