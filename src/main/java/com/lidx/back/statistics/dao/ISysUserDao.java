package com.lidx.back.statistics.dao;

import java.util.Date;
import java.util.List;

import com.lidx.back.statistics.entity.Role;
import com.lidx.back.statistics.entity.SysUser;

public interface ISysUserDao {

	int saveSysUser(SysUser user);
	
	
	List<Role> getRoles();

	List<SysUser> getUserList();


	SysUser getUserByLoginName(String loginName);
	
	SysUser getUserById(Integer id);


	List<Role> getUserRoleList(String loginName);


	int updateLoginInfo(String host, Date date, int userId);


	void deleteUser(int userId);

    List<String> findUserNames();

    void updateSysUser(SysUser user);

    SysUser getUserByName(String name);
}
