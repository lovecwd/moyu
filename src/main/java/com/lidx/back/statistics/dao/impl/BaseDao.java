package com.lidx.back.statistics.dao.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.lang.reflect.ParameterizedType;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.lidx.back.statistics.dao.IBaseDao;
@SuppressWarnings({ "rawtypes", "unchecked" })
public class BaseDao<T> implements IBaseDao<T> {

	@Resource(name = "sessionFactory")
    private SessionFactory sessionFactory;
    protected Class<T> entityClass;
 
    public Session getSession() {
        //需要开启事物，才能得到CurrentSession
        return sessionFactory.getCurrentSession();
    }
    
    public Session getNewSession(){
    	return sessionFactory.openSession();
    }
    
	protected Class getEntityClass() {
        if (entityClass == null) {
            entityClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        }
        return entityClass;
    }
	
	public void save(T t) {
		getSession().save(t);
	}

	public void saveOrUpdate(T t) {
		// TODO Auto-generated method stub
		
	}

	public T load(Serializable id) {
		 T load = (T) this.getSession().load(getEntityClass(), id);
        return load;
	}

	public T get(Serializable id) {
		T load = (T) this.getSession().get(getEntityClass(), id);
        return load;
	}

	public boolean contains(T t) {
		// TODO Auto-generated method stub
		return false;
	}

	public void delete(T t) {
		this.getSession().delete(t);
	}

	public boolean deleteById(Serializable Id) {
		 T t = get(Id);
	     if(t == null){
	         return false;
	     }
	     delete(t);
        return true;
	}

	public void deleteAll(Collection<T> entities) {
		for(Object entity : entities) {
            this.getSession().delete(entity);
        }
	}

	public void queryHql(String hqlString, Object... values) {
		// TODO Auto-generated method stub
		
	}

	public void querySql(String sqlString, Object... values) {
		// TODO Auto-generated method stub
		
	}

	public T getByHQL(String hqlString, Object... values) {
		// TODO Auto-generated method stub
		return null;
	}

	public T getBySQL(String sqlString, Object... values) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<T> getListByHQL(String hqlString, Object... values) {
		Query query = this.getSession().createQuery(hqlString);
        if (values != null)
        {
            for (int i = 0; i < values.length; i++)
            {
                query.setParameter(i, values[i]);
            }
        }
        return query.list();
	}

	public List<T> getListBySQL(String sqlString, Object... values) {
		Query query = this.getSession().createSQLQuery(sqlString);
        if (values != null)
        {
            for (int i = 0; i < values.length; i++)
            {
                query.setParameter(i, values[i]);
            }
        }
        return query.list();
	}

	public void refresh(T t) {
		this.getSession().refresh(t);
		
	}

	public void update(T t) {
		this.getSession().update(t);
	}

	public Long countByHql(String hql, Object... values) {
		Query query = this.getSession().createQuery(hql);
        if(values != null){
            for(int i = 0; i < values.length; i++) {
                query.setParameter(i, values[i]);
            }
        }
        return (Long) query.uniqueResult();
	}

}
