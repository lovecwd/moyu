package com.lidx.back.statistics.dao;

import java.util.List;

import com.lidx.back.statistics.entity.Filter;

public interface IFilterDao {
	void updateFilterUseNum(Filter filter);
	
	void updateFilterMakeNum(Filter filter);
	
	List<Filter> getFilters(String versionType);

	void addFilter(Filter filter);
}
