package com.lidx.back.statistics.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import com.lidx.back.statistics.commons.Constants;
import com.lidx.back.statistics.commons.MaterialUpdateConfig;
import com.lidx.back.statistics.commons.SpringContextHolder;

public class FTPUtils {
	public static boolean uploadFile(String url,int port,String username, String password, String path, String filename, FileInputStream input) {  
	    boolean success = false;  
	    FTPClient ftp = new FTPClient();  
	    try {  
	        int reply;  
	        ftp.connect(url, port);//连接FTP服务器  
	        ftp.setFileType(FTPClient.BINARY_FILE_TYPE);
	        //如果采用默认端口，可以使用ftp.connect(url)的方式直接连接FTP服务器  
	        ftp.login(username, password);//登录  
	        reply = ftp.getReplyCode();  
	        if (!FTPReply.isPositiveCompletion(reply)) {  
	            ftp.disconnect();  
	            return success;  
	        }  
	        ftp.changeWorkingDirectory(path);  
	        ftp.storeFile(filename, input);           
	          
	        input.close();  
	        ftp.logout();  
	        success = true;  
	    } catch (IOException e) {  
	        e.printStackTrace();  
	    } finally {  
	        if (ftp.isConnected()) {  
	            try {  
	                ftp.disconnect();  
	            } catch (IOException ioe) {  
	            	ioe.printStackTrace(); 
	                throw new RuntimeException("关闭FTP连接发生异常！" , ioe); 
	            }  
	        }  
	    }  
	    return success;  
	}
	
	/*public static void main(String[] args) throws IOException {
		//MaterialUpdateConfig config = SpringContextHolder.getBean("materialUpdateConfig");
		File file = new File("d:\\pictures\\1476867235802.jpg");
		FileInputStream is = new FileInputStream(file);
		FTPClient ftpClient = new FTPClient(); 
		 ftpClient.connect("114.55.128.129"); 
		//设置文件类型（二进制） 
        ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE); 
		
		 boolean loginResult = ftpClient.login("magicjoy" , "Lidx2016"); 
		 boolean ftpChangeResult = ftpClient.changeWorkingDirectory(Constants.IIS_URL_FEEDBACKIMAGES_ROOT ); 
         
         ftpClient.storeFile("3.jpg" , is); 
	}*/
}
