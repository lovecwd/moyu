package com.lidx.back.statistics.controller.admin;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.lidx.back.statistics.commons.ClientAction;
import com.lidx.back.statistics.commons.MaterialAction;
import com.lidx.back.statistics.entity.excel.ClientSexExcel;
import com.lidx.back.statistics.service.IClientRecordService;
import com.lidx.back.statistics.service.IClientService;
import com.lidx.back.statistics.service.IMaterialRecordService;
import com.lidx.back.statistics.service.ISystemVersionService;
import org.apache.commons.lang3.StringUtils;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.params.ExcelExportEntity;
import org.jeecgframework.poi.excel.entity.vo.MapExcelConstants;
import org.jeecgframework.poi.excel.entity.vo.NormalExcelConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/admin/client")
public class ClientAdminController {
	@Autowired
	private IClientRecordService recordService;
	@Autowired
	private IClientService clientService;
	@Autowired
	private IMaterialRecordService materialService;
	
	@Autowired
	private ISystemVersionService systemVersionService;

	private static Map<String,String> actionWithText = null;

	/*static {
		actionWithText = Maps.newHashMap();
		actionWithText.put(ClientAction.CLIENT_CLOSE_SOUND,"用户关闭声音的次数");
		actionWithText.put(ClientAction.CLIENT_OPEN_FLASH_LIGHT,"用户开启闪光灯的次数");
		actionWithText.put(ClientAction.CLIENT_SWITCH_CAMERA,"用户切换摄像头的次数");
		actionWithText.put(ClientAction.CLIENT_SWITCH_FILTER,"用户使用滤镜快速切换的次数");
		actionWithText.put(ClientAction.CLIENT_RECORD_VIDEO,"用户录制视频的平均时长");
		actionWithText.put(ClientAction.CLIENT_LINK_SHARE,"用户分享推荐链接数统计");
		actionWithText.put(ClientAction.CLIENT_LINK_OPEN,"推荐链接的打开量");
		actionWithText.put(ClientAction.CLIENT_SAVE_VIDEO,"用户保存视频到本地的次数");
		actionWithText.put(ClientAction.CLIENT_SAVE_PIC,"用户保存照片到本地的次数");
		actionWithText.put(ClientAction.CLIENT_SEX_PERCENTAGE,"用户男女比例");

	}*/
	
	@RequestMapping(value="list",method=RequestMethod.GET)
	public String clientPage(Model model){
		List<String> list = systemVersionService.getVersionNumbers();
		/*if(list != null){
			model.addAttribute("versionNumbers", list);
		}*/
		return "client/list";
	}
	
	@RequestMapping(value="list",method=RequestMethod.POST)
	public ModelAndView goClientPage(@RequestParam(value="clientRecords") String action,
			@RequestParam(required=false) String versionType,
			@RequestParam(required=false) String versionNumber,
									 @RequestParam(required=false)  String online,
			@RequestParam(required=false) String from,
			@RequestParam(required=false) String to){
		ModelAndView mav = new ModelAndView("client/list");
		
		if(StringUtils.isNotBlank(action)){
			mav.addObject("action", action);
			if(StringUtils.isNotBlank(versionType)){
				mav.addObject("versionType", versionType);
			}
			if(StringUtils.isNotBlank(from)&&StringUtils.isNotBlank(to)){
				mav.addObject("from", from);
				mav.addObject("to", to);
			}
			int number = clientService.getClientInfos(versionType,versionNumber).size();
					//recordService.getClientRecordsNumber(versionType,versionNumber,from,to);
			mav.addObject("number",number);
			/*if(action.equalsIgnoreCase(ClientAction.CLIENT_SEX_PERCENTAGE)){
				List<Object[]> sexInfo = recordService.getSexPercentage(versionType,StringUtils.EMPTY,from,to,online);
				if(sexInfo != null){
					mav.addObject("sexInfo", sexInfo);
				}
			}else if(ClientAction.CLIENT_RECORD_VIDEO.equalsIgnoreCase(action)){
				Object[] videoRecordInfo = recordService.getVideoRecordInfo(versionType,StringUtils.EMPTY,from,to,online);
				String[] videoRecordStringInfo = new String[videoRecordInfo.length];
				if(null == videoRecordInfo[1]){
					//videoRecordInfo[0] = "1";
				}else{
					float recordCount =  Float.parseFloat(videoRecordInfo[0].toString()) ;
					float recordAllTime = Float.parseFloat(videoRecordInfo[1].toString());
					float avgRecordTime = recordAllTime/recordCount;
					//videoRecordInfo[0] ="用户视频录制平均时长";
					videoRecordStringInfo[1] = String.valueOf(avgRecordTime);
					videoRecordStringInfo[2] = videoRecordInfo[2].toString();
					videoRecordStringInfo[3] = videoRecordInfo[3].toString();
					mav.addObject("videoRecordInfo", videoRecordStringInfo);
				}
			}
			else{
				List<Object[]> list = recordService.getRecordsWithParams(action,versionType,StringUtils.EMPTY,from,to,online);
				mav.addObject("records", list);
			}*/
		}
		/*List<String> list2 = systemVersionService.getVersionNumbers();
		if(list2 != null){
			mav.addObject("versionNumbers", list2);
		}*/
		return mav;
	}
	
	@RequestMapping(value="exportClientActionExcel",method=RequestMethod.POST)
	public ModelAndView exportClientActionExcelMsg(@RequestParam(value="clientRecords") String action,
			ModelMap modelMap,
			@RequestParam String versionType,
			@RequestParam(required=false) String from,
			//@RequestParam(required=false) String versionNumber,
												   @RequestParam(required=false)  String online,
			@RequestParam(required=false) String to){
		ModelAndView mav = new ModelAndView(MapExcelConstants.JEECG_MAP_EXCEL_VIEW);
		if(StringUtils.isNotBlank(action)){
			if(action.equalsIgnoreCase(ClientAction.CLIENT_SEX_PERCENTAGE)){
				mav = new ModelAndView(NormalExcelConstants.JEECG_EXCEL_VIEW);
				List<Object[]> sexInfo = recordService.getSexPercentage(versionType,StringUtils.EMPTY,from,to,online);
				if(sexInfo != null){
					List<ClientSexExcel> entityList = new ArrayList<ClientSexExcel>();
					ClientSexExcel cs = null;
					for (Object[] o : sexInfo){
						cs = new ClientSexExcel();
						cs.setSex(o[0].toString());
						cs.setVersionType(o[1].toString());
						cs.setVersionNumber(o[2].toString());
						cs.setNumber(o[3].toString());
						entityList.add(cs);
					}
					modelMap.put(NormalExcelConstants.DATA_LIST, entityList);
					modelMap.put(NormalExcelConstants.CLASS, ClientSexExcel.class);
					//modelMap.put(MapExcelConstants.MAP_LIST, dataResult);
					modelMap.put(NormalExcelConstants.FILE_NAME, "用户行为相关数据统计");
					modelMap.put(NormalExcelConstants.PARAMS, new ExportParams(actionWithText.get(action), action));
				}
				return mav;
			}


			List<Object[]> list = recordService.getRecordsWithParams(action,versionType,StringUtils.EMPTY,from,to,online);
			
			List<Map<String, Object>> mapList = Lists.newArrayList();
			Map<String, Object> map = null;
			for(Object[] objects : list){
				map = Maps.newHashMap();
				map.put("action", objects[0]);
				map.put("versionType", objects[1]);
				map.put("versionNumber", objects[2]);
				map.put("num", objects[3]);
				mapList.add(map);
			}
			/*List<ClientExcel> entityList = new ArrayList<ClientExcel>();
			ClientExcel ce = null;
			for(Object[] objects : list){
				ce = new ClientExcel();
				ce.setAction(objects[0].toString());
				ce.setVersionType(objects[1].toString());
				ce.setNum(objects[2].toString());
				entityList.add(ce);
			}*/
			List<ExcelExportEntity> entityList = new ArrayList<ExcelExportEntity>();
			entityList.add(new ExcelExportEntity("用户行为", "action", 35));
			entityList.add(new ExcelExportEntity("设备版本信息", "versionType", 15));
			entityList.add(new ExcelExportEntity("APP版本号", "versionNumber", 15));
			entityList.add(new ExcelExportEntity("相关数量", "num", 15));
			
			modelMap.put(MapExcelConstants.ENTITY_LIST, entityList);
			modelMap.put(MapExcelConstants.MAP_LIST, mapList);
			//modelMap.put(MapExcelConstants.MAP_LIST, dataResult);
			modelMap.put(MapExcelConstants.FILE_NAME, "用户行为数据统计");
			modelMap.put(MapExcelConstants.PARAMS, new ExportParams(actionWithText.get(action), action));
		}else {
			mav = new ModelAndView("client/list");
		}
		
		return mav;
	}
	
	@RequestMapping(value="shareData",method=RequestMethod.GET)
	public String sharePage(Model model){
		List<String> list = systemVersionService.getVersionNumbers();
		/*if(list != null){
			model.addAttribute("versionNumbers", list);
		}*/
		return "client/share";
	}
	
	@RequestMapping(value="shareData",method=RequestMethod.POST)
	public ModelAndView goSharePage(@RequestParam(value="clientRecords") String action,
			@RequestParam String versionType,
			//@RequestParam(required=false) String versionNumber,
									@RequestParam(required=false)  String online,
			@RequestParam(required=false) String from,
			@RequestParam(required=false) String to){
		ModelAndView mav = new ModelAndView("client/share");
		if(StringUtils.isNotBlank(action) && StringUtils.isNoneBlank(versionType)){
			mav.addObject("action", action);
			mav.addObject("versionType", versionType);
			if(StringUtils.isNotBlank(online)){
				if(!online.equals("y") && !online.equals("n")){
					online = "y";
				}
				mav.addObject("online", online);
			}
			//if(StringUtils.isNoneBlank(to)&& StringUtils.isNoneBlank(from)){
			if(StringUtils.isNotBlank(from)&&StringUtils.isNotBlank(to)){
				mav.addObject("from", from);
				mav.addObject("to", to);
			}
			if(MaterialAction.MATERIAL_PIC_SHARE.equals(action) || MaterialAction.MATERIAL_VIDEO_SHARE.equals(action)){
				List<Object[]> list = materialService.getRecordsWithChannel(action,versionType,StringUtils.EMPTY,from,to,online);
				mav.addObject("records", list);
			}
		}
		List<String> list = systemVersionService.getVersionNumbers();
		if(list != null){
			mav.addObject("versionNumbers", list);
		}
		return mav;
	}
	
	@RequestMapping(value="exportClientShareData",method=RequestMethod.POST)
	public ModelAndView exportClientShareData(@RequestParam(value="clientRecords") String action,
			ModelMap modelMap,
			@RequestParam String versionType,
			//@RequestParam(required=false) String versionNumber,
											  @RequestParam(required=false)  String online,
			@RequestParam(required=false) String from,
			@RequestParam(required=false) String to){
		ModelAndView mav = new ModelAndView(MapExcelConstants.JEECG_MAP_EXCEL_VIEW);
		if(StringUtils.isNotBlank(action) && StringUtils.isNoneBlank(versionType)){
			//List<Object[]> list = recordService.getRecordsWithParams(action,versionType,from,to);
			
			List<Map<String, Object>> mapList = Lists.newArrayList();
			Map<String, Object> map = null;
			if(MaterialAction.MATERIAL_PIC_SHARE.equals(action) || MaterialAction.MATERIAL_VIDEO_SHARE.equals(action)){
				List<Object[]> list = materialService.getRecordsWithChannel(action,versionType,StringUtils.EMPTY,from,to,online);
				for(Object[] objects : list){
					map = Maps.newHashMap();
					map.put("channel", objects[0]);
					map.put("versionType", objects[1]);
					map.put("versionNumber", objects[2]);
					map.put("num", objects[3]);
					mapList.add(map);
				}
			}
			
			List<ExcelExportEntity> entityList = new ArrayList<ExcelExportEntity>();
			entityList.add(new ExcelExportEntity("分享渠道", "channel", 25));
			entityList.add(new ExcelExportEntity("设备版本信息", "versionType", 15));
			entityList.add(new ExcelExportEntity("分享数量", "num", 15));
			
			modelMap.put(MapExcelConstants.ENTITY_LIST, entityList);
			modelMap.put(MapExcelConstants.MAP_LIST, mapList);
			//modelMap.put(MapExcelConstants.MAP_LIST, dataResult);
			modelMap.put(MapExcelConstants.FILE_NAME, "用户分享数据统计");
			modelMap.put(MapExcelConstants.PARAMS, new ExportParams(actionWithText.get(action), action));
		}
		
		return mav;
	}
}
