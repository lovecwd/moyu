package com.lidx.back.statistics.controller.admin;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.lidx.back.statistics.commons.FilterAction;
import com.lidx.back.statistics.entity.Filter;
import com.lidx.back.statistics.entity.excel.FilterExcel;
import com.lidx.back.statistics.service.IFilterRecordService;
import com.lidx.back.statistics.service.IFilterService;
import com.lidx.back.statistics.service.ISystemVersionService;
import org.apache.commons.lang3.StringUtils;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.vo.NormalExcelConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/admin/filter")
public class FilterAdminController {
	@Autowired
	private IFilterRecordService filterService;
	@Autowired
	private IFilterService filterManageService;
	@Autowired
	private ISystemVersionService systemVersionService;
	
	@RequestMapping(value="manage",method=RequestMethod.GET)
	public String filterManage(Model model){
		List<Filter> filters = filterManageService.getFilters(null);
		model.addAttribute("filters", filters);
		return "filter/manage";
	}
	
	@RequestMapping(value="add",method=RequestMethod.GET)
	public String addFilter(Model model){
		Filter filter = new Filter();
		model.addAttribute("filter", filter);
		return "filter/add";
	}

	@RequestMapping(value="add",method=RequestMethod.POST)
	public String addFilterGo(Model model,@ModelAttribute("filter") Filter filter){
		filterManageService.addFilter(filter);
		return "redirect:/admin/filter/manage";
	}
	
	@RequestMapping(value="list",method=RequestMethod.GET)
	public String filterPage(Model model){
		List<String> list = systemVersionService.getVersionNumbers();
		/*if(list != null){
			model.addAttribute("versionNumbers", list);
		}*/
		return "filter/list";
	}
	
	@RequestMapping(value="list",method=RequestMethod.POST)
	public ModelAndView goFilterPage(@RequestParam(value="filterRecords") String action,
			@RequestParam String versionType,
			@RequestParam(required=false) String versionNumber,
									 @RequestParam(required=false)  String online,
									 @RequestParam(required = false) String order,
			@RequestParam(required=false) String from,
			@RequestParam(required=false) String to){
		ModelAndView mav = new ModelAndView("filter/list");
		if(StringUtils.isNotBlank(action) && StringUtils.isNoneBlank(versionType)){
			mav.addObject("versionType", versionType);
			if(StringUtils.isNotBlank(order)){
				mav.addObject("order", order);
			}
			/*if(StringUtils.isNotBlank(versionNumber)){
				mav.addObject("versionNumber", versionNumber);
			}*/
			//if(StringUtils.isNoneBlank(to)&& StringUtils.isNoneBlank(from)){
				List<Object[]> list = filterService.getRecordsWithParams(action,versionType,
						versionNumber,from,to,null,order);
				mav.addObject("records", list);
			//}
		}
		/*List<String> list = systemVersionService.getVersionNumbers();
		if(list != null){
			mav.addObject("versionNumbers", list);
		}*/
		return mav;
	}

	@RequestMapping(value = "exportFilterData",method = RequestMethod.POST)
	public ModelAndView exportFilterDataToExcel(@RequestParam(value="filterRecords") String action,
												@RequestParam String versionType,
												@RequestParam(required=false)  String online,
												@RequestParam(required = false) String order,
												@RequestParam(required=false) String from,
												@RequestParam(required=false) String to,
														ModelMap modelMap){
		ModelAndView mav = null;
		if(StringUtils.isNotBlank(action)){
			List<Object[]> list = filterService.getRecordsWithParams(action,versionType,
					StringUtils.EMPTY,from,to,online,order);
			mav = new ModelAndView(NormalExcelConstants.JEECG_EXCEL_VIEW);
			List<FilterExcel> filterExcels = null;
			if(null != list){
				FilterExcel filterExcel = null;
				filterExcels = Lists.newArrayList();
				for (Object[] o : list){
					filterExcel = new FilterExcel();
					filterExcel.setFilterName(o[0].toString());
					filterExcel.setVersionType(o[1].toString());
					filterExcel.setVersionNumber(o[2].toString());
					filterExcel.setNumber(o[3].toString());
					filterExcels.add(filterExcel);
				}
			}
			modelMap.put(NormalExcelConstants.DATA_LIST, filterExcels);
			modelMap.put(NormalExcelConstants.CLASS, FilterExcel.class);
			//modelMap.put(MapExcelConstants.MAP_LIST, dataResult);
			modelMap.put(NormalExcelConstants.FILE_NAME, "滤镜相关数据统计");
			modelMap.put(NormalExcelConstants.PARAMS, new ExportParams(actionWithText.get(action), action));
		}else{
			mav = new ModelAndView("filter/list");
		}

		return mav;
	}

	private static Map<String,String> actionWithText = null;

	static {
		actionWithText = Maps.newHashMap();
		actionWithText.put(FilterAction.FILTER_USE,"通用滤镜的使用量");
		actionWithText.put(FilterAction.FILTER_MAKE,"通用滤镜的制作量");
	}
}
