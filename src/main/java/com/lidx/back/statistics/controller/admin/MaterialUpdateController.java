package com.lidx.back.statistics.controller.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresRoles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.lidx.back.statistics.entity.MaterialPO;
import com.lidx.back.statistics.entity.MaterialUpdateRequestInfo;
import com.lidx.back.statistics.service.IMaterialService;

@Controller
@RequestMapping("/admin/material")
public class MaterialUpdateController {
	private static final Logger logger = LoggerFactory.getLogger(MaterialUpdateController.class);

	@Autowired
	private IMaterialService materialService;
	
	/*@RequestMapping(value="/list",method=RequestMethod.GET)
	public ModelAndView materialListPage(){
		ModelAndView mav = new ModelAndView("materialManage/list");
		return mav;
	}
	
	@RequestMapping(value="/list",method=RequestMethod.POST)
	public ModelAndView goMaterialListPage(@RequestParam String versionType,
			@RequestParam String versionNum){
		ModelAndView mav = new ModelAndView("materialManage/list");
		List<MaterialPO> materialPOs = materialService.findAllMaterialList(versionType, versionNum);
		mav.addObject("materials", materialPOs);
		return mav;
	}*/

	@RequestMapping(value = "/update", method = RequestMethod.GET)
	public String updatePage(Model model, HttpServletRequest request,
			@RequestParam(required=false) String versionType,@RequestParam(required=false) String versionNum) {

		/*Map<String, List<MaterialPO>> androidMaterials = new LinkedHashMap<String, List<MaterialPO>>();
		Map<String, List<MaterialPO>> iosMaterials = new LinkedHashMap<String, List<MaterialPO>>();
		model.addAttribute("androidMaterials", androidMaterials);
		model.addAttribute("iosMaterials", iosMaterials);
		model.addAttribute("versionNum", versionNum);
		model.addAttribute("versionType", versionType);
		List<MaterialPO> pos = materialService.findAllMaterialList("", "");
		if(null != pos && pos.size()>0){
			for (MaterialPO po : pos) {

				String versionNumber = po.getVersionNumber();

				if ("ios".equalsIgnoreCase(po.getVersionType())) {
					List<MaterialPO> iosPOs = iosMaterials.get(versionNumber);
					if (iosPOs == null) {
						iosPOs = new ArrayList<MaterialPO>();
						iosMaterials.put(versionNumber, iosPOs);
					}
					iosPOs.add(po);
				} else if ("android".equalsIgnoreCase(po.getVersionType())) {
					List<MaterialPO> androidPOs = androidMaterials.get(versionNumber);
					if (androidPOs == null) {
						androidPOs = new ArrayList<MaterialPO>();
						androidMaterials.put(versionNumber, androidPOs);
					}
					androidPOs.add(po);
				} else {
				}
			}
		}*/
		MaterialUpdateRequestInfo info = materialService.getRequestInfo();
		List<String> mNames = materialService.getMaterialNames(info.getVersionType(),info.getVersionNum());
		model.addAttribute("names", mNames);
		model.addAttribute("info", info);
		model.addAttribute("number", mNames.size());
		return "material/update";
	}

	@RequiresRoles(value={"3"})
	@RequestMapping(value = "update", method = RequestMethod.POST)
	public String submitUpdate(@RequestParam("versionType") String versionType, @RequestParam("versionNumber") String versionNumber,
			HttpServletRequest request, HttpServletResponse response,Model model) throws IOException {
		logger.debug(versionType);
		logger.debug(versionNumber);
		try {
			MaterialUpdateRequestInfo info = new MaterialUpdateRequestInfo();
			materialService.updateMaterial(versionType, versionNumber);
			info.setVersionNum(versionNumber);
			info.setVersionType(versionType);
			materialService.updateMaterialUpdateRequest(info);
			info = materialService.getRequestInfo();
			model.addAttribute("info", info);
			List<MaterialPO> pos = materialService.findAllMaterialList(versionType, versionNumber);
			model.addAttribute("materials", pos);
			return "material/update";
		} catch (Exception e) {
			response.getWriter().write("Error:[" + e.getMessage() + "]   URL= " + versionType + "	(" + versionNumber + ")");
			e.printStackTrace();
			return null;
		}
	}

}
