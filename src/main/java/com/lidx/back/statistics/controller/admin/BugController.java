package com.lidx.back.statistics.controller.admin;


import com.google.common.collect.Lists;
import com.lidx.back.statistics.commons.Constants;
import com.lidx.back.statistics.commons.MaterialUpdateConfig;
import com.lidx.back.statistics.commons.PageInfo;
import com.lidx.back.statistics.commons.config.Global;
import com.lidx.back.statistics.entity.Bug;
import com.lidx.back.statistics.service.IBugService;
import com.lidx.back.statistics.service.ISysUserService;
import com.lidx.back.statistics.utils.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by chen on 2017/2/18.
 */
@Controller
@RequestMapping("/admin/bug")
public class BugController {

    @Autowired
    private IBugService bugService;
    @Autowired
    private ISysUserService sysUserService;

    public static List<String> imgExtension = Lists.newArrayList();

    static {
        imgExtension.add("gif");
        imgExtension.add("jpg");
        imgExtension.add("jpeg");
        imgExtension.add("png");
        imgExtension.add("bmp");
    }

    @RequestMapping(value = "list",method = RequestMethod.GET)
    public ModelAndView bugList(@RequestParam(required=false,defaultValue="10") int pageSize,
                                @RequestParam(required=false,defaultValue="1") int pageNo){
        ModelAndView mav = new ModelAndView("bug/list");
        //List<Bug> bugs = bugService.findBugs();
        PageInfo bugs = bugService.findBugsPage(pageNo,pageSize);
        mav.addObject("pageInfo",bugs);
        return mav;
    }

    @RequestMapping(value = "list",method = RequestMethod.POST)
    public ModelAndView bugListGo(@RequestParam(required=false,defaultValue="10") int pageSize,
                                @RequestParam(required=false,defaultValue="1") int pageNo){
        ModelAndView mav = new ModelAndView("bug/list");
        //List<Bug> bugs = bugService.findBugs();
        PageInfo bugs = bugService.findBugsPage(pageNo,pageSize);
        mav.addObject("pageInfo",bugs);
        return mav;
    }

    @RequestMapping(value = "add",method = RequestMethod.GET)
    public ModelAndView addBug(){
        ModelAndView mav = new ModelAndView("bug/add");
        List<String> userNames = sysUserService.getUserNames();
        mav.addObject("userNames",userNames);
        mav.addObject("bug",new Bug());
        return mav;
    }

    @RequestMapping(value = "add",method = RequestMethod.POST)
    public ModelAndView addBug(@ModelAttribute("bug") Bug bug, @RequestParam("bugImage") MultipartFile[] bugImgs){
        ModelAndView mav = null;
        String originalFilename = null;
        String fileName = null;
        String savePath = null;
        StringBuffer saveUrls = null;
        try {
            if(bugImgs.length > 0){
                saveUrls = new StringBuffer();
                int i = 0;
                String fileExtension = "";
                for(MultipartFile file : bugImgs){
                    if(file.isEmpty()){
                        continue;
                    }
                    originalFilename = file.getOriginalFilename();
                    fileExtension = FileUtils.getFileExtension(originalFilename);
                    if(!imgExtension.contains(fileExtension)){
                        throw new RuntimeException("上传图片类型只能为gif,jpg,jpeg,png,bmp");
                    }
                    fileName = System.currentTimeMillis()+"."+FileUtils.getFileExtension(originalFilename);
                    savePath = Global.getConfig("nginx.root")+ Constants.FILE_BUG_PATH;
                    FileUtils.copyInputStreamToFile(file.getInputStream(),new File(savePath,fileName));
                    saveUrls.append(Global.getConfig("nginx.url.root")).append(Constants.FILE_BUG_PATH).append("/")
                            .append(fileName).append("-");
                }
                if(saveUrls.indexOf("-") > 0){
                    saveUrls.deleteCharAt(saveUrls.lastIndexOf("-"));
                }
            }
            if (saveUrls != null){
                bug.setBugImgUrls(saveUrls.toString());
            }
            bugService.saveBug(bug);
            mav = new ModelAndView("redirect:/admin/bug/list");
        } catch (IOException e) {
            e.printStackTrace();
            mav = new ModelAndView("bug/add");
            List<String> userNames = sysUserService.getUserNames();
            mav.addObject("userNames",userNames);
            mav.addObject("errorMsg",e.getMessage());
        }catch (Exception e2){
            e2.printStackTrace();
            mav = new ModelAndView("bug/add");
            mav.addObject("errorMsg",e2.getMessage());
        }
        return mav;
    }

    @RequestMapping(value = "dealed",method = RequestMethod.GET)
    public ModelAndView dealBug(@RequestParam int id,@RequestParam(required = false) String operator){
        ModelAndView mav = null;

        try {
            if(id>0 && bugService.dealedBug(id,operator)){
                mav = new ModelAndView("redirect:/admin/bug/list");
                return mav;
            }
        } catch (Exception e) {
            e.printStackTrace();
            mav = new ModelAndView("forward:/admin/bug/list");
            mav.addObject("errorMsg",e.getMessage());
        }
        return mav;
    }

    @RequestMapping(value = "delete",method = RequestMethod.GET)
    public ModelAndView deleteBug(@RequestParam int id){
        ModelAndView mav = null;
        try {
            if(id>0 && bugService.deleteBug(id)){
                mav = new ModelAndView("redirect:/admin/bug/list");
                return mav;
            }
        } catch (Exception e) {
            e.printStackTrace();
            mav = new ModelAndView("forward:/admin/bug/list");
            mav.addObject("errorMsg",e.getMessage());
        }
        return mav;
    }

    @RequestMapping(value = "detail",method = RequestMethod.GET)
    public ModelAndView detail(@RequestParam int id){
        ModelAndView mav = null;
        if(id>0){
            Bug bug = bugService.getBugById(id);
            mav = new ModelAndView("bug/detail");
            mav.addObject("bug",bug);
            return mav;
        }
        mav = new ModelAndView("bug/list");
        return mav;
    }

}
