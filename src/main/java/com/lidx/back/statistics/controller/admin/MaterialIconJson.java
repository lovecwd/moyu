package com.lidx.back.statistics.controller.admin;

public class MaterialIconJson {

	private String iconName;
	
	private String iconMD5;
	
	private String category;
	
	private String isRecommend;
	
	private String recommendOrder;
	
	private String categoryOrder;

	public String getIconName() {
		return iconName;
	}

	public void setIconName(String iconName) {
		this.iconName = iconName;
	}

	public String getIconMD5() {
		return iconMD5;
	}

	public void setIconMD5(String iconMD5) {
		this.iconMD5 = iconMD5;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getIsRecommend() {
		return isRecommend;
	}

	public void setIsRecommend(String isRecommend) {
		this.isRecommend = isRecommend;
	}

	public String getRecommendOrder() {
		return recommendOrder;
	}

	public void setRecommendOrder(String recommendOrder) {
		this.recommendOrder = recommendOrder;
	}

	public String getCategoryOrder() {
		return categoryOrder;
	}

	public void setCategoryOrder(String categoryOrder) {
		this.categoryOrder = categoryOrder;
	}
	
}
