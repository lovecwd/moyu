package com.lidx.back.statistics.controller.admin;

import com.lidx.back.statistics.commons.Constants;
import com.lidx.back.statistics.commons.PageInfo;
import com.lidx.back.statistics.entity.FeedBack;
import com.lidx.back.statistics.service.IDictionaryService;
import com.lidx.back.statistics.service.IFeedbackService;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.vo.NormalExcelConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/admin/feedback")
public class FeedbackAdminController {

	@Autowired
	private IFeedbackService feedbackService;
	
	@Autowired
	private IDictionaryService dictionaryService;
	
	@RequestMapping(value="list",method=RequestMethod.GET)
	public String listGet(Model model,@RequestParam(required=false,defaultValue="10") int pageSize,
			@RequestParam(required=false,defaultValue="1") int pageNo){
	    PageInfo pageInfo = feedbackService.queryFeedbacks(pageNo, pageSize);
		String imageUrlPrefix = dictionaryService.getValueByCode(Constants.IIS_IMAGES_URL_PREFIX_CODE);
		if(pageInfo != null){
			model.addAttribute("pageInfo", pageInfo);
			model.addAttribute("prefixUrl", imageUrlPrefix);
		}
		return "feedback/list";
	}
	
	@RequestMapping(value="list",method=RequestMethod.POST)
	public String list(Model model,@RequestParam(required=false,defaultValue="10") int pageSize,
			@RequestParam(required=false,defaultValue="1") int pageNo){
		//List<FeedBack> feedBacks = feedbackService.getFeedBacks();
		 PageInfo pageInfo = feedbackService.queryFeedbacks(pageNo, pageSize);
		String imageUrlPrefix = dictionaryService.getValueByCode(Constants.IIS_IMAGES_URL_PREFIX_CODE);
		if(pageInfo != null){
			model.addAttribute("pageInfo", pageInfo);
			model.addAttribute("prefixUrl", imageUrlPrefix);
		}
		model.addAttribute("pageSize", pageSize);
		return "feedback/list";
	}
	
	@RequestMapping(value="exportFeedbackExcel",method=RequestMethod.POST)
	public String exportFeedbackExcel(Model model,HttpServletRequest request,
			ModelMap map,@RequestParam(required=false,defaultValue="10") int pageSize,
			@RequestParam(required=false,defaultValue="1") int pageNo) throws IOException{
		//List<FeedBack> feedBacks = feedbackService.getFeedBacks();
		PageInfo pageInfo = feedbackService.queryFeedbacks(pageNo, pageSize);
		String path = request.getSession().getServletContext().getRealPath("");
		String serverFeedbackUrl = path+"\\resources\\feedback";
		List<FeedBack> list = pageInfo.getList();
		/*for(FeedBack feedBack : list){
			String imgNames = feedBack.getImageNames();//逗号隔开
			String[] names = imgNames.split(",");
			String s = "\\resources\\feedback\\";
			StringBuilder sb = new StringBuilder();
			for(int i = 0;i<names.length;i++){
				//sb.append(Constants.IMAGE_FILE_PATH).append("//").append(names[i]).append("\\n");
				sb.append(s).append(names[i]);
				FileUtils.copyFileToDirectory(new File(Constants.IMAGE_FILE_PATH+"//"+names[i]), new File(serverFeedbackUrl));
			}
			
			feedBack.setImageNames(names[0]);
		}*/
		
		map.put(NormalExcelConstants.FILE_NAME,"用户反馈信息");
        map.put(NormalExcelConstants.CLASS,FeedBack.class);
        map.put(NormalExcelConstants.PARAMS,new ExportParams("反馈列表", "反馈sheet"));
        map.put(NormalExcelConstants.DATA_LIST,pageInfo.getList());
        return NormalExcelConstants.JEECG_EXCEL_VIEW;
	}
}
