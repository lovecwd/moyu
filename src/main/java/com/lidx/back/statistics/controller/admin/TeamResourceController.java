package com.lidx.back.statistics.controller.admin;

import com.lidx.back.statistics.commons.Constants;
import com.lidx.back.statistics.commons.PageInfo;
import com.lidx.back.statistics.commons.config.Global;
import com.lidx.back.statistics.entity.TeamResource;
import com.lidx.back.statistics.service.ITeamResourceService;
import com.lidx.back.statistics.utils.FileUtils;
import com.lidx.back.statistics.utils.StringUtils;
import com.lidx.back.statistics.utils.UploadUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by chen on 2017/2/17.
 */
@Controller
@RequestMapping("/admin/teamResource")
public class TeamResourceController {

    @Autowired
    private ITeamResourceService teamResourceService;


    @RequestMapping(value = "list",method = RequestMethod.GET)
    public ModelAndView index(@RequestParam(required=false,defaultValue="10") int pageSize,
                              @RequestParam(required=false,defaultValue="1") int pageNo,
                              @RequestParam(required=false) String resourceCategory){
        ModelAndView mav = new ModelAndView("teamResource/list");
        PageInfo teamResources = teamResourceService.findTeamResourcePage(pageNo,pageSize,StringUtils.EMPTY);
        mav.addObject("pageInfo",teamResources);
       /* List<TeamResource> teamResources = teamResourceService.findTeamResourceList();
        mav.addObject("teamResources",teamResources);*/
        return mav;
    }

    @RequestMapping(value = "list",method = RequestMethod.POST)
    public ModelAndView indexGo(@RequestParam(required=false,defaultValue="10") int pageSize,
                                @RequestParam(required=false,defaultValue="1") int pageNo,
                                @RequestParam(required=false) String resourceCategory){
        ModelAndView mav = new ModelAndView("teamResource/list");
        if(StringUtils.isNotBlank(resourceCategory)){
            mav.addObject("resourceCategory",resourceCategory);
        }
        PageInfo teamResources = teamResourceService.findTeamResourcePage(pageNo,pageSize,resourceCategory);
        mav.addObject("pageInfo",teamResources);
        return mav;
    }
    @RequestMapping(value = "add",method = RequestMethod.GET)
    public ModelAndView add(){
        ModelAndView mav = new ModelAndView("teamResource/add");
        mav.addObject("teamResource",new TeamResource());
        return mav;
    }

    @RequestMapping(value = "add",method = RequestMethod.POST)
    public ModelAndView addGo(@ModelAttribute("teamResource") TeamResource teamResource,
                              @RequestParam("resourceFile") MultipartFile resFile,HttpServletRequest request ){
        ModelAndView mav = new ModelAndView("redirect:/admin/teamResource/list");
        //文件名很难处理，于是定为系统时间
        String originalFilename = resFile.getOriginalFilename();
        String fileName = System.currentTimeMillis()+"."+FileUtils.getFileExtension(originalFilename);
        //不能放到当前tomcat服务器下面，因为是集群的，所以将上传的静态资源放到nginx服务器下面。
        //String savePath = request.getSession().getServletContext().getRealPath("/") + Constants.FILE_UPLOAD_PATH + "/";
        String savePath = Global.getConfig("nginx.root")+ Constants.FILE_UPLOAD_PATH;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String ymd = sdf.format(new Date());
        String fileUrl = request.getContextPath()+"/"+Constants.FILE_DOWNLOAD_PATH+"/"+fileName+"/"+ymd;
        try {
            if(null != resFile){
                FileUtils.copyInputStreamToFile(resFile.getInputStream(),new File(savePath,fileName));
            }
            if(null != teamResource){
                teamResource.setResourceUrl(fileUrl);
                teamResourceService.saveTeamResource(teamResource);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }

    @RequestMapping(value = "view",method = RequestMethod.GET)
    public ModelAndView view(@RequestParam int id){
        ModelAndView mav = new ModelAndView("teamResource/view");
        TeamResource tr = teamResourceService.getTeamResourceById(id);
        mav.addObject("teamResource",tr);
        return mav;
    }

    @RequiresRoles(value = "1")
    @RequestMapping(value = "delete",method = RequestMethod.GET)
    public ModelAndView delete(@RequestParam int id){
        ModelAndView mav = new ModelAndView("redirect:/admin/teamResource/list");
        teamResourceService.deleteTeamResource(id);
        return mav;
    }

    @RequestMapping(value = "update",method = RequestMethod.GET)
    public ModelAndView update(@RequestParam int id){
        ModelAndView mav = new ModelAndView("teamResource/update");
        TeamResource tr = teamResourceService.getTeamResourceById(id);
        mav.addObject("teamResource",tr);
        return mav;
    }

    @RequestMapping(value = "update",method = RequestMethod.POST)
    public ModelAndView updateGo(@ModelAttribute("teamResource") TeamResource tr,
                                 @RequestParam("resourceFile") MultipartFile resFile,
                                 HttpServletRequest request){
        ModelAndView mav = new ModelAndView("redirect:/admin/teamResource/list");
        String originalFilename = resFile.getOriginalFilename();
        String fileName = System.currentTimeMillis()+"."+FileUtils.getFileExtension(originalFilename);
        String savePath = Global.getConfig("nginx.root")+ Constants.FILE_UPLOAD_PATH;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String ymd = sdf.format(new Date());
        String fileUrl = request.getContextPath()+"/"+Constants.FILE_DOWNLOAD_PATH+"/"+fileName+"/"+ymd;
        try {
            if(null != resFile){
                FileUtils.copyInputStreamToFile(resFile.getInputStream(),new File(savePath,fileName));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        tr.setResourceUrl(fileUrl);
        teamResourceService.updateTeamResource(tr);
        return mav;
    }
}
