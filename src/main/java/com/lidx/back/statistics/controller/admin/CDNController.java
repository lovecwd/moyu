package com.lidx.back.statistics.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.cdn.model.v20141111.DescribeRefreshTasksRequest;
import com.aliyuncs.cdn.model.v20141111.DescribeUserDomainsRequest;
import com.aliyuncs.cdn.model.v20141111.PurgeObjectCachesRequest;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.FormatType;
import com.aliyuncs.http.HttpResponse;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;

@RequestMapping("/admin/cdn")
@Controller
public class CDNController {

	private static IAcsClient client = null;

	public static void init() throws ClientException {
		IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou",
				"LTAIdsnJNpxiDrKD", "fNMUdQxj5D3d0o3i7eRTzSxcdRMXgX");
		if(client == null){
			client = new DefaultAcsClient(profile);
		}
	}

	@RequestMapping(value = "list", method = RequestMethod.GET)
	public ModelAndView cdnList() {
		ModelAndView mav = new ModelAndView("cdn/list");
		DescribeUserDomainsRequest request = new DescribeUserDomainsRequest();
		request.setAcceptFormat(FormatType.JSON);
		try {
			init();
			HttpResponse httpResponse = client.doAction(request);
			System.out.println(httpResponse.getUrl());
			String content = new String(httpResponse.getContent());
			System.out.println(content);
			mav.addObject("content", content);
			System.out.println(httpResponse.getStatus());
		} catch (ServerException e) {
			e.printStackTrace();
		} catch (ClientException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return mav;
	}

	@RequestMapping(value = "updateDir", method = RequestMethod.GET)
	public ModelAndView updateDir() {
		ModelAndView mav = new ModelAndView("cdn/updateDir");
		return mav;
	}

	@RequestMapping(value = "updateDir", method = RequestMethod.POST)
	public ModelAndView updateDirGo(@RequestParam String domain,
			@RequestParam String filePath) {
		ModelAndView mav = new ModelAndView("cdn/updateDir");

		PurgeObjectCachesRequest request = new PurgeObjectCachesRequest();
		// 要刷新的域名
		request.setDomainName(domain);
		// 要刷新的文件路径
		request.setObjectPath(filePath);
		// 刷新类型，默认是File,刷新目录为Directory
		request.setObjectType("Directory");
		// 设置返回格式为JSON
		request.setAcceptFormat(FormatType.JSON);

		try {
			init();
			HttpResponse httpResponse = client.doAction(request);
			System.out.println(httpResponse.getUrl());
			System.out.println(new String(httpResponse.getContent()));
			System.out.println(httpResponse.getStatus());
		} catch (ServerException e) {
			e.printStackTrace();
		} catch (ClientException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return mav;
	}
	
	@RequestMapping(value = "updateTasks", method = RequestMethod.GET)
	public ModelAndView updateTasks(@RequestParam(required=false) String domain) {
		ModelAndView mav = new ModelAndView("cdn/updateTasks");

		DescribeRefreshTasksRequest request = new DescribeRefreshTasksRequest();
		// 设置返回格式为JSON
		request.setAcceptFormat(FormatType.JSON);

		try {
			init();
			HttpResponse httpResponse = client.doAction(request);
			System.out.println(httpResponse.getUrl());
			String content = new String(httpResponse.getContent());
			System.out.println(content);
			mav.addObject("content", content);
			System.out.println(httpResponse.getStatus());
		} catch (ServerException e) {
			e.printStackTrace();
		} catch (ClientException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return mav;
	}
}
