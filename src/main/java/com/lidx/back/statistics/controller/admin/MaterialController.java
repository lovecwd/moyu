package com.lidx.back.statistics.controller.admin;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.lidx.back.statistics.commons.MaterialAction;
import com.lidx.back.statistics.commons.config.Global;
import com.lidx.back.statistics.entity.MaterialCategory;
import com.lidx.back.statistics.entity.MaterialLatest;
import com.lidx.back.statistics.entity.MaterialPO;
import com.lidx.back.statistics.entity.MaterialRecord;
import com.lidx.back.statistics.entity.excel.MaterialExcel;
import com.lidx.back.statistics.entity.record.MaterialShareRecord;
import com.lidx.back.statistics.service.*;
import com.lidx.back.statistics.utils.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.vo.NormalExcelConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.DigestUtils;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/admin/material")
public class MaterialController {

    @Autowired
    private IMaterialService materialService;
    @Autowired
    private IMaterialCategoryService categoryService;
    @Autowired
    private IMaterialRecordService recordService;
    @Autowired
    private ISystemVersionService systemVersionService;
    @Autowired
    private ILatestMaterialService latestMaterialService;


    private static Map<String,String> actionWithText = null;

    static {
        actionWithText = Maps.newHashMap();
        actionWithText.put(MaterialAction.MATERIAL_DOWNLOAD,"素材的下载量");
        actionWithText.put(MaterialAction.MATERIAL_MAKE_VIDEO,"素材制作视频数量");
        actionWithText.put(MaterialAction.MATERIAL_PHOTOGRAPH,"素材拍照数量");
        actionWithText.put(MaterialAction.MATERIAL_PIC_SHARE,"素材图片的分享量");
        actionWithText.put(MaterialAction.MATERIAL_VIDEO_SHARE,"素材的视频的分享量");
    }

    @RequestMapping(value = "latestMaterials", method = RequestMethod.GET)
    public String getLatestMaterials(Model model) {
        List<MaterialLatest> materialLatests = latestMaterialService.findLatestMaterials();
        model.addAttribute("materialLatests", materialLatests);
        List<MaterialCategory> categoryList = categoryService.getCategories(null, null, null);
        model.addAttribute("categoryList", categoryList);
        return "material/latestMaterials";
    }

    @RequestMapping(value = "initLatestMaterials", method = RequestMethod.POST)
    public String initLatestMaterials(Model model,String size) {
        boolean init = latestMaterialService.initLatestMaterials(Integer.parseInt(size));
        List<MaterialLatest> materialLatests = latestMaterialService.findLatestMaterials();
        model.addAttribute("materialLatests", materialLatests);
        List<MaterialCategory> categoryList = categoryService.getCategories(null, null, null);
        model.addAttribute("categoryList", categoryList);
        return "material/latestMaterials";
    }

    @RequiresRoles(value = "3")
    @RequestMapping(value = "deleteLatestMaterial", method = RequestMethod.GET)
    public String deleteLatestMaterial(@RequestParam int id) {
        latestMaterialService.deleteLatestMaterial(id);
        return "redirect:/admin/material/latestMaterials";
    }

    @RequestMapping(value = "listMaterials", method = RequestMethod.GET)
    public String getAllMaterials(Model model) {
        model.addAttribute("material", new MaterialPO());
        List<MaterialCategory> categoryList = categoryService.getCategories(null, null, null);
        model.addAttribute("categoryList", categoryList);
        return "material/materialList";
    }



    @RequestMapping(value = "listMaterials", method = RequestMethod.POST)
    public String goGetAllMaterials(Model model,
                                    //@RequestParam String versionType,
                                    //@RequestParam String versionNum,
                                    @RequestParam(value = "categoryId", required = false) String categoryId) {

        Map<String, List<MaterialPO>> androidMaterials = new LinkedHashMap<String, List<MaterialPO>>();
        Map<String, List<MaterialPO>> iosMaterials = new LinkedHashMap<String, List<MaterialPO>>();
        model.addAttribute("androidMaterials", androidMaterials);
        model.addAttribute("iosMaterials", iosMaterials);
        MaterialPO mp = buildMaterial(null, null, null, null, categoryId);
        List<MaterialPO> pos = materialService.findAllMaterialList(mp);
       /* //List<String> categoryNames = categoryService.getCategoryNames();
        model.addAttribute("categoryNames", categoryNames);
        if (null != pos && pos.size() > 0) {
            for (MaterialPO po : pos) {
                String versionNumber = po.getVersionNumber();
                if ("ios".equalsIgnoreCase(po.getVersionType())) {
                    List<MaterialPO> iosPOs = iosMaterials.get(versionNumber);
                    if (iosPOs == null) {
                        iosPOs = new ArrayList<MaterialPO>();
                        iosMaterials.put(versionNumber, iosPOs);
                    }
                    iosPOs.add(po);
                } else if ("android".equalsIgnoreCase(po.getVersionType())) {
                    List<MaterialPO> androidPOs = androidMaterials.get(versionNumber);
                    if (androidPOs == null) {
                        androidPOs = new ArrayList<MaterialPO>();
                        androidMaterials.put(versionNumber, androidPOs);
                    }
                    androidPOs.add(po);
                } else {
                }
            }
        }*/

        model.addAttribute("materials", pos);
        model.addAttribute("material", mp);
        List<MaterialCategory> categoryList = categoryService.getCategories(null, null, null);
        model.addAttribute("categoryList", categoryList);
        return "material/materialList";
    }

    @RequestMapping(value = "editMaterial", method = RequestMethod.GET)
    public String editMaterial(Model model, @RequestParam int id) {
        MaterialPO mp = materialService.getMaterialById(id);
        model.addAttribute("material", mp);
        List<MaterialCategory> categoryList = categoryService.getCategories(null, null, null);
        model.addAttribute("categoryList", categoryList);
        return "material/editMaterial";
    }

    @RequiresRoles("3")
    @RequestMapping(value = "editMaterial", method = RequestMethod.POST)
    public String editMaterialGo(Model model, @ModelAttribute("material") MaterialPO materialPO,
                                 @RequestParam("iconFile") MultipartFile iconFile,
                                 @RequestParam("materialFile") MultipartFile materialFile) {

        try{
            String savePath = Global.getConfig("nginx.root.materials")+materialPO.getVersionType()+"\\"+materialPO.getVersionNumber();
            String fileUrlPrefix = Global.getConfig("nginx.url.root");
            File saveFileDir = new File(savePath);
            if(!saveFileDir.exists()){
                saveFileDir.mkdirs();
            }
            if(!iconFile.isEmpty()){
                materialPO.setIconName(iconFile.getOriginalFilename());
                materialPO.setIconDownloadUrl(fileUrlPrefix+materialPO.getVersionType()+"/"
                        +materialPO.getVersionNumber()+"/"+materialPO.getIconName());
                File iconFileLocal = new File(savePath, iconFile.getOriginalFilename());
                FileUtils.copyInputStreamToFile(iconFile.getInputStream(), iconFileLocal);
                materialPO.setIconMD5(DigestUtils.md5DigestAsHex(FileCopyUtils.copyToByteArray(iconFileLocal)));
            }
            if(!materialFile.isEmpty()){
                materialPO.setMaterialName(materialFile.getOriginalFilename());
                materialPO.setMaterialDownloadUrl(fileUrlPrefix+materialPO.getVersionType()+"/"
                        +materialPO.getVersionNumber()+"/"+materialPO.getMaterialName());
                File materialFileLocal = new File(savePath, materialFile.getOriginalFilename());
                FileUtils.copyInputStreamToFile(materialFile.getInputStream(), materialFileLocal);
                materialPO.setMaterialMD5(DigestUtils.md5DigestAsHex(FileCopyUtils.copyToByteArray(materialFileLocal)));
            }
            materialService.updateMaterial(materialPO);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:/admin/material/listMaterials";
    }

    @ResponseBody
    @RequestMapping(value = "getMD5s", method = RequestMethod.GET)
    public Map<String, String> getMd5(@RequestParam String iconName,
                                      @RequestParam String materialName,
                                      @RequestParam String versionType,
                                      @RequestParam String versionNumber) {
        Map<String, String> md5s = null;
        try {
            md5s = materialService.getMaterialMd5s(iconName, materialName, versionType, versionNumber);
            md5s.put("msg", "success");
        } catch (Exception e) {
            e.printStackTrace();
            md5s.put("msg", "fail");
        }
        return md5s;
    }

    private MaterialPO buildMaterial(String versionType, String versionNum,
                                     String sex, String isRecommend, String categoryId) {
        MaterialPO mp = new MaterialPO();
        if (StringUtils.isNotBlank(versionType)) {
            mp.setVersionType(versionType);
        }
        if (StringUtils.isNotBlank(versionNum)) {
            mp.setVersionNumber(versionNum);
        }
        if (StringUtils.isNotBlank(categoryId)) {
            mp.setCategoryId(Integer.valueOf(categoryId));
        }
        /*if (StringUtils.isNotBlank(sex)) {
            mp.setGender(sex);
        }*/
        /*if (StringUtils.isNotBlank(isRecommend)) {
            mp.setIsRecommend(isRecommend);
        }*/
        /*if (StringUtils.isNotBlank(category)) {
            mp.setCategory(category);
        }*/
        return mp;
    }

    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String MaterialRecords(Model model) {
        /*List<String> list = systemVersionService.getVersionNumbers();
		if(list != null){
			model.addAttribute("versionNumbers", list);
		}*/
        return "material/list";
    }


    @RequestMapping(value = "list", method = RequestMethod.POST)
    public String MaterialRecordsPost(Model model, @RequestParam(value = "materialRecords") String action,
                                      @RequestParam String versionType,
                                      @RequestParam(required = false) String order,
                                      @RequestParam(required=false) String versionNumber,
                                      @RequestParam(required = false) String from,
                                      @RequestParam(required = false) String to) {
        if (StringUtils.isNotBlank(action) && StringUtils.isNotBlank(versionType)) {
            model.addAttribute("action", action);
            model.addAttribute("versionType", versionType);
            List<String> list2 = systemVersionService.getVersionNumbers();
            if (list2 != null) {
                model.addAttribute("versionNumbers", list2);
            }
            if (StringUtils.isNotBlank(order)) {
                model.addAttribute("order", order);
            }
            if (StringUtils.isNotBlank(from) && StringUtils.isNotBlank(to)) {
                model.addAttribute("from", from);
                model.addAttribute("to", to);
            }
            List<MaterialRecord> materialRecords = recordService.getRecordsWithParams(action,versionType,versionNumber,from,to,null,order);
            model.addAttribute("materialRecords", materialRecords);
            /*if (MaterialAction.MATERIAL_PIC_SHARE.equals(action)
                    || MaterialAction.MATERIAL_VIDEO_SHARE.equals(action)) {
                List<MaterialShareRecord> sortedShareRecords = recordService.getRecordsWithMaterialChannel(action,
                        versionType, StringUtils.EMPTY, from, to, online, order);
                //model.addAttribute("shareMapData", shareMapData);
                model.addAttribute("shareMapData", sortedShareRecords);

            } else {
                List<Object[]> list = recordService.getRecordsWithParams(action, versionType,
                        StringUtils.EMPTY, from, to, online, order);
                model.addAttribute("records", list);
            }*/
        }
        return "material/list";
    }

    @RequiresRoles("3")
    @RequestMapping(value = "delete", method = RequestMethod.GET)
    public String deleteMaterial(@RequestParam int id) throws Exception {
        materialService.deleteMaterial(id);
        return "redirect:/admin/material/listMaterials";
    }


    @RequiresRoles("3")
    @RequestMapping(value = "batchDelete", method = RequestMethod.POST)
    public String deleteMaterial(@RequestParam int id[]) {
        try {
            materialService.deleteMaterialsByIds(id);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:/admin/material/listMaterials";
    }

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public ModelAndView addMaterial(Model model) {
        ModelAndView mav = new ModelAndView("material/add");
        MaterialPO m = new MaterialPO();
        mav.addObject("material", m);
        List<MaterialCategory> categoryList = categoryService.getCategories(null, null, null);
        model.addAttribute("categoryList", categoryList);
        return mav;
    }

   /* *
     * 用于批量添加素材
     * @param model
     * @return
     * */
	@RequestMapping(value="addMaterials",method=RequestMethod.GET)
	public ModelAndView addMaterials(Model model) {
		ModelAndView mav = new ModelAndView("material/addMaterials");
        List<MaterialCategory> categoryList = categoryService.getCategories(null, null, null);
        model.addAttribute("categoryList", categoryList);
		return mav;
	}

   /* *//**
     * 用于批量添加素材
     *
     * @return
     *//*
	*/@RequestMapping(value="addMaterials",method=RequestMethod.POST)
	public ModelAndView addMaterialsGo(@RequestParam(value="versionType") String[] versionTypes,
                                        @RequestParam(value="versionNumber") String[] versionNumbers,
                                        @RequestParam(value="uuid") String[] uuids,
                                        @RequestParam(value="categoryId") int[] categoryIds,
                                        @RequestParam(value="categoryOrder") int[] categoryOrders,
                                        @RequestParam("iconFile") MultipartFile[] iconFiles,
                                        @RequestParam("materialFile") MultipartFile[] materialFiles,
                                        @RequestParam("audio") String[] audios) {
        ModelAndView mav = null;
        try {
            mav = new ModelAndView("redirect:/admin/material/listMaterials");
            String savePath = Global.getConfig("nginx.root.materials");
            File saveFileDir = new File(savePath);
            if(!saveFileDir.exists()){
                saveFileDir.mkdirs();
            }
            String fileUrlPrefix = Global.getConfig("nginx.url.root");
            MaterialPO m = null;
            List<MaterialPO> materialPOS = Lists.newArrayListWithExpectedSize(uuids.length);
            for(int i=0;i<uuids.length;i++){
                m = new MaterialPO();
                m.setUuid(uuids[i]);
                m.setVersionType(versionTypes[i]);
                m.setVersionNumber(versionNumbers[i]);
                m.setCategoryId(categoryIds[i]);
                m.setCategoryOrder(categoryOrders[i]);
                m.setIconName(iconFiles[i].getOriginalFilename());
                m.setIconDownloadUrl(fileUrlPrefix+m.getVersionType()+"/"+m.getVersionNumber()+"/"+m.getIconName());
                m.setMaterialName(materialFiles[i].getOriginalFilename());
                m.setMaterialDownloadUrl(fileUrlPrefix+m.getVersionType()+"/"+m.getVersionNumber()+"/"+m.getMaterialName());
                m.setAudio(audios[i]);
                File iconFile = new File(savePath+m.getVersionType()+"\\"+m.getVersionNumber(), m.getIconName());
                File materialFile = new File(savePath+m.getVersionType()+"\\"+m.getVersionNumber(), m.getMaterialName());
                FileUtils.copyInputStreamToFile(iconFiles[i].getInputStream(),iconFile );
                FileUtils.copyInputStreamToFile(materialFiles[i].getInputStream(), materialFile);
                m.setIconMD5(DigestUtils.md5DigestAsHex(FileCopyUtils.copyToByteArray(iconFile)));
                m.setMaterialMD5(DigestUtils.md5DigestAsHex(FileCopyUtils.copyToByteArray(materialFile)));
                materialPOS.add(m);
            }
            materialService.addMaterials(materialPOS);
        } catch (Exception e) {
            e.printStackTrace();
        }
		return mav;
	}
    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String addMaterialGo(@ModelAttribute("material") MaterialPO material,
                                Model model,
                                @RequestParam("iconFile") MultipartFile iconFile,
                                @RequestParam("materialFile") MultipartFile materialFile) {
        try {
            String savePath = Global.getConfig("nginx.root.materials")+material.getVersionType()
                    +"\\"+material.getVersionNumber(); //+ Constants.MATERIALS_DIRECTORY + "\\test";
            try {
                File saveFileDir = new File(savePath);
                if(!saveFileDir.exists()){
                    saveFileDir.mkdirs();
                }
                if (!iconFile.isEmpty() && !materialFile.isEmpty()) {
                    material.setIconName(iconFile.getOriginalFilename());
                    material.setMaterialName(materialFile.getOriginalFilename());
                    File iconFileLocal = new File(savePath, iconFile.getOriginalFilename());
                    File materialFileLocal = new File(savePath, materialFile.getOriginalFilename());
                    FileUtils.copyInputStreamToFile(iconFile.getInputStream(), iconFileLocal);
                    FileUtils.copyInputStreamToFile(materialFile.getInputStream(), materialFileLocal);
                }
                materialService.addMaterial(material, savePath);
            } catch (Exception e) {
                e.printStackTrace();
                model.addAttribute("material", material);
                List<MaterialCategory> categoryList = categoryService.getCategories(null, null, null);
                model.addAttribute("categoryList", categoryList);
                model.addAttribute("errorMsg",e.getMessage());
                return "material/add";
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            model.addAttribute("material", material);
            List<MaterialCategory> categoryList = categoryService.getCategories(null, null, null);
            model.addAttribute("categoryList", categoryList);
            return "material/add";
        }
        return "redirect:/admin/material/listMaterials";
    }

    @RequestMapping(value = "exportExcel", method = RequestMethod.POST)
    public String exportMaterialRecords2Excel(ModelMap modelMap, Model model,
                                              @RequestParam(value = "materialRecords") String action,
                                              @RequestParam String versionType,
                                              @RequestParam String online,
                                              @RequestParam(required = false) String order,
                                              //@RequestParam(required=false) String versionNumber,
                                              @RequestParam(required = false) String from,
                                              @RequestParam(required = false) String to) {
        if (StringUtils.isNotBlank(action) && StringUtils.isNotBlank(versionType)) {
            /*model.addAttribute("action", action);
            model.addAttribute("versionType", versionType);*/
            List<Object[]> list = null;
            List<MaterialExcel> entityList = null;
            List<MaterialShareRecord> shareRecords = null;
            MaterialExcel me = null;
            if (MaterialAction.MATERIAL_PIC_SHARE.equals(action)
                    || MaterialAction.MATERIAL_VIDEO_SHARE.equals(action)) {
                List<MaterialShareRecord> materialShareRecords = recordService.getRecordsWithMaterialChannel(action,
                        versionType, StringUtils.EMPTY, from, to, online, order);
                modelMap.put(NormalExcelConstants.DATA_LIST, materialShareRecords);
                modelMap.put(NormalExcelConstants.CLASS, MaterialShareRecord.class);
                modelMap.put(NormalExcelConstants.FILE_NAME, "素材分享相关数据统计");
                modelMap.put(NormalExcelConstants.PARAMS, new ExportParams(actionWithText.get(action), action));
				/*entityList = new ArrayList<MaterialExcel>();
				for(Object[] objects : list){
					me = new MaterialExcel();
					me.setUuid(objects[0].toString());
					me.setVersionType(objects[1].toString());
					me.setVersionNumber(objects[2].toString());
					if(objects[3] == null){
						objects[3] = "";
					}
					me.setChannel(objects[3].toString());
					me.setNum(objects[4].toString());
					entityList.add(me);
				}*/
            } else {
                list = recordService.getRecordsWithParams(action, versionType, StringUtils.EMPTY, from, to,
                        online, order);
                entityList = new ArrayList<MaterialExcel>();
                for (Object[] objects : list) {
                    me = new MaterialExcel();
                    me.setUuid(objects[0].toString());
                    me.setVersionType(objects[1].toString());
                    me.setVersionNumber(objects[2].toString());
                    me.setNum(objects[3].toString());
                    entityList.add(me);
                }
                modelMap.put(NormalExcelConstants.DATA_LIST, entityList);
                modelMap.put(NormalExcelConstants.CLASS, MaterialExcel.class);
                //modelMap.put(MapExcelConstants.MAP_LIST, dataResult);
                modelMap.put(NormalExcelConstants.FILE_NAME, "素材相关数据统计");
                modelMap.put(NormalExcelConstants.PARAMS, new ExportParams(actionWithText.get(action), action));
            }


        }

        return NormalExcelConstants.JEECG_EXCEL_VIEW;
    }

    @RequestMapping(value = "categoryList", method = RequestMethod.GET)
    public String categoryManage(Model model) {
        List<MaterialCategory> categories = categoryService.getCategories2(null, null, null);
        if (categories != null) {
            model.addAttribute("categories", categories);
        }
        return "materialCategory/list";
    }

    /*@RequestMapping(value = "categoryList", method = RequestMethod.POST)
    public String categoryManageGo(Model model) {
        List<MaterialCategory> categories = categoryService.getCategories(null,null,null);
        if (categories != null) {
            model.addAttribute("categories", categories);
        }
        return "materialCategory/list";
    }*/


    @RequestMapping(value = "addCategory", method = RequestMethod.GET)
    public String addCategory() {

        return "materialCategory/add";
    }

    @RequestMapping(value = "deleteCategory", method = RequestMethod.GET)
    public String addCategoryPost(@RequestParam int id) {
        categoryService.deleteCategory(id);
        return "redirect:/admin/material/categoryList";
    }

    @RequestMapping(value = "addCategory", method = RequestMethod.POST)
    public String addCategoryPost(@RequestParam String categoryName,
                                  @RequestParam Integer order
                                  //@RequestParam String gender,
    ) {
        if (StringUtils.isEmpty(categoryName)) {
            return "materialCategory/add";
        }
        try {
            MaterialCategory category = new MaterialCategory();
            category.setCategoryName(categoryName);
            //category.setGender(gender);
            category.setCategoryOrder(order);
            /*category.setVersionNum(versionNum);
            category.setVersionType(versionType);*/
            categoryService.saveMaterialCategory(category);
        } catch (Exception e) {
            e.printStackTrace();
            return "redirect:/admin/material/categoryList";
        }

        return "redirect:/admin/material/categoryList";
    }

    @RequestMapping(value = "editCategory", method = RequestMethod.GET)
    public String addCategory(@RequestParam int id,Model model) {
        MaterialCategory category = categoryService.getCategoryById(id);
        model.addAttribute("category",category);
        return "materialCategory/edit";
    }

    @RequestMapping(value = "editCategory", method = RequestMethod.POST)
    public String addCategory(@RequestParam("categoryId") int id,
                              @RequestParam String categoryName,
                              @RequestParam int categoryOrder) {

        categoryService.updateCategory(new MaterialCategory(id,categoryName,categoryOrder));
        return "redirect:/admin/material/categoryList";
    }


    @RequestMapping(value = "uploadConfigFile", method = RequestMethod.GET)
    public ModelAndView uploadFile() {
        return new ModelAndView("material/uploadFile");
    }

    @RequestMapping(value = "uploadConfigFile", method = RequestMethod.POST)
    public ModelAndView uploadFileGo(@RequestParam MultipartFile file, HttpServletRequest request, ModelMap model) {

        String path = request.getSession().getServletContext().getRealPath("upload");


        String fileName = file.getOriginalFilename();
//        String fileName = new Date().getTime()+".jpg";  
        File targetFile = new File(path, fileName);
        if (!targetFile.exists()) {
            targetFile.mkdirs();
        }
        List<String> lines = null;
        //保存  
        try {
            file.transferTo(targetFile);
            lines = FileUtils.readLines(targetFile, "utf-8");
            for (int i = 1; i < lines.size(); i++) {
                String line = lines.get(i);
                //String [] attrs = line.split(";");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (lines != null) {
            model.addAttribute("dataList", lines);
        }
        model.addAttribute("fileUrl", request.getContextPath() + "/upload/" + fileName);

        return new ModelAndView("material/uploadFile");
    }

    @RequestMapping(value = "exportConfigFile", method = RequestMethod.GET)
    public ModelAndView exportConfigFile() {
        return new ModelAndView("material/exportConfigFile");
    }

    /*@RequestMapping(value = "exportConfigFile", method = RequestMethod.POST)
    public ModelAndView exportConfigFileGo(@RequestParam String versionType,
                                           @RequestParam String versionNumber,
                                           HttpServletRequest request,
                                           HttpServletResponse response) {
        List<MaterialPO> materialPOs = materialService.findAllMaterialList(versionType, versionNumber);
        String filePath = "d:\\configFile\\config.txt";
        File configFile = new File(filePath);
        if (!configFile.exists()) {
            boolean createStatus = FileUtils.createFile(filePath);
            if (!createStatus) {
                throw new RuntimeException("create file error");
            }
        }

        StringBuilder sb = new StringBuilder("#素材文件列表，使用分号(;)分割。排列顺序即客户端显示顺序。格式：格式："
                + "【素材标示】【图标文件名】【素材文件名】【是否推荐】【推荐顺序】【对应的类别素材顺序】【素材的性别取向 1代表男 0代表女】\r\n");
        for (MaterialPO m : materialPOs) {
            sb.append(m.getUuid()).append(";").append(m.getIconName()).append(";").append(m.getMaterialName())
                    .append(";").append(m.getIsRecommend().equals("1") ? "y" : "n").append(";")
                    .append(m.getRecommendOrder()).append(";").append(m.getCategoryOrder()).append(";")
                    .append(m.getCategory()).append(";").append(m.getGender()).append("\r\n");
        }
        FileUtils.writeToFile(filePath, sb.toString(), false);
        //configFile = new File(filePath);
        FileUtils.downFile(configFile, request, response, "config.txt");
        return new ModelAndView("material/exportConfigFile");
    }*/

}
