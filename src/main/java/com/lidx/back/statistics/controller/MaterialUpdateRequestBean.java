package com.lidx.back.statistics.controller;

import java.io.File;

import com.lidx.back.statistics.entity.MaterialPO;


public class MaterialUpdateRequestBean {
	private MaterialPO materialPo = new MaterialPO();

	private File iconFile;

	private File materialFile;

	public MaterialPO getMaterialPo() {
		return materialPo;
	}

	public void setMaterialPo(MaterialPO materialPo) {
		this.materialPo = materialPo;
	}

	public File getIconFile() {
		return iconFile;
	}

	public void setIconFile(File iconFile) {
		this.iconFile = iconFile;
	}

	public File getMaterialFile() {
		return materialFile;
	}

	public void setMaterialFile(File materialFile) {
		this.materialFile = materialFile;
	}
}
