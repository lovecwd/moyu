package com.lidx.back.statistics.rest.material;

import java.util.List;

import com.lidx.back.statistics.entity.MaterialPO;

public class MaterialResponse{

	private String rspMsg ;
	
	private RestMaterialBody body;
	
	public RestMaterialBody getBody() {
		return body;
	}

	public void setBody(RestMaterialBody body) {
		this.body = body;
	}

	public String getRspMsg() {
		return rspMsg;
	}

	public void setRspMsg(String rspMsg) {
		this.rspMsg = rspMsg;
	}
	
	
}
