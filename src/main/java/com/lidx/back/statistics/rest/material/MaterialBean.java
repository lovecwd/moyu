package com.lidx.back.statistics.rest.material;

import com.lidx.back.statistics.entity.MaterialPO;
import com.lidx.back.statistics.utils.DateUtils;

import java.util.Date;

public class MaterialBean {
	private int id;

	private String uuid;
	//推荐素材的顺序
	//private String recommendOrder;
	private int categoryId;
	//类别顺序
	private int categoryOrder;
	// 是否是推荐素材
	//private String isRecommend;
	// 图标文件名
	private String iconName;
	// 图标文件MD5
	private String iconMD5;
	// 图标文件下载路径
	private String iconDownloadUrl;

	// 素材文件名
	private String materialName;
	// 素材文件MD5
	private String materialMD5;
	// 素材文件下载路径
	private String materialDownloadUrl;

	private String createTime;
	private String updateTime;

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	/*//素材是否是内置的（0,1）
            private String isIconDownloaded;*/
	//素材类别
	//private String category;
	private boolean audio;

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	/*public String getRecommendOrder() {
		return recommendOrder;
	}

	public void setRecommendOrder(String recommendOrder) {
		this.recommendOrder = recommendOrder;
	}*/

	public int getCategoryOrder() {
		return categoryOrder;
	}

	public void setCategoryOrder(int categoryOrder) {
		this.categoryOrder = categoryOrder;
	}

	/*public String getIsRecommend() {
			return isRecommend;
        }

        public void setIsRecommend(String isRecommend) {
            this.isRecommend = isRecommend;
        }
    */
	public String getIconName() {
		return iconName;
	}

	public void setIconName(String iconName) {
		this.iconName = iconName;
	}

	public String getIconMD5() {
		return iconMD5;
	}

	public void setIconMD5(String iconMD5) {
		this.iconMD5 = iconMD5;
	}

	public String getIconDownloadUrl() {
		return iconDownloadUrl;
	}

	public void setIconDownloadUrl(String iconDownloadUrl) {
		this.iconDownloadUrl = iconDownloadUrl;
	}

	public String getMaterialName() {
		return materialName;
	}

	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}

	public String getMaterialMD5() {
		return materialMD5;
	}

	public void setMaterialMD5(String materialMD5) {
		this.materialMD5 = materialMD5;
	}

	public String getMaterialDownloadUrl() {
		return materialDownloadUrl;
	}

	public void setMaterialDownloadUrl(String materialDownloadUrl) {
		this.materialDownloadUrl = materialDownloadUrl;
	}

	/*public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}*/

	public boolean isAudio() {
		return audio;
	}

	public void setAudio(boolean audio) {
		this.audio = audio;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public MaterialBean(MaterialPO po) {
		this.setId(po.getId());
		this.setAudio(Boolean.parseBoolean(po.getAudio()));
		this.setCategoryId(po.getCategoryId());
		this.setCategoryOrder(po.getCategoryOrder());
		this.setIconDownloadUrl(po.getIconDownloadUrl());
		this.setIconMD5(po.getIconMD5());
		this.setIconName(po.getIconName());
		this.setMaterialDownloadUrl(po.getMaterialDownloadUrl());
		this.setMaterialMD5(po.getMaterialMD5());
		this.setMaterialName(po.getMaterialName());
		this.setUuid(po.getUuid());
		this.setCreateTime(po.getCreateTime().toString());
		this.setUpdateTime(po.getUpdateTime().toString());
	}
}
