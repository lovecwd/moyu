package com.lidx.back.statistics.rest.material;

import java.util.List;

public class GetCategoriesBody {
	private List<CategoryBean> categories;

	public List<CategoryBean> getCategories() {
		return categories;
	}

	public void setCategories(List<CategoryBean> categories) {
		this.categories = categories;
	}
}
