package com.lidx.back.statistics.rest.material;

import org.springframework.util.StringUtils;

import com.lidx.back.statistics.rest.VersionBean;


public class GetRecommendMaterialsInputCheck {
	
	public void checkInput(RecommendMaterialRequest input) throws Exception{
		String deviceId = input.getDeviceId();
		if(StringUtils.isEmpty(deviceId))
			throw new Exception("deviceId不能为空");
		VersionBean versionBean = input.getVersion();
		if(versionBean == null){
			throw new Exception("版本信息不能为空");
		}
		String versionNum = versionBean.getVersionNumber();
		String versionType = versionBean.getVersionType();
		if(StringUtils.isEmpty(versionType) || StringUtils.isEmpty(versionNum))
			throw new Exception("版本类型和版本号不能为空");
		
	}
}
