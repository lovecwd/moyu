package com.lidx.back.statistics.rest.material;

import java.util.List;

public class RestMaterialBody {
	private List<MaterialBean> materials;
	public List<MaterialBean> getMaterials() {
		return materials;
	}

	public void setMaterials(List<MaterialBean> materials) {
		this.materials = materials;
	}
}
