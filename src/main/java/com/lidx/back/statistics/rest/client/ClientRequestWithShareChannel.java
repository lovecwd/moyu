package com.lidx.back.statistics.rest.client;

import com.lidx.back.statistics.rest.VersionBean;

public class ClientRequestWithShareChannel {
	/**
	 * 分享渠道
	 */
	private String channel;
	private String deviceId;
	private VersionBean version;
	private String online;
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public VersionBean getVersion() {
		return version;
	}
	public void setVersion(VersionBean version) {
		this.version = version;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getOnline() {
		return online;
	}

	public void setOnline(String online) {
		this.online = online;
	}
}
