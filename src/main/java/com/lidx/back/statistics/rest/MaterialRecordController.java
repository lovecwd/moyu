package com.lidx.back.statistics.rest;

import com.lidx.back.statistics.commons.MaterialAction;
import com.lidx.back.statistics.entity.MaterialRecord;
import com.lidx.back.statistics.rest.material.MaterialRecordPool;
import com.lidx.back.statistics.service.IMaterialRecordService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("material")
public class MaterialRecordController {

    protected static final Logger logger = LoggerFactory.getLogger(MaterialRecordController.class);

    @Autowired
    private IMaterialRecordService recordService;

    //private static GenericObjectPool<MaterialRecord> pool = null;
    private static MaterialRecordPool pool = null;

    static {
        /*GenericObjectPoolConfig conf = new GenericObjectPoolConfig();
        conf.setMaxTotal(100);
        conf.setMaxIdle(100);
        conf.setMinIdle(30);
        conf.setTestOnReturn(false);
        conf.setTimeBetweenEvictionRunsMillis(300000);
        pool = new GenericObjectPool<MaterialRecord>(new MaterialRecordFactory(), conf);*/
        pool = new MaterialRecordPool(100,50);
    }



    /*
    单个素材的下载次数
     */
    @RequestMapping(value = {"download"}, method = {org.springframework.web.bind.annotation.RequestMethod.POST})
    public RestRecordResponse downloadMaterial(@RequestBody CountRequest request) {
        MaterialRecord materialDownload = null;
        try{
            materialDownload = checkInput(request);
            materialDownload.setAction(MaterialAction.MATERIAL_DOWNLOAD);
            logger.debug("materialRecord action : {}",MaterialAction.MATERIAL_DOWNLOAD);
            this.recordService.saveRecord(materialDownload);
        }catch (Exception e){
            e.printStackTrace();
            return new RestRecordResponse("失败",e.getMessage());
        }finally {
            pool.returnObject(materialDownload);
        }
        return new RestRecordResponse("成功",null);

    }

   /* @RequestMapping(value = {"makeVideo"}, method = {org.springframework.web.bind.annotation.RequestMethod.POST})
    public void makeVideo(@RequestBody CountRequest request) {
        MaterialRecord count = null;
        *//*VersionBean version = request.getVersion();
        String uuid = request.getMaterialUuid();
        String online = request.getOnline();
        if ((null != request) && (null != version) && (!StringUtils.isEmpty(uuid)) &&
                (!StringUtils.isEmpty(version.getVersionNumber())) && (!StringUtils.isEmpty(version.getVersionType())) && !StringUtils.isEmpty(online)) {*//*
        try{
            count = checkInput(request);
            count.setAction(MaterialAction.MATERIAL_MAKE_VIDEO);
            logger.debug("materialRecord action : {} begin",MaterialAction.MATERIAL_MAKE_VIDEO);
            this.recordService.saveRecord(count);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            pool.returnObject(count);
        }

    }*/

    /*
    单个素材的拍照次数
     */
    @RequestMapping(value = {"photograph"}, method = {org.springframework.web.bind.annotation.RequestMethod.POST})
    public RestRecordResponse photographWithMaterial(@RequestBody CountRequest request) {
        MaterialRecord count = null;
        try{
            count = checkInput(request);
            count.setAction(MaterialAction.MATERIAL_PHOTOGRAPH);
            logger.debug("materialRecord action : {} begin",MaterialAction.MATERIAL_PHOTOGRAPH);
            this.recordService.saveRecord(count);
        }catch (Exception e){
            e.printStackTrace();
            return new RestRecordResponse("失败",e.getMessage());
        }finally {
            pool.returnObject(count);
        }
        return new RestRecordResponse("成功",null);
    }

    /*
   单个素材拍照后下载本地的次数
    */
    @RequestMapping(value = {"picDownload"}, method = {org.springframework.web.bind.annotation.RequestMethod.POST})
    public RestRecordResponse picDownloadLocal(@RequestBody CountRequest request) {
        MaterialRecord count = null;
        try{
            count = checkInput(request);
            count.setAction(MaterialAction.MATERIAL_PIC_DOWNLOAD);
            logger.debug("materialRecord action : {} begin",MaterialAction.MATERIAL_PHOTOGRAPH);
            this.recordService.saveRecord(count);
        }catch (Exception e){
            return new RestRecordResponse("失败",e.getMessage());
        }finally {
            pool.returnObject(count);
        }
        return new RestRecordResponse("成功",null);
    }

    /*
    单个素材拍照后分享次数
     */
    @RequestMapping(value = {"picShare"}, method = {org.springframework.web.bind.annotation.RequestMethod.POST})
    public RestRecordResponse picShareWithMaterial(@RequestBody CountRequest request) {
        MaterialRecord count = null;
        try {
            count = checkInput(request);
            count.setAction(MaterialAction.MATERIAL_PIC_SHARE);
            logger.debug("materialRecord action : {} begin",MaterialAction.MATERIAL_PIC_SHARE);
            this.recordService.saveRecord(count);
        }catch (Exception e){
            return new RestRecordResponse("失败",e.getMessage());
        }finally {
            pool.returnObject(count);
        }
        return new RestRecordResponse("成功",null);
    }

    /*
    单个素材录像后分享次数
     */
    @RequestMapping(value = {"videoShare"}, method = {org.springframework.web.bind.annotation.RequestMethod.POST})
    public RestRecordResponse videoShareWithMaterial(@RequestBody CountRequest request) {
        MaterialRecord count = null;
        try {
            count = checkInput(request);
            count.setAction(MaterialAction.MATERIAL_VIDEO_SHARE);
            logger.debug("materialRecord action : {} begin",MaterialAction.MATERIAL_VIDEO_SHARE);
            this.recordService.saveRecord(count);
        }catch (Exception e){
            return new RestRecordResponse("失败",e.getMessage());
        }finally {
            pool.returnObject(count);
        }
        return new RestRecordResponse("成功",null);
    }
    /*
    单个素材的录像次数
     */
    @RequestMapping(value = {"video"}, method = {org.springframework.web.bind.annotation.RequestMethod.POST})
    public RestRecordResponse videoWithMaterial(@RequestBody CountRequest request) {
        MaterialRecord count = null;
        try {
            count = checkInput(request);
            count.setAction(MaterialAction.MATERIAL_VIDEO);
            logger.debug("materialRecord action : {} begin",MaterialAction.MATERIAL_VIDEO);
            this.recordService.saveRecord(count);
        }catch (Exception e){
            return new RestRecordResponse("失败",e.getMessage());
        }finally {
            pool.returnObject(count);
        }
        return new RestRecordResponse("成功",null);
    }

    /*
    单个素材录像后下载本地的次数
     */
    @RequestMapping(value = {"videoDownload"}, method = {org.springframework.web.bind.annotation.RequestMethod.POST})
    public RestRecordResponse videoDownload(@RequestBody CountRequest request) {
        MaterialRecord count = null;
        try {
            count = checkInput(request);
            count.setAction(MaterialAction.MATERIAL_VIDEO_DOWNLOAD);
            logger.debug("materialRecord action : {} begin",MaterialAction.MATERIAL_VIDEO_DOWNLOAD);
            this.recordService.saveRecord(count);
        }catch (Exception e){
            return new RestRecordResponse("失败",e.getMessage());
        }finally {
            pool.returnObject(count);
        }
        return new RestRecordResponse("成功",null);
    }
    private static MaterialRecord checkInput(CountRequest request) throws Exception {
        MaterialRecord record = null;
        VersionBean version = request.getVersion();
        String uuid = request.getParameters().getMaterialUuid();
        String deviceId = request.getDeviceId();
        if ((null != request) && (null != version) && (!StringUtils.isEmpty(uuid))
                &&(!StringUtils.isEmpty(version.getVersionNumber()))
                && (!StringUtils.isEmpty(version.getVersionType()))){
            record = pool.borrowObject();
            logger.debug("pool materialRecord size: {}",MaterialRecordPool.getMaterialRecordsSize());
            record.setUuid(uuid);
            record.setVersionNum(version.getVersionNumber());
            record.setVersionType(version.getVersionType());
            record.setDeviceId(deviceId);
            return record;
        }
        return null;
    }

    private static MaterialRecord checkInput(CountRequestWithChannel request) throws Exception {
        MaterialRecord record = null;
        VersionBean version = request.getVersion();
        String uuid = request.getMaterialUuid();
        String online = request.getOnline();
        String deviceId = request.getDeviceId();
        String channel = request.getChannel();
        if ((null != request) && (null != version) && (!StringUtils.isEmpty(uuid))
                &&(!StringUtils.isEmpty(version.getVersionNumber()))
                && (!StringUtils.isEmpty(version.getVersionType()))
                && !StringUtils.isEmpty(online)
                && (!StringUtils.isEmpty(channel))){
            record = pool.borrowObject();
            logger.debug("pool materialRecord size: {}",MaterialRecordPool.getMaterialRecordsSize());
            record.setUuid(uuid);
            record.setVersionNum(version.getVersionNumber());
            record.setVersionType(version.getVersionType());
            record.setDeviceId(deviceId);
            record.setChannel(channel);
            return record;
        }
        return null;
    }

}