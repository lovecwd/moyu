package com.lidx.back.statistics.rest;

import com.google.common.collect.Lists;
import com.lidx.back.statistics.commons.Constants;
import com.lidx.back.statistics.entity.ClientInfo;
import com.lidx.back.statistics.entity.FeedBack;
import com.lidx.back.statistics.entity.MaterialCategory;
import com.lidx.back.statistics.entity.MaterialPO;
import com.lidx.back.statistics.rest.app.AppInitRequestCheck;
import com.lidx.back.statistics.rest.app.AppVersionCheckBody;
import com.lidx.back.statistics.rest.app.AppVersionCheckResponse;
import com.lidx.back.statistics.rest.feedback.FeedbackInputCheck;
import com.lidx.back.statistics.rest.feedback.FeedbackResponse;
import com.lidx.back.statistics.rest.material.*;
import com.lidx.back.statistics.service.*;
import com.lidx.back.statistics.utils.BeanCopyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
/**
 * @author chenweidong
 * 提供app端获取数据的api接口
 */
@RestController
@RequestMapping(value="app")
public class RestApi {
	
	protected static final Logger LOGGER = LoggerFactory.getLogger(RestApi.class);

	@Autowired
	private IAppInitService appInitService;
	@Autowired
	private IMaterialService materialService;
	@Autowired
	private IFeedbackService feedbackService;
	@Autowired
	private IMaterialCategoryService categoryService;
	@Autowired
	private ISystemVersionService versionService;
	@Autowired
	private ILatestMaterialService latestMaterialService;
	
	@RequestMapping(value="init",method=RequestMethod.POST,consumes="application/json")
	public RestResponse appInit(@RequestBody RestRequest clientInfoRequest){
		RestResponse restResponse = new RestResponse();
		RestResponseBody body = new RestResponseBody();
		try{
			Assert.notNull(clientInfoRequest, "请求不能为空");
			AppInitRequestCheck inputCheck = new AppInitRequestCheck();
			ClientInfo info = inputCheck.checkInput(clientInfoRequest);
			if(!appInitService.checkInfoIfExists(info)){
				LOGGER.debug("save client info.deviceid: {}",info.getDeviceId());
				appInitService.saveClientInfo(info);
			}
			restResponse.setRspMsg(Constants.RESPONSE_SECCESS);
			restResponse.setBody(body);
		}catch(java.lang.Exception e){
			e.printStackTrace();
			restResponse.setRspMsg(Constants.RESPONSE_ERROR+e.getMessage());
			restResponse.setBody(body);
		}
		return restResponse;
	}

	//获取一个分类下所有的素材
	@RequestMapping(value="getCategoryMaterials",method=RequestMethod.POST,consumes="application/json")
	public MaterialResponse getAllMaterials(@RequestBody RestRequest materialRequest) {
		MaterialResponse materialResponse = new MaterialResponse();
		try{
			Assert.notNull(materialRequest, "请求不能为空");
			GetAllMaterialsInputCheck inputCheck = new GetAllMaterialsInputCheck();
			inputCheck.checkInput(materialRequest);
			VersionBean versionBean = materialRequest.getVersion();
			String versionType = versionBean.getVersionType();
			String versionNumber = versionBean.getVersionNumber();
			int categoryId = Integer.parseInt(materialRequest.getParameters().getCategoryId());
			LOGGER.debug("getAllMaterials params 手机版本{}, app版本号{}", versionType, versionNumber);
			List<MaterialPO> materialPOs = materialService.findAllMaterialListByCategoryId(versionType, versionNumber, categoryId);
			List<MaterialBean> materialBeans = Lists.newArrayList();
			MaterialBean bean = null;
			for(MaterialPO po : materialPOs){
				bean = new MaterialBean(po);
				materialBeans.add(bean);
			}
			RestMaterialBody body = new RestMaterialBody();
			body.setMaterials(materialBeans);
			materialResponse.setBody(body);
			materialResponse.setRspMsg(Constants.RESPONSE_SECCESS);
		}catch(java.lang.Exception e){
			e.printStackTrace();
			materialResponse.setRspMsg(Constants.RESPONSE_ERROR+e.getMessage());
		}
		return materialResponse;
	}
	
	@RequestMapping(value="feedback",method=RequestMethod.POST,consumes="application/json")
	public FeedbackResponse sendFeedBack(@RequestBody RestRequest feedbackRequest){
		FeedbackResponse feedbackResponse = new FeedbackResponse();
		RestResponseBody body = new RestResponseBody();
		try{
			Assert.notNull(feedbackRequest);
			FeedbackInputCheck inputCheck = new FeedbackInputCheck();
			FeedBack feedBack = inputCheck.checkInput(feedbackRequest);
			LOGGER.debug("save feedback begin.");
			feedbackService.saveFeedback(feedBack);
			feedbackResponse.setRspMsg(Constants.RESPONSE_SECCESS);
			feedbackResponse.setBody(body);
		}catch(java.lang.Exception e){
			e.printStackTrace();
			feedbackResponse.setRspMsg(Constants.RESPONSE_ERROR+e.getMessage());
			feedbackResponse.setBody(body);
		}
		return feedbackResponse;
	}

	@RequestMapping(value="getMaterialCategories",method=RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE)
	public GetCategoriesResponse getAllCategories(@RequestBody RestRequest request){
		GetCategoriesResponse response = new GetCategoriesResponse();
		GetCategoriesBody body = new GetCategoriesBody();
		try {
			Assert.notNull(request);
			String deviceId = request.getDeviceId();
			if(StringUtils.isEmpty(deviceId)){
				throw new RuntimeException("deviceId不能为空");
			}
			LOGGER.debug("get categories, deviceid is {} ",deviceId);
			VersionBean version = request.getVersion();
			String versionType = version.getVersionType();
			String versionNum = version.getVersionNumber();
			if(StringUtils.isEmpty(versionType) || StringUtils.isEmpty(versionNum)){
				LOGGER.error("version empty!");
				throw new RuntimeException("version 信息不能为空");
			}
			List<MaterialCategory> materialCategories = categoryService.getCategories(deviceId,null,null);
			List<MaterialCategory> sortedMaterialCategories = Lists.newArrayList(materialCategories);
			
			for(MaterialCategory category : materialCategories){
				int order = category.getCategoryOrder();
				sortedMaterialCategories.set(order - 1, category);
			}
			
			List<CategoryBean> categoryBeans =Lists.newArrayList();
			CategoryBean categoryBean;
			for(int i = 0;i < sortedMaterialCategories.size();i++){
				categoryBean = new CategoryBean();
				MaterialCategory category = sortedMaterialCategories.get(i);
				BeanCopyUtils.copySamePropertiesTo(category, categoryBean);
				categoryBeans.add(categoryBean);
			}
			body.setCategories(categoryBeans);
			response.setBody(body);
			response.setRspMsg(Constants.RESPONSE_SECCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.setRspMsg(Constants.RESPONSE_ERROR+e.getMessage());
			response.setBody(null);
		}
		return response;
	}
	@RequestMapping(value="appVersionCheck",method=RequestMethod.POST,consumes="application/json")
	public AppVersionCheckResponse versionCheck(@RequestBody RestRequest request){
		AppVersionCheckResponse response = new AppVersionCheckResponse();
		AppVersionCheckBody body = new AppVersionCheckBody();
		try{
			Assert.notNull(request);
			VersionBean versionBean = request.getVersion();
			body = versionService.getVersionCheck(versionBean);
			response.setBody(body);
			response.setRspMsg(Constants.RESPONSE_SECCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.setRspMsg(Constants.RESPONSE_ERROR+e.getMessage());
			response.setBody(null);
		}
		return response;
	}

	@RequestMapping(value = "getLatestMaterialIds", method = RequestMethod.POST, consumes = "application/json")
	public LatestMaterialIdsResponse getLatestMaterialIds(@RequestBody RestRequest request) {
		LatestMaterialIdsResponse response = new LatestMaterialIdsResponse();
		LatestMaterialIdsRspBody body = new LatestMaterialIdsRspBody();
		try {
			Assert.notNull(request);
			VersionBean versionBean = request.getVersion();
			int[] materialIds = latestMaterialService.getLatestMaterialIds(versionBean.getVersionType(), versionBean.getVersionNumber());
			body.setMaterialIds(materialIds);
			response.setBody(body);
			response.setRspMsg(Constants.RESPONSE_SECCESS);
		}catch(Exception e){
			e.printStackTrace();
			response.setRspMsg(Constants.RESPONSE_ERROR+e.getMessage());
			response.setBody(null);
		}
		return response;
	}

	@RequestMapping(value = "getMaterialById", method = RequestMethod.POST, consumes = "application/json")
	public RestResponse getLatestMaterials(@RequestBody RestRequest request) {
		RestResponse response = new RestResponse();
		RestResponseBody body = new RestResponseBody();
		try {
			Assert.notNull(request);
			Integer materialId = request.getParameters().getMaterialId();
			if(null == materialId){
				throw new RuntimeException("materialId cant be null");
			}
			MaterialPO m = materialService.getMaterialById(materialId);
			body.setMaterial(new MaterialBean(m));
			response.setBody(body);
			response.setRspMsg(Constants.RESPONSE_SECCESS);
		}catch(Exception e){
			e.printStackTrace();
			response.setRspMsg(Constants.RESPONSE_ERROR);
			body.setDetailedMsg(e.getMessage());
			response.setBody(null);
		}
		return response;
	}
}
