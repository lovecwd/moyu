package com.lidx.back.statistics.rest.material;

import com.lidx.back.statistics.entity.MaterialRecord;
import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;

/**
 * Created by chen on 2017/2/9.
 */
public class MaterialRecordFactory extends  BasePooledObjectFactory<MaterialRecord> {
    @Override
    public MaterialRecord create() throws Exception {
        return new MaterialRecord();
    }

    @Override
    public PooledObject<MaterialRecord> wrap(MaterialRecord materialRecord) {
        return new DefaultPooledObject<MaterialRecord>(materialRecord);
    }

    @Override
    public void destroyObject(PooledObject<MaterialRecord> p) throws Exception {
        MaterialRecord mr = p.getObject();
        mr.setVersionNum(null);
        mr.setVersionType(null);
        mr.setAction(null);
        mr.setChannel(null);
        mr.setDeviceId(null);
        mr.setId(null);
        mr.setRecordTime(null);
        mr.setUuid(null);
        super.destroyObject(p);
    }

    @Override
    public boolean validateObject(PooledObject<MaterialRecord> p) {
        return false;
    }

    @Override
    public void passivateObject(PooledObject<MaterialRecord> p) throws Exception {
        super.passivateObject(p);
        MaterialRecord mr = p.getObject();
        mr.setVersionNum(null);
        mr.setVersionType(null);
        mr.setAction(null);
        mr.setChannel(null);
        mr.setDeviceId(null);
        mr.setId(null);
        mr.setRecordTime(null);
        mr.setUuid(null);
    }




  /*  @Override
    public PooledObject<MaterialRecord> makeObject() throws Exception {
        return new MaterialRecord();
    }

    @Override
    public void destroyObject(PooledObject<MaterialRecord> pooledObject) throws Exception {
        MaterialRecord mr = pooledObject.getObject();
        mr.setVersionNum(null);
        mr.setVersionType(null);
        mr.setAction(null);
        mr.setChannel(null);
        mr.setDeviceId(null);
        mr.setOnline(null);
        mr.setId(null);
        mr.setRecordTime(null);
        mr.setUuid(null);
    }

    @Override
    public boolean validateObject(PooledObject<MaterialRecord> pooledObject) {
        return false;
    }

    @Override
    public void activateObject(PooledObject<MaterialRecord> pooledObject) throws Exception {

    }

    @Override
    public void passivateObject(PooledObject<MaterialRecord> pooledObject) throws Exception {

    }*/
}
