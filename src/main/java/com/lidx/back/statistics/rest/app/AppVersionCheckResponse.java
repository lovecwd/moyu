package com.lidx.back.statistics.rest.app;


public class AppVersionCheckResponse {

	private String rspMsg;
	private AppVersionCheckBody body;
	
	public String getRspMsg() {
		return rspMsg;
	}

	public void setRspMsg(String rspMsg) {
		this.rspMsg = rspMsg;
	}

	public AppVersionCheckBody getBody() {
		return body;
	}

	public void setBody(AppVersionCheckBody body) {
		this.body = body;
	}

}
