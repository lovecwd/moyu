package com.lidx.back.statistics.rest.app;

import com.lidx.back.statistics.rest.client.ClientInfoRequest;
import org.springframework.util.StringUtils;

import com.lidx.back.statistics.commons.Constants;
import com.lidx.back.statistics.entity.ClientInfo;
import com.lidx.back.statistics.rest.RestRequest;
import com.lidx.back.statistics.rest.VersionBean;

public class AppInitRequestCheck {

	public ClientInfo checkInput(RestRequest request) throws Exception{
		if(StringUtils.isEmpty(request.getDeviceId())){
			throw new Exception("deviceId不能为空！");
		}
		ClientInfo clientInfo = new ClientInfo();
		clientInfo.setDeviceId(request.getDeviceId());
		/*String sex = request.getSex();
		if(!Constants.PERSON_SEX_FEMALE.equals(sex) && !Constants.PERSON_SEX_MALE.equals(sex)){
			throw new Exception("性别选择只能为男或者女");
		}
		clientInfo.setSex(request.getSex());
		String online = request.getOnline();
		if(StringUtils.isEmpty(online)){
			throw new Exception("用户 是否是线上的属性不能为空");
		}
		if(!"y".equals(online) && !"n".equals(online)){
			throw new RuntimeException("是否是线上的属性只能为y或者n");
		}
		clientInfo.setOnline(online);*/
		VersionBean version = request.getVersion();
		if(null != version){
			if(!StringUtils.isEmpty(version.getVersionNumber())){
				clientInfo.setVersionNum(version.getVersionNumber());
			}
			if(!StringUtils.isEmpty(version.getVersionType())){
				clientInfo.setVersionType(version.getVersionType());
			}
		}
		return clientInfo;
	}
}
