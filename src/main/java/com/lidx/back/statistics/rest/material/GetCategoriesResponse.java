package com.lidx.back.statistics.rest.material;

import java.util.List;

public class GetCategoriesResponse {

	private String rspMsg;
	
	private GetCategoriesBody body;

	public GetCategoriesBody getBody() {
		return body;
	}

	public void setBody(GetCategoriesBody body) {
		this.body = body;
	}

	public String getRspMsg() {
		return rspMsg;
	}

	public void setRspMsg(String rspMsg) {
		this.rspMsg = rspMsg;
	}
	
}
