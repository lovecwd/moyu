package com.lidx.back.statistics.rest;

import com.lidx.back.statistics.entity.BeautyLevel;
import com.lidx.back.statistics.service.IBeautyLevelService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lidx.back.statistics.commons.FilterAction;
import com.lidx.back.statistics.entity.FilterRecord;
import com.lidx.back.statistics.service.IFilterRecordService;
/**
 * 
 * @author chenweidong
 *滤镜统计
 */
@RestController
@RequestMapping(value="filter")
public class FilterRecordController {

	@Autowired
	private IFilterRecordService filterService;

	@Autowired
	private IBeautyLevelService beautyLevelService;
	
	@RequestMapping(value="useFilter",method=RequestMethod.POST)
	public RestRecordResponse useFilter(@RequestBody CountRequest request){
		try {
			FilterRecord record = checkInput(request);
			if(null != record){
                record.setAction(FilterAction.FILTER_USE);
                filterService.saveRecord(record);
            }
		} catch (Exception e) {
			e.printStackTrace();
			return new RestRecordResponse("失败",e.getMessage());
		}
		return new RestRecordResponse("成功",null);
	}

	@RequestMapping(value="beauty",method=RequestMethod.POST)
	public RestRecordResponse Beauty(@RequestBody CountRequest request){
		try{
			if(null ==request.getParameters().getBeautyLevel()){
				throw new RuntimeException("beanutylevel cant be null!");
			}
			BeautyLevel beautyLevel = new BeautyLevel();
			beautyLevel.setLevel(request.getParameters().getBeautyLevel());
			beautyLevelService.saveBeautyLevel(beautyLevel);
		}catch (Exception e){
			e.printStackTrace();
			return new RestRecordResponse("失败",e.getMessage());
		}
		return new RestRecordResponse("成功",null);
	}
	
	/*@RequestMapping(value="makeFilter",method=RequestMethod.POST)
	public void makeFilter(@RequestBody CountRequest request){
		FilterRecord record = checkInput(request);
		if(null != record){
			record.setAction(FilterAction.FILTER_MAKE);
			filterService.saveRecord(record);
		}
	}*/
	
	private FilterRecord checkInput(CountRequest request){
		FilterRecord record = null;
		if(null != request){
			VersionBean version = request.getVersion();
			String deviceId = request.getDeviceId();
			String versionNum = version.getVersionNumber();
			String versionType = version.getVersionType();
			String filterName = request.getParameters().getFilterName();
			if(!StringUtils.isEmpty(deviceId) && !StringUtils.isEmpty(versionType)
					&& !StringUtils.isEmpty(versionNum)
					&& !StringUtils.isEmpty(filterName)){
				record = new FilterRecord();
				record.setDeviceId(deviceId);
				record.setVersionNum(versionNum);
				record.setVersionType(versionType);
				record.setFilterName(filterName);
			}
		}
		return record;
	}
	
}
