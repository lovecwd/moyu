package com.lidx.back.statistics.rest.feedback;

import com.lidx.back.statistics.rest.RestResponseBody;

public class FeedbackResponse {
	
	private String rspMsg;
	
	private RestResponseBody body;

	public String getRspMsg() {
		return rspMsg;
	}

	public void setRspMsg(String rspMsg) {
		this.rspMsg = rspMsg;
	}

	public RestResponseBody getBody() {
		return body;
	}

	public void setBody(RestResponseBody body) {
		this.body = body;
	}
	
}
