package com.lidx.back.statistics.rest;

public class CountRequest {

    private String deviceId;
    private VersionBean version;
    private CountParameters parameters;

    /*private String online;//对一条记录加入是否是线上的标识，app端传入 "y" or "n"*/

    /*public String getOnline() {
        return online;
    }

    public void setOnline(String online) {
        this.online = online;
    }*/

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public VersionBean getVersion() {
        return version;
    }

    public void setVersion(VersionBean version) {
        this.version = version;
    }

    public CountParameters getParameters() {
        return parameters;
    }

    public void setParameters(CountParameters parameters) {
        this.parameters = parameters;
    }
}
