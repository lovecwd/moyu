package com.lidx.back.statistics.rest.client;

import com.lidx.back.statistics.rest.VersionBean;

/**
 * Created by chen on 2017/2/6.
 * 新版本修改，无需其他字段 2017/4/10
 */
public class ClientInfoRequest {

    private String deviceId;
    //private String sex;
    //private String online;
    private VersionBean version;

    public VersionBean getVersion() {
        return version;
    }

    public void setVersion(VersionBean version) {
        this.version = version;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    /*public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getOnline() {
        return online;
    }

    public void setOnline(String online) {
        this.online = online;
    }*/
}
