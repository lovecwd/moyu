package com.lidx.back.statistics.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="materialupdaterequestmsg")
public class MaterialUpdateRequestInfo {

	private Integer id;
	private String versionType;
	private String versionNum;
	private String materialNames;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getVersionType() {
		return versionType;
	}
	public void setVersionType(String versionType) {
		this.versionType = versionType;
	}
	public String getVersionNum() {
		return versionNum;
	}
	public void setVersionNum(String versionNum) {
		this.versionNum = versionNum;
	}
	public String getMaterialNames() {
		return materialNames;
	}
	public void setMaterialNames(String materialNames) {
		this.materialNames = materialNames;
	}
	
}
