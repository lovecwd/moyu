package com.lidx.back.statistics.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.google.common.collect.Lists;
@Table(name="sys_role")
@Entity
public class Role implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -370021786334670456L;

	private Integer id;
	
	private String name;
	
	private List<SysUser> userList = Lists.newArrayList(); // 拥有用户列表

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@ManyToMany(mappedBy = "roleList", fetch=FetchType.LAZY)
	@OrderBy("id")
	@Fetch(FetchMode.SUBSELECT)
	public List<SysUser> getUserList() {
		return userList;
	}

	public void setUserList(List<SysUser> userList) {
		this.userList = userList;
	}
}
