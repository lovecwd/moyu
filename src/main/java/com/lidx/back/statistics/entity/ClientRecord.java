package com.lidx.back.statistics.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;

@Entity
@Table(name="client_record")
@DynamicInsert
public class ClientRecord {
	private Integer id;
	private String deviceId;
	private String versionType;
	private String versionNum;
	private String action;
	/**
	 * 录制视频的时长，单位秒
	 */
	private Long recordVideoTime;
	private Date recordTime;
	private String shareChannel;
	private String online;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getVersionType() {
		return versionType;
	}
	public void setVersionType(String versionType) {
		this.versionType = versionType;
	}
	public String getVersionNum() {
		return versionNum;
	}
	public void setVersionNum(String versionNum) {
		this.versionNum = versionNum;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public Date getRecordTime() {
		return recordTime;
	}
	public void setRecordTime(Date recordTime) {
		this.recordTime = recordTime;
	}
	public Long getRecordVideoTime() {
		return recordVideoTime;
	}
	public void setRecordVideoTime(Long recordVideoTime) {
		this.recordVideoTime = recordVideoTime;
	}
	public String getShareChannel() {
		return shareChannel;
	}
	public void setShareChannel(String shareChannel) {
		this.shareChannel = shareChannel;
	}

	public String getOnline() {
		return online;
	}

	public void setOnline(String online) {
		this.online = online;
	}
}
