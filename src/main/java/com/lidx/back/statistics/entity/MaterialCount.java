package com.lidx.back.statistics.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="material_count")
public class MaterialCount {
	private Integer id;
	// 特定素材的唯一标示；同一素材的不同版本的该值应该相同。
	private String uuid;
	private String versionType;
	private String versionNum;
	private Integer downloadNum = 0;
	private Integer makeVideoNum = 0;
	//拍照数量
	private Integer photographNum = 0;
	
	private Integer picShareNum = 0;
	
	private Integer videoShareNum = 0;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getVersionType() {
		return versionType;
	}
	public void setVersionType(String versionType) {
		this.versionType = versionType;
	}
	public String getVersionNum() {
		return versionNum;
	}
	public void setVersionNum(String versionNum) {
		this.versionNum = versionNum;
	}
	public Integer getDownloadNum() {
		return downloadNum;
	}
	public void setDownloadNum(Integer downloadNum) {
		this.downloadNum = downloadNum;
	}
	public Integer getMakeVideoNum() {
		return makeVideoNum;
	}
	public void setMakeVideoNum(Integer makeVideoNum) {
		this.makeVideoNum = makeVideoNum;
	}
	public Integer getPhotographNum() {
		return photographNum;
	}
	public void setPhotographNum(Integer photographNum) {
		this.photographNum = photographNum;
	}
	public Integer getVideoShareNum() {
		return videoShareNum;
	}
	public void setVideoShareNum(Integer videoShareNum) {
		this.videoShareNum = videoShareNum;
	}
	public Integer getPicShareNum() {
		return picShareNum;
	}
	public void setPicShareNum(Integer picShareNum) {
		this.picShareNum = picShareNum;
	}
	
}
