package com.lidx.back.statistics.entity.record;

import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelTarget;

import java.util.Map;

/**
 * Created by chen on 2017/2/7.
 */
@ExcelTarget("materialShare")
public class MaterialShareRecord {
    @Excel(name="素材的名称")
    private String uuid;
    @Excel(name="设备版本信息")
    private String versionType;
    @Excel(name="APP版本信息")
    private String versionNum;
    //private String channel;
    @Excel(name="总共数量")
    private int totalNum = 0;
    @Excel(name="微信渠道数量")
    private int weiXinNum;
    @Excel(name="QQ渠道数量")
    private int qqNum;
    @Excel(name="微博渠道数量")
    private int weiBoNum;
    @Excel(name="微信朋友圈渠道数量")
    private int pyqNum;//微信朋友圈

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getVersionType() {
        return versionType;
    }

    public void setVersionType(String versionType) {
        this.versionType = versionType;
    }

    public String getVersionNum() {
        return versionNum;
    }

    public void setVersionNum(String versionNum) {
        this.versionNum = versionNum;
    }

    public int getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(int totalNum) {
        this.totalNum = totalNum;
    }

    public int getWeiXinNum() {
        return weiXinNum;
    }

    public void setWeiXinNum(int weiXinNum) {
        this.weiXinNum = weiXinNum;
    }

    public int getQqNum() {
        return qqNum;
    }

    public void setQqNum(int qqNum) {
        this.qqNum = qqNum;
    }

    public int getWeiBoNum() {
        return weiBoNum;
    }

    public void setWeiBoNum(int weiBoNum) {
        this.weiBoNum = weiBoNum;
    }

    public int getPyqNum() {
        return pyqNum;
    }

    public void setPyqNum(int pyqNum) {
        this.pyqNum = pyqNum;
    }
}
