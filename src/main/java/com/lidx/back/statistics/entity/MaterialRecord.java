package com.lidx.back.statistics.entity;

import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.PooledObjectState;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Deque;

@Entity
@Table(name = "material_record")
@DynamicInsert
public class MaterialRecord {
    private Integer id;
    // 特定素材的唯一标示；同一素材的不同版本的该值应该相同。
    private String uuid;
    private String deviceId;
    private String versionType;
    private String versionNum;
    private String action;
    private Date recordTime;
    private String channel;
    //private String online;//对一条记录加入是否是线上的标识，app端传入 "y" or "n"

   /* public String getOnline() {
        return online;
    }

    public void setOnline(String online) {
        this.online = online;
    }*/

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getVersionType() {
        return versionType;
    }

    public void setVersionType(String versionType) {
        this.versionType = versionType;
    }

    public String getVersionNum() {
        return versionNum;
    }

    public void setVersionNum(String versionNum) {
        this.versionNum = versionNum;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Date getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(Date recordTime) {
        this.recordTime = recordTime;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

}
