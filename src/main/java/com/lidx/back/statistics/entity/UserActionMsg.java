package com.lidx.back.statistics.entity;

public class UserActionMsg {
	//private String sex;
	
	private Integer closeSoundNum = 0;
	private Integer openFlashLightNum = 0;
	private Integer switchCameraNum = 0;
	private Integer switchFilterNum = 0;
	//时间单位为秒
	/*private Long recordVideoTime = 0L;
	private Integer recordVideoCounts = 0;*/
	//用户录制视频的平均时长
	private double avgRecordVideoTime;
	
	private Integer linkShareNum = 0;
	private Integer saveVideoNum = 0;
	private Integer savePictureNum = 0;
	private Integer shareVideoNum= 0 ;
	//private Integer sharePictureNum = 0;
	
	private Integer maleNum = 0;
	private Integer femaleNum = 0;
	//private Integer maleFemalePercentage ;
	public Integer getCloseSoundNum() {
		return closeSoundNum;
	}
	public void setCloseSoundNum(Integer closeSoundNum) {
		this.closeSoundNum = closeSoundNum;
	}
	public Integer getOpenFlashLightNum() {
		return openFlashLightNum;
	}
	public void setOpenFlashLightNum(Integer openFlashLightNum) {
		this.openFlashLightNum = openFlashLightNum;
	}
	public Integer getSwitchCameraNum() {
		return switchCameraNum;
	}
	public void setSwitchCameraNum(Integer switchCameraNum) {
		this.switchCameraNum = switchCameraNum;
	}
	public Integer getSwitchFilterNum() {
		return switchFilterNum;
	}
	public void setSwitchFilterNum(Integer switchFilterNum) {
		this.switchFilterNum = switchFilterNum;
	}
	public double getAvgRecordVideoTime() {
		return avgRecordVideoTime;
	}
	public void setAvgRecordVideoTime(double avgRecordVideoTime) {
		this.avgRecordVideoTime = avgRecordVideoTime;
	}
	public Integer getLinkShareNum() {
		return linkShareNum;
	}
	public void setLinkShareNum(Integer linkShareNum) {
		this.linkShareNum = linkShareNum;
	}
	public Integer getSaveVideoNum() {
		return saveVideoNum;
	}
	public void setSaveVideoNum(Integer saveVideoNum) {
		this.saveVideoNum = saveVideoNum;
	}
	public Integer getSavePictureNum() {
		return savePictureNum;
	}
	public void setSavePictureNum(Integer savePictureNum) {
		this.savePictureNum = savePictureNum;
	}
	public Integer getShareVideoNum() {
		return shareVideoNum;
	}
	public void setShareVideoNum(Integer shareVideoNum) {
		this.shareVideoNum = shareVideoNum;
	}
	/*public Integer getSharePictureNum() {
		return sharePictureNum;
	}
	public void setSharePictureNum(Integer sharePictureNum) {
		this.sharePictureNum = sharePictureNum;
	}*/
	public Integer getMaleNum() {
		return maleNum;
	}
	public void setMaleNum(Integer maleNum) {
		this.maleNum = maleNum;
	}
	public Integer getFemaleNum() {
		return femaleNum;
	}
	public void setFemaleNum(Integer femaleNum) {
		this.femaleNum = femaleNum;
	}
	
}
