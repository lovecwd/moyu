package com.lidx.back.statistics.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name="dictionary")
@Cache(usage =  CacheConcurrencyStrategy.READ_ONLY, region="dictionary") 
public class Dictionary implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6377249991260043875L;

	@Id
	private Integer id;
	private String code;
	private String value;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
}
