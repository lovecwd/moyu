<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../common/header.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>CDN管理页面</title>
</head>
<body>

	<div class="all">
		<div class="main">
			<div class="iconDiv">
				<div class="imageDetail"><img src="${ctx }/resources/images/logo.png"></div>
			</div>
			<div class="ulContent">
				<jsp:include page="../common/menu.jsp"></jsp:include>
			</div>
		</div>
	<div class="body">
	<jsp:include page="../common/bodyHead.jsp"></jsp:include>
	<div class="tableDiv">
		<div>
			<div>
				<a class="btn btn-default" href="${ctx }/admin/cdn/updateDir">更新目录缓存</a>
				<a class="btn btn-default" href="${ctx }/admin/cdn/updateTasks">CDN更新记录</a>
			</div>
		</div>
		<table class="table table-borded">
			<tr class="trHead">
				<th>cdn类别</th>
				<th>域名状态</th>
				<th>加速域名名称</th>
				<th>加速域名修改时间</th>
				<th>加速域名创建时间</th>
			</tr>
		</table>
	</div>


	
</div>
</div>
<script type="text/javascript">
$(function(){
	var data = eval(${content });
	var PageData = data.Domains.PageData;
	$.each(PageData,function(index,value){
		var domain = value;
		var modifyDate = new Date(domain.GmtModified);
		var modifyDateString = modifyDate.toLocaleString();
		var createDate = new Date(domain.GmtCreated);
		var createDateString = createDate.toLocaleString();
		var $tr = "<tr><td>"+domain.CdnType+"</td><td>"+domain.DomainStatus+"</td><td>"+domain.DomainName+"</td>"+
				  "<td>"+modifyDateString+"</td><td>"+createDateString+"</td></tr>";
		$(".trHead").after($tr);
	});
});
</script>
</body>
</html>