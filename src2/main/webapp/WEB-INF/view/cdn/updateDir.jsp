<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../common/header.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>CDN更新目录缓存</title>
</head>
<body>

	<div class="all">
		<div class="main">
			<div class="iconDiv">
				<div class="imageDetail"><img src="${ctx }/resources/images/logo.png"></div>
			</div>
			<div class="ulContent">
				<jsp:include page="../common/menu.jsp"></jsp:include>
			</div>
		</div>
	<div class="body">
	<jsp:include page="../common/bodyHead.jsp"></jsp:include>
	<div class="formDiv">
		<form action="${ctx }/admin/cdn/updateDir" method="post" class="form-horizontal">
			<div class="form-group">
				<label for="firstname" class="col-sm-2 control-label">要刷新的域名</label>
				<div class="col-sm-4">
					<input type="text" class="form-control" name="domain" value="api.magicjoy.cn"/>
				</div>
			</div>
			<div class="form-group">
				<label for="firstname" class="col-sm-2 control-label">要刷新的文件路径</label>
				<div class="col-sm-4">
					<input type="text" class="form-control" name="filePath"/>
				</div>
			</div>
			<div class="form-group">
				<input id="btnSubmit" class="btn btn-primary" type="submit" value="刷 新"/>&nbsp;
				<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
			</div>
		</form>
	</div>


	
</div>
</div>
</body>
</html>