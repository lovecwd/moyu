<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../common/header.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>cdn更新记录</title>
</head>
<body>

	<div class="all">
		<div class="main">
			<div class="iconDiv">
				<div class="imageDetail"><img src="${ctx }/resources/images/logo.png"></div>
			</div>
			<div class="ulContent">
				<jsp:include page="../common/menu.jsp"></jsp:include>
			</div>
		</div>
	<div class="body">
	<jsp:include page="../common/bodyHead.jsp"></jsp:include>
	<div class="tableDiv">
		<input id="btnCancel" class="btn btn-default" type="button" value="返 回" onclick="history.go(-1)"/>
		<table class="table table-borded">
			<tr class="trHead">
				<th>创建时间</th>
				<th>更新的路径</th>
				<th>更新的状态</th>
				<th>任务ID</th>
			</tr>
			<%-- <tr>
				<td>${user.loginName }</td>
				<td>${user.name }</td>
				<td><a class="btn btn-default" href="${ctx }/admin/user/delete?id=${user.id}">删除用户</a></td>
			</tr> --%>
		</table>
	</div>


	
</div>
</div>
<script type="text/javascript">
$(function(){
	var data = eval(${content });
	var tasks = data.Tasks.CDNTask;
	$.each(tasks,function(index,value){
		var task = value;
		var date2 = new Date(task.CreationTime);
		var localeString = date2.toLocaleString();
		var $tr = "<tr><td>"+localeString+"</td><td>"+task.ObjectPath+"</td><td>"+task.Status+"</td>"+
				  "<td>"+task.TaskId+"</td></tr>";
		$(".trHead").after($tr);
	});
});
</script>
</body>
</html>