<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../common/header.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>用户添加</title>
</head>
<body>

	<div class="all">
		<div class="main">
			<div class="iconDiv">
				<div class="imageDetail"><img src="${ctx }/resources/images/logo.png"></div>
			</div>
			<div class="ulContent">
				<jsp:include page="../common/menu.jsp"></jsp:include>
			</div>
		</div>
	<div class="body">
	<jsp:include page="../common/bodyHead.jsp"></jsp:include>
	<div class="formDiv">
		<form:form id="inputForm" modelAttribute="user" action="${ctx}/admin/user/add" method="post" class="form-horizontal">
		<%-- <form:hidden path="id"/> --%>
		<div>${msg }</div>
		<div>添加用户</div>
		<div class="control-group">
			<label class="control-label" for="oldLoginName">登录名:</label>
			<div class="controls">
				<form:input path="loginName" htmlEscape="false" maxlength="50" class="required userName"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="newPassword">密码:</label>
			<div class="controls">
				<form:input path="password" type="password" htmlEscape="false" maxlength="50" class="required password"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="newPassword">真实姓名:</label>
			<div class="controls">
				<form:input path="name" htmlEscape="false" maxlength="50" />
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label" for="roleIdList">用户角色:</label>
			<div class="controls">
				<form:checkboxes path="roleIdList" items="${allRoles}" itemLabel="name" itemValue="id" htmlEscape="false" class="required"/>
			</div>
		</div>
		<div class="form-actions">
			<input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
	</div>
	
	<%-- <div class="tableDiv">
		<div>
			<div><a class="btn btn-default" href="${ctx }/admin/user/add">用户添加</a></div>
		</div>
		<c:if test="${! empty userList }">
		<table class="table table-borded">
			<tr>
				<th>登录名</th>
				<th>真实姓名</th>
			</tr>
			<c:forEach items="${userList }" var="user">
			<tr>
				<td>${user.loginName }</td>
				<td>${user.name }</td>
			</tr>
			</c:forEach>
		</table>
	</c:if>
	</div> --%>


	
	</div>
	</div>
</body>
</html>