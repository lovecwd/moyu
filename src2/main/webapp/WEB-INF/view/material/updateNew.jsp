<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>素材更新管理</title>
<script type="text/javascript" src="${ctx}/resources/js/jquery-3.1.0.min.js"></script>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/bootstrap.min.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/main.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/body.css"/>
<script type="text/javascript" src="${ctx }/resources/js/jquery-3.1.0.min.js"></script>
<style type="text/css">
*{margin:0;padding:0;}
/* .all{
	float:left;
	width:100%;
	background-color: #434d63;
}
.menu{
	float:left;
	width:326px;
	margin-left: 200px;
	margin-top:70px;
}
.body{
	float:left;
	width:84%;
	/* margin-left: 100px;
	margin-top:72px; */
	background-color: #cadbde;
} */
#updateForm{
	
}
 .formDiv{
 float:left;
	margin-left: 32px;
	margin-top:50px;
}
#submitBtn{
	margin-left: 28px;
}
.tableDiv{
	margin-left: 32px;
	margin-top: 22px;
}
table{
	background-color: #fff;
	border-radius:17px;
	table-layout: fixed;
}
td {
/* overflow: hidden;
white-space: nowrap;
text-overflow: ellipsis; */
 /* text-overflow: ellipsis; /* for IE */  
    /* -moz-text-overflow: ellipsis; 
    overflow: hidden;  
    white-space: nowrap;   */
    word-break:break-all; word-wrap:break-word;
    color:#434d63;
}
/* td:HOVER {
	overflow: visible;
	width: auto;
} */
.headTr > td{
	color:#b1bcc0;
}
input{
	border-radius:10px;
	height:35px;
}
.lastUpdateMsg{
	float:left;
	margin-left: 32px;
	margin-top: 22px;
}
</style>
</head>
<body>
<%-- <jsp:include page="../common/head.jsp"></jsp:include> --%>
	<div class="all">
		<div class="main">
			<div class="iconDiv">
				<div class="imageDetail"><img src="${ctx }/resources/images/logo.png"></div>
			</div>
			<div class="ulContent">
				<jsp:include page="../common/menu.jsp"></jsp:include>
			</div>
		</div>
	<div class="body">
	<jsp:include page="../common/bodyHead.jsp"></jsp:include>
		<div class="formDiv">
		<form class="form-inline" id="updateForm" action="${ctx }/admin/material/update" method="POST">
				<label>版本类型:</label>&nbsp;<input id="versionType" type="text" style="width: 400px" name="versionType" value="${info.versionType }"/> 
				<label>版本号:</label>&nbsp;<input	 id="versionNumber" type="text" name="versionNumber"  value="${info.versionNum }"/>
				<div class="form-group">
		            <input type="text" class="form-control" placeholder="请输入你的用户名">
		        </div>
		        <div class="form-group">
		            <input type="text" class="form-control" placeholder="请输入你的密码">
		        </div>
				<input id="submitBtn" type="button" class="btn btn-default"	value="UPDATE" />
		</form>
		</div>
		
		<div class="lastUpdateMsg">
			<c:if test="${! empty info }">
			上次更新版本：${info.versionType }&nbsp;版本号：${info.versionNum }<br>
			
			</c:if>
			<c:if test="${! empty names }">
				更新的相关素材名称:
				<c:forEach items="${names }" var="mName" >
				${mName }&nbsp;&nbsp;
				</c:forEach>
			</c:if>
			<br>
			更新数量：${number }
		</div>
		
		<div class="tableDiv">
		
		<c:if test="${! empty materials }">
			<table class="table table-borded">
				<tr class="headTr">
					<td>素材uuid</td>
					<td>推荐顺序</td>
					<td>类别顺序</td>
					<td>APP版本类型</td>
					<td>APP版本号</td>
					<td>是否是推荐素材</td>
					<td>图标文件名</td>
					<td>图标文件MD5</td>
					<td>图标文件下载路径</td>
					<td>素材文件名</td>
					<td>素材文件MD5</td>
					<td>素材文件下载路径</td>
					<td>素材文件的类别</td>
					<td>素材的性别取向</td>
				</tr>
			<c:forEach items="${materials }" var="po">
				<tr>
					<td>${po.uuid}</td>
					<td>${po.recommendOrder}</td>
					<td>${po.categoryOrder}</td>
					<td>${po.versionType}</td>
					<td>${po.versionNumber}</td>
					<td>${po.isRecommend}</td>
					<td>${po.iconName}</td>
					<td>${po.iconMD5}</td>
					<td>${po.iconDownloadUrl}</td>
					<td>${po.materialName}</td>
					<td>${po.materialMD5}</td>
					<td>${po.materialDownloadUrl}</td>
					<td>${po.category}</td>
					<td>${po.gender}</td>
				</tr>
			</c:forEach>
			</table>
		</c:if>
		
	</div>
	
	
	</div>
	</div>
</body>
<script type="text/javascript">
$(function(){
	$("#submitBtn").click(function(){
		var versionType = $("#versionType").val();
		var versionNumber = $("#versionNumber").val();
		if(versionType == ""){
			alert("版本类型不能为空！");
			return false;
		}
		if(versionNumber == ""){
			alert("版本号不能为空");
			return false;
		}
		$("#updateForm").submit();
	});
});
</script>
</html>