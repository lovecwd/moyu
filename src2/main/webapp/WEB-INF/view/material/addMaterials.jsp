<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>批量添加素材</title>
	<script type="text/javascript" src="${ctx}/resources/js/jquery-3.1.0.min.js"></script>
	<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/bootstrap.min.css"/>
	<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/main.css"/>
	<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/body.css"/>
	<script type="text/javascript" src="${ctx }/resources/js/jquery-3.1.0.min.js"></script>
	<style type="text/css">
		*{margin:0;padding:0;}
		.formDiv{
			float:left;
			margin-left: 32px;
			margin-top:50px;
		}
		#submitBtn{
			margin-left: 28px;
		}
		.tableDiv{
			margin-left: 32px;
			margin-top: 22px;
		}
		table{
			background-color: #fff;
			border-radius:17px;
			table-layout: fixed;
		}
		td {
			word-break:break-all; word-wrap:break-word;
			color:#434d63;
		}
		/* td:HOVER {
            overflow: visible;
            width: auto;
        } */
		.headTr > td{
			color:#b1bcc0;
		}
		input{
			border-radius:10px;
			height:28px;
		}
		select{
			height:28px;
			border-radius:10px;
			width: 100%;
		}
		.lastUpdateMsg{
			float:left;
			margin-left: 32px;
			margin-top: 22px;
		}
	</style>
</head>
<body>
<%-- <jsp:include page="../common/head.jsp"></jsp:include> --%>
<div class="all">
	<div class="main">
		<div class="iconDiv">
			<div class="imageDetail"><img src="${ctx }/resources/images/logo.png"></div>
		</div>
		<div class="ulContent">
			<jsp:include page="../common/menu.jsp"></jsp:include>
		</div>
	</div>
	<div class="body">
		<jsp:include page="../common/bodyHead.jsp"></jsp:include>
		<div style="padding-left: 37px;">
			<h1>批量添加素材</h1>
			<div class=""><button class="btn btn-default" onclick="history.go(-1);">返回</button></div>
		</div>
		<div class="formDiv">
			<form id="hotUpForm" enctype="multipart/form-data" action="${ctx}/admin/material/addMaterials" class="form-horizontal" role="form" method="post">
				<table class="table table-borded">
					<thead>
					<tr>
						<th>素材uuid</th>
						<th>APP版本类型</th>
						<%--<th>APP版本号</th>--%>
						<th>素材文件的类别</th>
						<th>类别顺序</th>
						<th>icon文件</th>
						<th>素材文件</th>
						<th>素材文件是否有声音</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td><input name="uuid" type="text"/></td>
						<td>
							<select name="versionType" >
								<option  value="ios">IOS系统</option>
								<option value="android">安卓系统</option>
							</select>
						</td>
						<%--<td>
							<select name="versionNumber">
								<option  value="1.1.0">1.1.0版本</option>
								<option  value="1.0.1">1.0.1版本</option>
							</select>
						</td>--%>
						<td>
							<select name="categoryId">
									<c:forEach items="${categoryList }" var="category">
										<option value="${category.categoryId }">${category.categoryName }</option>
									</c:forEach>
							</select>
						</td>
						<td>
							<input name="categoryOrder" type="text"/>
						</td>
						<td><input name="iconFile" type="file"/></td>
						<td><input name="materialFile" type="file"/></td>
						<td>
							<select name="audio">
								<option value="true">有声音</option>
								<option value="false">没有声音</option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="7"></td>
						<td><button id="addHotMaterial" class="btn btn-default">添加一个热更新素材</button></td>
					</tr>
					</tbody>
				</table>

				<div class="form-group">
					<div class="col-sm-10">
						<button id="updateBtn" type="submit" class="btn btn-default">批量添加</button>
					</div>
				</div>
			</form>
		</div>




	</div>
</div>
</body>
<script type="text/javascript">
    $(function(){
		$("#addHotMaterial").click(function(){
			var row = "<tr><td><input name='uuid' type='text'/></td>"
				+"<td><select name='versionType' ><option  value='ios'>IOS系统</option><option value='android'>安卓系统</option></select></td>"
					/*+"<td><select name='versionNumber'><option  value='1.1.0'>1.1.0版本</option><option  value='1.0.1'>1.0.1版本</option></select></td>"*/
				+"<td><select name='categoryId'>"
				<c:forEach items="${categoryList}" var="category">
				+"<option value='${category.categoryId}'>${category.categoryName}</option>"
				</c:forEach>
				+"</select></td>"
				+"<td><input name='categoryOrder' type='text'/></td>"
				+"<td><input name='iconFile' type='file'/></td>"
				+"<td><input name='materialFile' type='file'/></td>"
				+"<td><select name='audio'>"
				+"<option value='true'>有声音</option>"
				+"<option value='false'>没有声音</option>"
				+"</select></td>"
				+"</tr>";

			$(row).insertBefore(this.closest("tr"));
			return false;
		});
	})(jQuery);
</script>
</html>