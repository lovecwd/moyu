<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<jsp:include page="../common/header.jsp"></jsp:include>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<style type="text/css">
*{margin:0;padding:0;}
.all{
	float:left;
	width:100%;
}
.menu{
	float:left;
	width:326px;
	margin-left: 200px;
	margin-top:100px;
}
.body{
	float:left;
	width:800px;
	margin-left: 100px;
	margin-top:72px;
}
</style>
</head>
<body>
<div class="all">
		<div class="menu">
			<jsp:include page="../common/menu.jsp"></jsp:include>
		</div>
	<div class="body">
	<jsp:include page="../common/bodyHead.jsp"></jsp:include>
<form action="${ctx }/material/addCategory" method="post">
类别名称：<input type="text" name="categoryName"/>
类别顺序：<input type="text" name="order"/>
类别对应的性别：<select name="gender">
	<option value="0">女</option>
	<option value="1">男</option>
</select>
<input type="submit" value="保存"/>
</form>
</div>
</div>
</body>
</html>