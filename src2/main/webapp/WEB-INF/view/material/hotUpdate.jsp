<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>素材热更新</title>
<script type="text/javascript" src="${ctx}/resources/js/jquery-3.1.0.min.js"></script>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/bootstrap.min.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/main.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/body.css"/>
<script type="text/javascript" src="${ctx }/resources/js/jquery-3.1.0.min.js"></script>
<style type="text/css">
*{margin:0;padding:0;}
#updateForm{
	
}
 .formDiv{
 float:left;
	margin-left: 32px;
	margin-top:50px;
}
#submitBtn{
	margin-left: 28px;
}
.tableDiv{
	margin-left: 32px;
	margin-top: 22px;
}
table{
	background-color: #fff;
	border-radius:17px;
	table-layout: fixed;
}
td {
    word-break:break-all; word-wrap:break-word;
    color:#434d63;
}
/* td:HOVER {
	overflow: visible;
	width: auto;
} */
.headTr > td{
	color:#b1bcc0;
}
input{
	border-radius:10px;
	height:28px;
}
select{
	height:28px;
	border-radius:10px;
	width: 100%;
}
.lastUpdateMsg{
	float:left;
	margin-left: 32px;
	margin-top: 22px;
}
</style>
</head>
<body>
<%-- <jsp:include page="../common/head.jsp"></jsp:include> --%>
	<div class="all">
		<div class="main">
			<div class="iconDiv">
				<div class="imageDetail"><img src="${ctx }/resources/images/logo.png"></div>
			</div>
			<div class="ulContent">
				<jsp:include page="../common/menu.jsp"></jsp:include>
			</div>
		</div>
	<div class="body">
	<jsp:include page="../common/bodyHead.jsp"></jsp:include>
		<div style="padding-left: 37px;">
			<h1>热更新</h1>
			<div class=""><button class="btn btn-default" onclick="history.go(-1);">返回</button></div>
		</div>
		<div class="formDiv">
		<form id="hotUpForm" class="form-horizontal" role="form" method="post" onsubmit="return false;">
		  <table class="table table-borded">
		  	<thead>
		  		<tr>
		  			<th>素材uuid</th>
		  			<th>APP版本类型</th>
		  			<th>APP版本号</th>
		  			<th>是否是推荐素材</th>
		  			<th>推荐顺序</th>
		  			<th>素材文件的类别</th>
		  			<th>类别顺序</th>
		  			<th>素材的性别取向</th>
		  		</tr>
		  	</thead>
		  	<tbody>
		  		<tr>
		  			<td><input name="uuid" type="text"/></td>
		  			<td>
		  				<select name="versionType" >
		  					<option  value="ios">IOS系统</option>
							<option value="android">安卓系统</option>
		  				</select>
	  				</td>
		  			<td>
		  				<select name="versionNumber">
		  					<option  value="1.1.0">1.1.0版本</option>
							<option  value="1.0.1">1.0.1版本</option>
		  				</select>
	  				</td>
		  			<td>
		  				<select name="isRecommend" >
					      	<option value="1">推荐</</option>
					      	<option value="-1">不推荐</option>
				        </select>
		  			</td>
		  			<td><input name="recommendOrder" type="text"/></td>
		  			<td>
		  				<!-- <input name="category" type="text"/> -->
		  				<select name="category">
		  					<c:forEach items="${categoryNames }" var="categoryName">
		  						<option value="${categoryName }">${categoryName }</option>
		  					</c:forEach>
		  				</select>
	  				</td>
		  			<td><input name="categoryOrder" type="text"/></td>
		  			<td> 
		  				<select name="gender" >
		  					<option value="-1">男女都要更新</</option>
					      	<option value="1">男</option>
					      	<option value="0">女</option>
				        </select>
		      		</td>
		  		</tr>
		  		<tr>
		  			<td colspan="7"></td>
		  			<td><button id="addHotMaterial" class="btn btn-default">添加一个热更新素材</button></td>
		  		</tr>
		  	</tbody>
		  </table>
			  
		  <div class="form-group">
		    <div class="col-sm-10">
		      <button id="updateBtn" class="btn btn-default">更新</button>
		    </div>
		  </div>
		</form>
		</div>
		
		
	
	
	</div>
	</div>
</body>
<script type="text/javascript">
$(function(){
	
	 //将表单序列化成json格式的数据(但不适用于含有控件的表单，例如复选框、多选的select)
    (function($){
        $.fn.serializeJson = function(){
            var jsonData1 = {};
            var serializeArray = this.serializeArray();
            // 先转换成{"id": ["12","14"], "name": ["aaa","bbb"], "pwd":["pwd1","pwd2"]}这种形式
            $(serializeArray).each(function () {
                if (jsonData1[this.name]) {
                    if ($.isArray(jsonData1[this.name])) {
                        jsonData1[this.name].push(this.value);
                    } else {
                        jsonData1[this.name] = [jsonData1[this.name], this.value];
                    }
                } else {
                    jsonData1[this.name] = this.value;
                }
            });
            // 再转成[{"id": "12", "name": "aaa", "pwd":"pwd1"},{"id": "14", "name": "bb", "pwd":"pwd2"}]的形式
            var vCount = 0;
            // 计算json内部的数组最大长度
            for(var item in jsonData1){
                var tmp = $.isArray(jsonData1[item]) ? jsonData1[item].length : 1;
                vCount = (tmp > vCount) ? tmp : vCount;
            }

            if(vCount > 1) {
                var jsonData2 = new Array();
                for(var i = 0; i < vCount; i++){
                    var jsonObj = {};
                    for(var item in jsonData1) {
                        jsonObj[item] = jsonData1[item][i];
                    }
                    jsonData2.push(jsonObj);
                }
                return JSON.stringify(jsonData2);
            }else{
                return "[" + JSON.stringify(jsonData1) + "]";
            }
        };
        
       
        
        $("#updateBtn").click(function(){
        	submitUserList_4();
        });
        $("#addHotMaterial").click(function(){
        	var row = "<tr><td><input name='uuid' type='text'/></td>"
  			+"<td><select name='versionType' ><option  value='ios'>IOS系统</option><option value='android'>安卓系统</option></select></td>"
			+"<td><select name='versionNumber'><option  value='1.1.0'>1.1.0版本</option><option  value='1.0.1'>1.0.1版本</option></select></td>"
  			+"<td><select name='isRecommend' ><option value='1'>推荐</</option><option value='-1'>不推荐</option>"
		    +"</select></td>" 
  			+"<td><input name='recommendOrder' type='text'/></td>"
  			
  			+"<td><select name='category'>"
				<c:forEach items="${categoryNames }" var="categoryName">
					+"<option value='${categoryName }'>${categoryName }</option>"
				</c:forEach>
			+"</select></td>"
  			
  			/* +"<td><input name='category' type='text'/></td>" */
  			+"<td><input name='categoryOrder' type='text'/></td>"
  			+"<td><select name='gender' ><option value='-1'>男女都要更新</</option><option value='1'>男</option><option value='0'>女</option></select></td></tr>";
  		
        	$(row).insertBefore(this.closest("tr"));
        });
    })(jQuery);

    function submitUserList_4() {
        var jsonStr = $("#hotUpForm").serializeJson();
        //console.log("jsonStr:\r\n" + jsonStr);
        //alert(jsonStr);
         $.ajax({
            url: "${ctx}/admin/material/hotUpdate",
            type: "POST",
            contentType : 'application/json', //设置请求头信息
            dataType:"json",
            data: jsonStr,
            async: true,
            success: function(data){
                alert(data.msg);
            },
            error: function(res){
                alert(res);
            },
            complete:function(){location.href ="${ctx}/admin/material/listMaterials";}//location.href实现客户端页面的跳转}//跳转页面
        }); 
    }
});
</script>
</html>