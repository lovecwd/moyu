<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.util.*"%>
<%@ page import="com.lidx.back.statistics.entity.*"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>最新的素材列表管理</title>
    <link type="text/css" rel="stylesheet" href="${ctx }/resources/css/WdatePicker.css"/>
    <link type="text/css" rel="stylesheet" href="${ctx }/resources/css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="${ctx }/resources/css/datepicker.css"/>
    <script type="text/javascript" src="${ctx }/resources/js/WdatePicker.js"></script>
    <link type="text/css" rel="stylesheet" href="${ctx }/resources/css/main.css"/>
    <link type="text/css" rel="stylesheet" href="${ctx }/resources/css/body.css"/>
    <link rel="shortcut icon" href="${ctx }/resources/images/favicon.png">
    <script type="text/javascript" src="${ctx }/resources/js/jquery-3.1.0.min.js"></script>
    <style type="text/css">
        *{margin:0;padding:0;}
        .all{
            float:left;
            width:100%;
        }

        /* .menu{
            float:left;
            width:326px;
            margin-left: 200px;
            margin-top:70px;
        } */
        /* .body{
            float:left;
            width:800px;
            margin-left: 100px;
            margin-top:72px;
        } */
    </style>
</head>
<body>
<%-- <jsp:include page="../common/head.jsp"></jsp:include> --%>
<div class="all">
    <%-- <div class="menu">
        <jsp:include page="../common/menu.jsp"></jsp:include>
    </div> --%>
    <div class="main">
        <div class="iconDiv">
            <div class="imageDetail"><img src="${ctx }/resources/images/logo.png"></div>
        </div>
        <div class="ulContent">
            <jsp:include page="../common/menu.jsp"></jsp:include>
        </div>
    </div>
    <div class="body">
        <%-- <div class="bodyHead">
            <div class="logoutDiv pull-right">
                <a class="btn btn-primary" href="${ctx }/logout">用户登出</a>
            </div>
        </div> --%>
        <jsp:include page="../common/bodyHead.jsp"></jsp:include>
            <div style="padding-left: 33px;">
                <a class="btn btn-default" href="${ctx }/admin/material/listMaterials">素材列表管理</a>
                <a class="btn btn-default" href="${ctx }/admin/material/latestMaterials">最新素材列表</a>
            </div>
            <div class="formDiv">

                <form id="materialForm" action="${ctx }/admin/material/initLatestMaterials" class="form-horizontal" method="post">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">所有类别前面素材的个数</label>
                        <div class="col-sm-2">
                            <input name="size" class="form-control" value="10"/>
                        </div>
                        <div class="col-sm-4">
                            <input type="submit" value="初始化最新素材" onclick="return confirm('确定要初始化最新素材吗？');" class="btn btn-default"/>
                        </div>
                    </div>
                </form>
                <form>

                    <div style="padding-left: 33px;">

                    </div>
                </form>
            </div>
        <div class="tableDiv">
            <c:if test="${ ! empty materialLatests  }">
                <table class="table table-borded">
                    <tr class="headTr">
                        <td>序号</td>
                        <td>最新素材id</td>
                        <td>最新素材文件名</td>
                        <td>最新素材文顺序</td>
                        <td>最新素材类别</td>
                        <td>最新素材创建时间</td>
                        <td>操作</td>
                    </tr>
                    <c:forEach items="${materialLatests }" var="ml" varStatus="status">
                        <tr>
                            <td>${status.count }</td>
                            <td>${ml.materialId}</td>
                            <td>${ml.materialName}</td>
                            <td>${ml.categoryOrder}</td>
                            <td>
                                <c:forEach items="${categoryList }" var="category">
                                        <c:if test="${ml.categoryId == category.categoryId }">
                                            ${category.categoryName }
                                        </c:if>
                                </c:forEach>
                            </td>
                            <td>${ml.createTime}</td>
                            <td><a class="btn btn-primary" href="${ctx}/admin/material/deleteLatestMaterial?id=${ml.id}">删除</a></td>
                        </tr>
                    </c:forEach>
                </table>
            </c:if>
        </div>
    </div>
    <script type="text/javascript">
        $(function(){
            $(".main").css("height",$(document).height()+"px");

        });
    </script>
</body>
</html>