<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<jsp:include page="../common/header.jsp"></jsp:include>
<link rel="shortcut icon" href="${ctx }/resources/images/favicon.png">
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/WdatePicker.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/bootstrap.min.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/datepicker.css"/>
<script type="text/javascript" src="${ctx }/resources/js/WdatePicker.js"></script>
<link rel="stylesheet" type="text/css" href="${ctx }/resources/css/layui.css" media="all">
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/main.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/body.css"/>
<script type="text/javascript" src="${ctx }/resources/js/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="${ctx }/resources/js/bootstrap.min.js"></script>
<style type="text/css">
*{margin:0;padding:0;}
.all{
	float:left;
	width:100%;
}
/* .menu{
	float:left;
	width:326px;
	margin-left: 200px;
	margin-top:70px;
}
.body{
	float:left;
	width:800px;
	margin-left: 100px;
	margin-top:72px;
} */
.pageSizeSelect{
	width:100px;
	height:30px;
	border:1px solid #ccc;
	border-radius:4px;
}
.pageDiv{
	float:right;
}
.tableBottom{
	float:left;
	width:100%;
}
.table-desc{
margin:20px 0 0 0;
float:left;
width:50%;
}
.pagination > li{
	float:left;
	margin:0 1px;
	border:1px solid #ddd;
}
</style>
</head>
<body>
<%-- <jsp:include page="../common/head.jsp"></jsp:include> --%>
	<div class="all">
		<div class="main">
			<div class="iconDiv">
				<div class="imageDetail"><img src="${ctx }/resources/images/logo.png"></div>
			</div>
			<div class="ulContent">
				<jsp:include page="../common/menu.jsp"></jsp:include>
			</div>
		</div>
	<div class="body">
	<jsp:include page="../common/bodyHead.jsp"></jsp:include>
	<%-- <h1>用户行为数据</h1>
	<form action="${ctx }/admin/client/list" method="post">
	<div class="form-group">
		 <label for="firstname" class="col-sm-2 control-label">用户行为</label>
		  <div class="col-sm-4">
		<select class="form-control" name="clientRecords">
			<option <c:if test="${action == 'closeSound' }">selected</c:if> value="closeSound">用户关闭声音的次数</option>
			<option <c:if test="${action == 'openFlashLight' }">selected</c:if> value="openFlashLight">用户开启闪光灯的次数</option>
			<option <c:if test="${action == 'switchCamera' }">selected</c:if> value="switchCamera">用户切换摄像头的次数</option>
			<option <c:if test="${action == 'switchFilter' }">selected</c:if> value="switchFilter">用户使用滤镜快速切换的次数</option>
			<option <c:if test="${action == 'recordVideo' }">selected</c:if> value="recordVideo">用户录制视频的平均时长</option>
			<option <c:if test="${action == 'linkShare' }">selected</c:if> value="linkShare">用户分享推荐链接数统计</option>
			<option <c:if test="${action == 'saveVideo' }">selected</c:if> value="saveVideo">用户保存视频到本地的次数</option>
			<option <c:if test="${action == 'savePicture' }">selected</c:if> value="savePicture">用户保存照片到本地的次数</option>
		</select>
		</div>
		<label for="firstname" class="col-sm-2 control-label">设备版本类型</label>
		<div class="col-sm-4">
		<select class="form-control" name="versionType">
						<option <c:if test="${versionType == 'ios' }">selected</c:if> value="ios">IOS系统</option>
						<option <c:if test="${versionType == 'android' }">selected</c:if> value="android">安卓系统</option>
		</select>
		</div>
	</div>
	<div class="form-group">
		从 <input name="from" class="Wdate" type="text" value="${from }" onClick="WdatePicker()">
		到<input name="to" class="Wdate" type="text" value="${to }" onClick="WdatePicker()">
		<input type="submit" class="btn btn-default" value="查询"/>
	</div>
	</form> --%>
	<div class="formDiv">
	<div><button id="exportBtn" class="btn btn-default">导出当前页面数据</button></div>
	<form id="feedbackForm" action="${ctx }/admin/feedback/list" method="post">
		<!-- <input type="submit" class="btn btn-default" value="查询"/> -->
	
	
	<c:if test="${! empty pageInfo }">
	<!-- <div class="tableDiv"> -->
	<table class="table table-bordered">
			<tr>
				<%--<td>设备类型</td>
				<td>版本号</td>--%>
				<td>反馈内容</td>
				<%--<td>反馈图片</td>
				<td>qq</td>
				<td>手机号码</td>--%>
				<td>反馈时间</td>
			</tr>
		
		<c:forEach items="${pageInfo.list }" var="feedback">
		<tr>
			<%--<td>${feedback.versionType }</td>
			<td>${feedback.versionNum }</td>--%>
			<td>${feedback.content }</td>
			<%--<td>
				<c:set value="${ fn:split(feedback.imageNames, ',') }" var="imageNames"></c:set>
				<c:forEach items="${ imageNames }" var="imageName">
					<img  width="200px" alt="" src="${prefixUrl}${imageName}">&nbsp;
				</c:forEach>
			<td>${feedback.qq }</td>
			<td>${feedback.phone }</td>--%>
			<td>${feedback.feedbackTime }</td>
		</tr>
		</c:forEach>	
	</table>
	<!-- </div> -->
	<div class="tableBottom">
		<div class="table-desc form-group">
		<label class="control-label">每页</label>
		<select class="pageSizeSelect" name="pageSize">
		<option <c:if test="${pageSize == '10' }">selected</c:if> value="10">10条</option>
		<option <c:if test="${pageSize == '20' }">selected</c:if> value="20">20条</option>
		<option <c:if test="${pageSize == '50' }">selected</c:if> value="50">50条</option>
	   </select> 
	   	<label class="control-label">条记录 第${pageInfo.pageNo }页 共${pageInfo.totalPage }页</label>
	   </div>
	   <div class="pageDiv">
		<ul class="pagination pagination-sm">
		    <c:forEach begin="1" end="${pageInfo.totalPage }"  var="i">
		    	<li><a class="hrefClass" href="#">${i }</a></li>
		    </c:forEach>
		</ul>
		</div>
	</div>
		<input id="pageNo" name="pageNo" type="hidden" value="${pageInfo.pageNo}"/>
		<%-- <input id="pageSize" name="pageSize" type="hidden" value="${pageInfo.pageSize}"/> --%>
	</form>
	</div>
</c:if>

	
</div>
</div>
<script type="text/javascript">
$(function(){
	$(".main").css("height",$(document).height()+"px");
	$(".hrefClass").click(function(){
		var pageNo = $(this).text();
		$("#pageNo").val(pageNo);
		$("#feedbackForm").attr("action","${ctx }/admin/feedback/list");
		$("#feedbackForm").submit();
	});
	
	$("#exportBtn").click(function(){
		$("#feedbackForm").attr("action","${ctx }/admin/feedback/exportFeedbackExcel");
		$("#feedbackForm").submit();
	});
});
</script>
</body>
</html>