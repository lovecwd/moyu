<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>日志管理</title>
<link rel="shortcut icon" href="${ctx }/resources/images/favicon.png">
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/WdatePicker.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/bootstrap.min.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/datepicker.css"/>
<script type="text/javascript" src="${ctx }/resources/js/WdatePicker.js"></script>
<link rel="stylesheet" type="text/css" href="${ctx }/resources/css/layui.css" media="all">
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/main.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/body.css"/>
<script type="text/javascript" src="${ctx }/resources/js/jquery-3.1.0.min.js"></script>
<style type="text/css">
*{margin:0;padding:0;}
.all{
	float:left;
	width:100%;
}
.table{
width:100%;}
/* .menu{
	float:left;
	width:326px;
	margin-left: 200px;
	margin-top:70px;
} */
/* .body{
	float:left;
	width:800px;
	margin-left: 100px;
	margin-top:72px;
} */
.pageSizeSelect{
	width:100px;
	height:30px;
	border:1px solid #ccc;
	border-radius:4px;
}
.pageDiv{
	float:right;
	width: 100%;
}
.tableBottom{
	float:left;
	width:100%;
}
.table-desc{
margin:20px 0 0 0;
float:left;
width:50%;
}
.pagination > li{
	float:left;
	margin:0 1px;
	border:1px solid #ddd;
}
</style>
</head>
<body>
<%-- <jsp:include page="../common/head.jsp"></jsp:include> --%>
<div class="all">
		<%-- <div class="menu">
			<jsp:include page="../common/menu.jsp"></jsp:include>
		</div> --%>
		<div class="main">
			<div class="iconDiv">
				<div class="imageDetail"><img src="${ctx }/resources/images/logo.png"></div>
			</div>
			<div class="ulContent">
				<jsp:include page="../common/menu.jsp"></jsp:include>
			</div>
		</div>
	<div class="body">
	<%-- <div class="bodyHead">
		<div class="logoutDiv pull-right">
			<a class="btn btn-primary" href="${ctx }/logout">用户登出</a>
		</div>
	</div> --%>
	<jsp:include page="../common/bodyHead.jsp"></jsp:include>
	<div class="formDiv">
		<form id="materialForm" action="${ctx }/admin/log/list" class="form-horizontal" method="post">
			<div class="form-group">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
				从 <input name="from" class="Wdate " type="text" value="${from }" onClick="WdatePicker()">
				</div>
				<div class="col-sm-2">
				到<input name="to" class="Wdate" type="text" value="${to }" onClick="WdatePicker()">
				</div>
				<input id="pageNo" name="pageNo" type="hidden" value="${pageInfo.pageNo}"/>
				<input id="pageSize" name="pageSize" type="hidden" value="${pageInfo.pageSize}"/>
				
				<!-- <div class="col-sm-2">
				<input id="exportBtn" type="button" class="btn btn-default" value="导出"/>
				</div> -->
			</div>
			<div class="form-group">
				<div class="col-sm-5">
				</div>
				<div class="col-sm-5">
					<input id="queryBtn" type="button" class="btn btn-default" value="查询"/>
				</div>
			</div>
		</form>
	</div>


	<div class="tableDiv">
	<c:if test="${! empty pageInfo }">
	<table class="table table-bordered">
			<tr>
				<td>日志类型（1：接入日志；2：错误日志）</td>
				<td>请求url</td>
				<td>请求方法</td>
				<td>请求参数</td>
				<td>异常</td>
				<td>操作用户</td>
				<td>userAgent</td>
				<td>log生成时间</td>
				<td>操作用户的IP地址</td>
			</tr>
		
		<c:forEach items="${pageInfo.list }" var="log">
		<tr>
			<td>${log.type }</td>
			<td>${log.requestUri }</td>
			<td>${log.method }</td>
			<td>${log.params }</td>
			<td>${log.exception }</td>
			<td>${log.operateUserName }</td>
			<td>${log.userAgent }</td>
			<td>${log.createDate }</td>
			<td>${log.remoteAddr }</td>
		</tr>
		</c:forEach>	
	</table>
		<%--<  page="../common/pageInfo.jsp"></>--%>
		<%@include file="../common/pageInfo.jsp"%>
	<%--<div class="tableBottom">
		<div class="table-desc form-group">
		<label class="control-label">每页</label>
		<select class="pageSizeSelect" name="pageSize">
		<option <c:if test="${pageInfo.pageSize == '10' }">selected</c:if> value="10">10条</option>
		<option <c:if test="${pageInfo.pageSize == '20' }">selected</c:if> value="20">20条</option>
		<option <c:if test="${pageInfo.pageSize == '50' }">selected</c:if> value="50">50条</option>
	   </select> 
	   	<label class="control-label">条记录 第${pageInfo.pageNo }页 共${pageInfo.totalPage }页 总共${pageInfo.allRow }条记录</label>
	   </div>
	   <div id="pageDiv" class="pageDiv">
		&lt;%&ndash; <ul class="pagination pagination-sm">
		    <c:forEach begin="1" end="${pageInfo.totalPage }"  var="i">
		    	<li><a class="hrefClass" href="#">${i }</a></li>
		    </c:forEach>
		</ul> &ndash;%&gt;
		
		</div>
	</div>--%>
	</c:if>
	
	</div>

</div>
</div>
<script type="text/javascript" src="${ctx }/resources/js/layui.all.js"></script>
<script type="text/javascript">
	layui.use(['layer','laypage'], function(){
		var laypage = layui.laypage,layer = layui.layer;

		//分页
		laypage({
			cont: 'pageDiv' //分页容器的id
			,pages: ${pageInfo.totalPage } //总页数
			,curr: ${pageInfo.pageNo }
			,skin: '#5FB878' //自定义选中色值
			,skip: true //开启跳页
			,jump: function(obj, first){
				if(!first){
					layer.msg('第'+ obj.curr +'页');
					var pageNo = obj.curr;
					var pageSize= $(".pageSizeSelect").find("option:selected").val();
					$("#pageNo").val(pageNo);
					$("#pageSize").val(pageSize);
					 $("#materialForm").attr("action","${ctx }/admin/log/list");
					 $("#materialForm").submit();
				}
			}
		});
	});
</script>

<script type="text/javascript">
$(function(){
	$(".main").css("height",$(document).height()+"px");
	$("#exportBtn").click(function(){
		$("#materialForm").attr("action","${ctx }/admin/material/exportExcel");
		$("#materialForm").submit();
	});
	
	$(".hrefClass").click(function(){
		var pageNo = $(this).text();
		var pageSize= $(".pageSizeSelect").find("option:selected").val();
		$("#pageNo").val(pageNo);
		$("#pageSize").val(pageSize);
		$("#materialForm").attr("action","${ctx }/admin/log/list");
		$("#materialForm").submit();
	});
	
	$("#queryBtn").click(function(){
		$("#materialForm").attr("action","${ctx }/admin/log/list");
		$("#materialForm").submit();
	});
});
</script>


</body>
</html>