<%@ page import="java.net.InetAddress" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	String ppp = request.getServerName() + ":" + request.getServerPort()
			+ path + "/";


%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>团队交流室</title>
<jsp:include page="../common/header.jsp"></jsp:include>
<style type="text/css">
</style>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/body.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/main.css"/>
	<style type="text/css">
		.chatMain{
			padding: 10rem 0 0 5rem;
		}
		div#onlineMsg{
			height: 80%;
			overflow-y:scroll;
			border:1px solid #f5f5f5;
			line-height:2.5rem;
		}
		.onlineMsgPanel{
			height: 15rem;
		}
	</style>
</head>
<body>
	<div class="all">
		<div class="main">
			<div class="iconDiv">
				<div class="imageDetail"><img src="${ctx }/resources/images/logo.png"></div>
			</div>
			<div class="ulContent">
				<jsp:include page="../common/menu.jsp"></jsp:include>
			</div>
		</div>
	<div class="body">
	<jsp:include page="../common/bodyHead.jsp"></jsp:include>
		<div class="row chatMain">
			<div class="col-md-3">
				<div class="panel panel-primary" id="online">
					<div class="panel-heading">
						<h3 class="panel-title">当前在线的其他用户</h3>
					</div>
					<div class="panel-body">
						<div class="list-group" id="users">
						</div>
					</div>
				</div>
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">群发</h3>
					</div>
					<div class="panel-body">
						<input type="text" class="form-control"  id="msg" /><br>
						<button id="broadcast" type="button" class="btn btn-primary">发送</button>
					</div>
				</div>
				<div class="panel panel-primary onlineMsgPanel">
					<div class="panel-heading">
						<h3 class="panel-title">用户上下线信息</h3>
					</div>
					<div id="onlineMsg" class="panel-body">


					</div>
				</div>
			</div>
			<div class="col-md-9">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title" id="talktitle"></h3>
					</div>
					<div class="panel-body">
						<div class="well" id="log-container" style="height:400px;overflow-y:scroll">

						</div>
						<div class="form-group">
							<button id="send" type="button" class="btn btn-primary">发送</button>
							<div class="col-sm-11">
								<input type="text" id="myinfo" class="form-control col-md-9" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>



	
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		// 指定websocket路径
		var websocket;
		var requestUrlPrefix = "http://114.55.119.181:8082";
		if ('WebSocket' in window) {

			/*websocket = new WebSocket("ws://localhost:8080/ws?uid="+<shiro:principal property="id"/>);*/
			/*websocket = new WebSocket(urlPrefix+"/ws?uid="+<shiro:principal property="id"/>);*/
			websocket = new WebSocket("ws://114.55.119.181:8082//ws?uid="+<shiro:principal property="id"/>);
		} else if ('MozWebSocket' in window) {
			websocket = new MozWebSocket("ws://114.55.119.181:8082/ws"+<shiro:principal property="id"/>);
		} else {
			websocket = new SockJS("http://114.55.119.181:8082/ws/sockjs"+<shiro:principal property="id"/>);
		}
		//var websocket = new WebSocket('ws://localhost:8080/Spring-websocket/ws');
		websocket.onmessage = function(event) {
			var mydate = new Date();
			var mydateLocal=mydate.toLocaleString();
			var data=JSON.parse(event.data);
			if(data.from>0||data.from==-1){//用户或者群消息
				// 接收服务端的实时消息并添加到HTML页面中
				$("#log-container").append("<div class='bg-info'><label class='text-danger'>"+data.fromName+"&nbsp;"+data.date+"</label><div class='text-success'>"+data.text+"</div></div><br>");
				// 滚动条滚动到最低部
				scrollToBottom();
			}else if(data.from==0){//上线消息
				if(data.text!= "<shiro:principal property='name'/>")
				{
					$("#users").append('<a href="#" onclick="talk(this)" class="list-group-item">'+data.text+'</a>');

					$("#onlineMsg").append(data.text+"上线了"+mydateLocal+"<br>");
				}
			}else if(data.from==-2){//下线消息
				if(data.text!="<shiro:principal property='name'/>")
				{
					$("#users > a").remove(":contains('"+data.text+"')");
					$("#onlineMsg").append(data.text+"下线了"+mydateLocal+"<br>");
				}
			}
		};
		$.post("${ctx}/onlineusers",function(data){
			for(var i=0;i<data.length;i++)
				$("#users").append('<a href="#" onclick="talk(this)" class="list-group-item">'+data[i]+'</a>');
		});

		$("#broadcast").click(function(){
			$.post("${ctx}/broadcast",{"text":$("#msg").val()});
		});

		$("#send").click(function(){
			$.post("${ctx}/getuid",{"username":$("body").data("to")},function(d){
				var v=$("#myinfo").val();

				if(v==""){
					return;
				}else{
					var data={};
					data["from"]="<shiro:principal property='id'/>";
					data["fromName"]="<shiro:principal property="name"/>";
					data["to"]=d.id;
					data["text"]=v;
					websocket.send(JSON.stringify(data));
					$("#log-container").append("<div class='bg-success'><label class='text-info'>我&nbsp;"+new Date()+"</label><div class='text-info'>"+v+"</div></div><br>");
					scrollToBottom();
					$("#myinfo").val("");
				}
			});

		});

	});

	function talk(a){
		$("#talktitle").text("与"+a.innerHTML+"的聊天");
		$("body").data("to",a.innerHTML);
	}
	function scrollToBottom(){
		var div = document.getElementById('log-container');
		div.scrollTop = div.scrollHeight;
	}
</script>
</body>
</html>