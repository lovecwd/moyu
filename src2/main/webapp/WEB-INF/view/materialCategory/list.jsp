<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<jsp:include page="../common/header.jsp"></jsp:include>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<style type="text/css">
*{margin:0;padding:0;}
.all{
	float:left;
	width:100%;
	background-color: #434d63;
}
/* .menu{
	float:left;
	width:326px;
	margin-left: 200px;
	margin-top:70px;
} 
.body{
	float:left;
	width:800px;
	margin-left: 100px;
	margin-top:72px;
}*/
table{
	background-color: #fff;
	border-radius:17px;
	table-layout: fixed;
}
</style>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/main.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/body.css"/>
</head>
<body>
<%-- <jsp:include page="../common/head.jsp"></jsp:include> --%>
<div class="all">
	<%-- <div class="menu">
		<jsp:include page="../common/menu.jsp"></jsp:include>
	</div> --%>
	<div class="main">
		<div class="iconDiv">
			<div class="imageDetail"><img src="${ctx }/resources/images/logo.png"></div>
		</div>
		<div class="ulContent">
			<jsp:include page="../common/menu.jsp"></jsp:include>
		</div>
	</div>
	<div class="body">
	<%-- <%@include file="../common/bodyHead.jsp" %> --%>
	<jsp:include flush="true" page="../common/bodyHead.jsp"></jsp:include>
	<div class="formDiv">
	<div><a class="btn btn-default" href="${ctx }/admin/material/addCategory">添加素材类别</a></div>
	<form action="${ctx }/admin/material/categoryList" method="post" class="form-horizontal">
	</form>
	</div>
		<c:if test="${! empty  categories}">
		<div class="tableDiv">
		
			<table class="table table-bordered">
			<tr>
				<td>类别名称</td>
				<td>类别顺序</td>

				<td>操作</td>
			</tr>
			
			<c:forEach items="${categories }" var="category">
			<tr>
				<td>${category.categoryName }</td>
				<td>${category.categoryOrder }</td>

				<td><a onclick="return confirm('是否确认删除？');" class="btn btn-primary" href="${ctx }/admin/material/deleteCategory?id=${category.categoryId}">删除</a>
					<a class="btn btn-primary" href="${ctx }/admin/material/editCategory?id=${category.categoryId}">编辑</a>
				</td>
			</tr>
			</c:forEach>
			</table>
		</div>
		</c:if>
	</div>
</div>
</div>
</body>
</html>