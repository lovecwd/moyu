<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<jsp:include page="../common/header.jsp"></jsp:include>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>编辑素材类别</title>
<style type="text/css">
*{margin:0;padding:0;}
.all{
	float:left;
	width:100%;
	background-color: #434d63;
}
/* .menu{
	float:left;
	width:326px;
	margin-left: 200px;
	margin-top:70px;
} 
.body{
	float:left;
	width:800px;
	margin-left: 100px;
	margin-top:72px;
}*/
table{
	background-color: #fff;
	border-radius:17px;
	table-layout: fixed;
}
</style>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/main.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/body.css"/>
</head>
<body>
<%-- <jsp:include page="../common/head.jsp"></jsp:include> --%>
<div class="all">
	<%-- <div class="menu">
		<jsp:include page="../common/menu.jsp"></jsp:include>
	</div> --%>
	<div class="main">
		<div class="iconDiv">
			<div class="imageDetail"><img src="${ctx }/resources/images/logo.png"></div>
		</div>
		<div class="ulContent">
			<jsp:include page="../common/menu.jsp"></jsp:include>
		</div>
	</div>
	<div class="body">
		<%-- <%@include file="../common/bodyHead.jsp" %> --%>
		<jsp:include flush="true" page="../common/bodyHead.jsp"></jsp:include>
		
		<div class="formDiv">
		<a href="javascript:history.go(-1);" class="btn btn-default">返回</a>
	<form action="${ctx }/admin/material/editCategory" method="post">
			<input type="hidden" name="categoryId" value="${category.categoryId}">
			类别名称：<input type="text" name="categoryName" value="${category.categoryName}"/>
			类别顺序：<input type="text" name="categoryOrder" value="${category.categoryOrder}"/>

		<%--版本类别：<select name="versionType">
        <option value="ios">IOS</option>
            <option value="android">安卓</option>
        </select>
        版本号：<input type="text" name="versionNum" />--%>

	<input type="submit" class="btn btn-default" value="保存"/> 
			<div class="tableDiv">
		
			</div>
		</div>
	</div>
</div>
</div>
</body>
</html>