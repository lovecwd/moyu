<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>更新团队资源</title>
	<link rel="shortcut icon" href="${ctx }/resources/images/favicon.png">
<script type="text/javascript" src="${ctx}/resources/js/jquery-3.1.0.min.js"></script>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/bootstrap.min.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/main.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/body.css"/>
	<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/fileinput.min.css"/>
<script type="text/javascript" src="${ctx }/resources/js/jquery-3.1.0.min.js"></script>

	<script type="text/javascript" src="${ctx }/resources/js/fileinput.min.js"></script>
	<script type="text/javascript" src="${ctx }/resources/js/file-input-zh.js"></script>
<style type="text/css">
*{margin:0;padding:0;}
#updateForm{
	
}
 .formDiv{
 float:left;
	margin-left: 32px;
	margin-top:50px;
}
#submitBtn{
	margin-left: 28px;
}
.tableDiv{
	margin-left: 32px;
	margin-top: 22px;
}
table{
	background-color: #fff;
	border-radius:17px;
	table-layout: fixed;
}
td {
    word-break:break-all; word-wrap:break-word;
    color:#434d63;
}
/* td:HOVER {
	overflow: visible;
	width: auto;
} */
.headTr > td{
	color:#b1bcc0;
}
input{
	border-radius:10px;
	height:35px;
}
.lastUpdateMsg{
	float:left;
	margin-left: 32px;
	margin-top: 22px;
}
</style>
</head>
<body>
<%-- <jsp:include page="../common/head.jsp"></jsp:include> --%>
	<div class="all">
		<div class="main">
			<div class="iconDiv">
				<div class="imageDetail"><img src="${ctx }/resources/images/logo.png"></div>
			</div>
			<div class="ulContent">
				<jsp:include page="../common/menu.jsp"></jsp:include>
			</div>
		</div>
	<div class="body">
	
	<jsp:include page="../common/bodyHead.jsp"></jsp:include>
		<div class="formDiv">
		<a class="btn btn-default" href="javascript:history.go(-1);">返回</a>
		<form:form commandName="teamResource" enctype="multipart/form-data" modelAttribute="teamResource" action="${ctx }/admin/teamResource/update" class="form-horizontal" role="form" method="post">
			<form:input path="id" type="hidden"/>
			<div class="form-group">
				<label for="lastname" class="col-sm-2 control-label">上传资源者的姓名</label>
				<div class="col-sm-4">
					<form:input path="submitterName"  class="form-control" id="lastname" placeholder="上传资源者的姓名"/>
				</div>
			</div>
			<div class="form-group">
			    <label for="lastname" class="col-sm-2 control-label">团队资源名称</label>
			    <div class="col-sm-4">
			      <form:input path="resourceName"  class="form-control" id="lastname" placeholder="团队资源名称"/>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="lastname" class="col-sm-2 control-label">资源描述</label>
			    <div class="col-sm-4">
			      <form:input path="resourceDesc" type="text" class="form-control" id="lastname" placeholder="资源描述"/>
			    </div>
			  </div>
			<div class="form-group">
				<label for="lastname" class="col-sm-2 control-label">资源类别</label>
				<div class="col-sm-4">
					<form:select path="resourceCategory" class="form-control">
						<form:option value="business">商务类(负责人桃子)</form:option>
						<form:option value="plan">策划类(负责人绘艺)</form:option>
						<form:option value="art">美术类(负责人方方)</form:option>
						<form:option value="program">程序类(负责人老姜)</form:option>
						<form:option value="algorithm">算法类(负责人三石)</form:option>
						<form:option value="other">其他</form:option>
					</form:select>
				</div>
			</div>
			<div class="form-group">
				<label for="lastname" class="col-sm-2 control-label">资源添加时间</label>
				<div class="col-sm-4">
					<form:input path="saveDate" type="text" class="form-control" placeholder="资源添加时间"/>
				</div>
			</div>
			<div class="form-group">
				<label for="lastname" class="col-sm-2 control-label">具体资源文件</label>
				<div class="col-sm-4">
					<input type="file" name="resourceFile" class="file" value="${deal.image}" />
				</div>
			</div>
			  <div class="form-group">
			    <div class="col-sm-offset-2 col-sm-10">
			      <button type="submit" class="btn btn-default">更新</button>
			    </div>
			  </div>
		</form:form>
		</div>
		
		
	
	
	</div>
	</div>
</body>
<script type="text/javascript">
$(function(){
});
</script>
</html>