<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ page import="org.apache.shiro.subject.Subject" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@ include file="../common/header.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>BUG管理</title>
	<link rel="stylesheet" type="text/css" href="${ctx }/resources/css/layui.css" media="all">
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/fileinput.min.css"/>
<script type="text/javascript" src="${ctx }/resources/js/fileinput.min.js"></script>
	<script type="text/javascript" src="${ctx }/resources/js/file-input-zh.js"></script>
	<style type="text/css">
		#bugList img{
			width: 5rem;
		}
		*{margin:0;padding:0;}
		.table{
			width:100%;}
		.pageSizeSelect{
			width:100px;
			height:30px;
			border:1px solid #ccc;
			border-radius:4px;
		}
		.pageDiv{
			float:right;
			width: 100%;
		}
		.tableBottom{
			float:left;
			width:100%;
		}
		.table-desc{
			margin:20px 0 0 0;
			float:left;
			width:50%;
		}
		.pagination > li{
			float:left;
			margin:0 1px;
			border:1px solid #ddd;
		}
	</style>
</head>
<body>

	<div class="all">
		<div class="main">
			<div class="iconDiv">
				<div class="imageDetail"><img src="${ctx }/resources/images/logo.png"></div>
			</div>
			<div class="ulContent">
				<jsp:include page="../common/menu.jsp"></jsp:include>
			</div>
		</div>
	<div class="body">
	<jsp:include page="../common/bodyHead.jsp"></jsp:include>
		<div class="formDiv layui-hide">
			<form id="bugForm" action="${ctx }/admin/bug/list" class="form-horizontal" method="post">
				<div class="form-group">
					<div class="col-sm-2"></div>
					<input id="pageNo" name="pageNo" type="hidden" value="${pageInfo.pageNo}"/>
					<input id="pageSize" name="pageSize" type="hidden" value="${pageInfo.pageSize}"/>
				</div>
			</form>
		</div>
	<div class="tableDiv">
		<div>
			<a href="${ctx}/admin/bug/add" class="btn btn-default">提交BUG</a>
		</div>
		<div>BUG列表</div>
		<div class="errorText">${errorMsg}</div>
		<table id="bugList" class="table table-borded">
			<tr>
				<th>序号</th>
				<th>提交者姓名</th>
				<th>受理bug的人的姓名</th>
				<th>Bug对应的APP版本类型</th>
				<th>Bug对应的APP版本号</th>
				<th>Bug的描述</th>
				<th>Bug提交日期</th>
				<th>Bug解决日期</th>
				<th>Bug状态</th>
				<th>Bug相关的图片</th>
				<th>对Bug进行操作</th>
			</tr>
			<c:forEach items="${pageInfo.list }" var="bug" varStatus="varStatus">
				<tr>
					<td>${varStatus.count}</td>
					<td>${bug.submitterName }</td>
					<td>${bug.receiverName }</td>
					<td>${bug.versionType}</td>
					<td>${bug.versionNumber}</td>
					<td>${bug.bugDesc}</td>
					<td>${bug.submitDate}</td>
					<td>${bug.dealedDate}</td>
					<td>
						<c:choose>
							<c:when test="${bug.bugStatus eq '0'}">待解决</c:when>
							<c:when test="${bug.bugStatus eq '1'}">已经解决</c:when>
						</c:choose>

					</td>
					<td>
						<c:forEach items="${bug.bugImgUrls.split('-')}" var="imgUrl">
							<c:if test="${imgUrl == ''}">
								无图片
							</c:if>
							<c:if test="${imgUrl != ''}">
								<img src="${imgUrl}" />
							</c:if>

						</c:forEach>
					</td>

					<td>
						<a class="btn btn-default dealBtn" href="${ctx}/admin/bug/dealed?id=${bug.id}&operator=<shiro:principal property='name'/>/>">已解决</a>
						<a class="btn btn-default dealBtn" href="${ctx}/admin/bug/detail?id=${bug.id}">详细信息</a>
						<a class="btn btn-default dealBtn" href="${ctx}/admin/bug/delete?id=${bug.id}">删除bug</a>
					</td>
				</tr>
			</c:forEach>

		</table>
		<div class="tableBottom">
			<div class="table-desc form-group">
				<label class="control-label">每页</label>
				<select class="pageSizeSelect" name="pageSize">
					<option <c:if test="${pageInfo.pageSize == '10' }">selected</c:if> value="10">10条</option>
					<option <c:if test="${pageInfo.pageSize == '20' }">selected</c:if> value="20">20条</option>
					<option <c:if test="${pageInfo.pageSize == '50' }">selected</c:if> value="50">50条</option>
				</select>
				<label class="control-label">条记录 第${pageInfo.pageNo }页 共${pageInfo.totalPage }页 总共${pageInfo.allRow }条记录</label>
			</div>
			<%--<div id="pageDiv" class="pageDiv">
				<ul class="pagination pagination-sm">
					<c:forEach begin="1" end="${pageInfo.totalPage }"  var="i">
						<li><a class="hrefClass" href="#">${i }</a></li>
					</c:forEach>
				</ul>

			</div>--%>
		</div>
		<div id="pageTest">

		</div>
	</div>

	</div>
</div>
<script type="text/javascript">
			$(function () {
				$(".hrefClass").click(function(){
					var pageNo = $(this).text();
					var pageSize= $(".pageSizeSelect").find("option:selected").val();
					$("#pageNo").val(pageNo);
					$("#pageSize").val(pageSize);
					$("#bugForm").attr("action","${ctx }/admin/bug/list");
					$("#bugForm").submit();
				});
			});
</script>
<script type="text/javascript" src="${ctx }/resources/js/layui.all.js"></script>
<script type="text/javascript">
	layui.use(['layer','laypage'], function(){
		var laypage = layui.laypage,layer = layui.layer;

		//分页
		laypage({
			cont: 'pageTest' //分页容器的id
			,pages: ${pageInfo.totalPage } //总页数
			,curr: ${pageInfo.pageNo }
			,skin: '#5FB878' //自定义选中色值
			,skip: true //开启跳页
			,jump: function(obj, first){
				if(!first){
					layer.msg('第'+ obj.curr +'页');
					var pageNo = obj.curr;
					var pageSize= $(".pageSizeSelect").find("option:selected").val();
					$("#pageNo").val(pageNo);
					$("#pageSize").val(pageSize);
					$("#bugForm").attr("action","${ctx }/admin/bug/list");
					$("#bugForm").submit();
				}
			}
		});
	});
</script>
</body>
</html>