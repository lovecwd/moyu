<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>BUG明细</title>
	<link rel="shortcut icon" href="${ctx }/resources/images/favicon.png">
<script type="text/javascript" src="${ctx}/resources/js/jquery-3.1.0.min.js"></script>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/bootstrap.min.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/main.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/body.css"/>
<script type="text/javascript" src="${ctx }/resources/js/jquery-3.1.0.min.js"></script>
<style type="text/css">
*{margin:0;padding:0;}
#updateForm{
	
}
 .formDiv{
 float:left;
	margin-left: 32px;
	margin-top:50px;
}
#submitBtn{
	margin-left: 28px;
}
.tableDiv{
	margin-left: 32px;
	margin-top: 22px;
}
table{
	background-color: #fff;
	border-radius:17px;
	table-layout: fixed;
}
td {
    word-break:break-all; word-wrap:break-word;
    color:#434d63;
}
/* td:HOVER {
	overflow: visible;
	width: auto;
} */
.headTr > td{
	color:#b1bcc0;
}
input{
	border-radius:10px;
	height:35px;
}
.lastUpdateMsg{
	float:left;
	margin-left: 32px;
	margin-top: 22px;
}
	#bugImgDiv img{
		width: 30rem;
	}
</style>
</head>
<body>
<%-- <jsp:include page="../common/head.jsp"></jsp:include> --%>
	<div class="all">
		<div class="main">
			<div class="iconDiv">
				<div class="imageDetail"><img src="${ctx }/resources/images/logo.png"></div>
			</div>
			<div class="ulContent">
				<jsp:include page="../common/menu.jsp"></jsp:include>
			</div>
		</div>
	<div class="body">
	
	<jsp:include page="../common/bodyHead.jsp"></jsp:include>
		<div class="tableDiv">
		<a class="btn btn-default" href="javascript:history.go(-1);">返回</a>
			<div class="form-group">
				<label for="" class="col-md-1 control-label">提交者姓名</label>
				<div class="col-md-5 tl th">
					${bug.submitterName}
				</div>
			</div>
			<div class="form-group">
				<label for="" class="col-md-1 control-label">受理bug的人的姓名</label>
				<div class="col-md-5 tl th">
					${bug.receiverName}
				</div>
			</div>
			<div class="form-group">
				<label for="" class="col-md-1 control-label">Bug对应的APP版本类型</label>
				<div class="col-md-5 tl th">
					${bug.versionType}
				</div>
			</div>
			<div class="form-group">
				<label for="" class="col-md-1 control-label">Bug对应的APP版本号</label>
				<div class="col-md-5 tl th">
					${bug.versionNumber}
				</div>
			</div>
			<div class="form-group">
				<label for="" class="col-md-1 control-label">Bug的描述</label>
				<div class="col-md-5 tl th">
					${bug.bugDesc}
				</div>
			</div>
			<div class="form-group">
				<label for="" class="col-md-1 control-label">Bug提交日期</label>
				<div class="col-md-5 tl th">
					${bug.submitDate}
				</div>
			</div>
			<div class="form-group">
				<label for="" class="col-md-1 control-label">Bug解决日期</label>
				<div class="col-md-5 tl th">
					${bug.dealedDate}
				</div>
			</div>
			<div class="form-group">
				<label for="" class="col-md-1 control-label">Bug状态</label>
				<div class="col-md-5 tl th">
					<c:choose>
						<c:when test="${bug.bugStatus eq '0'}">待解决</c:when>
						<c:when test="${bug.bugStatus eq '1'}">已经解决</c:when>
					</c:choose>
				</div>
			</div>

			<div id="bugImgDiv" class="form-group">
				<label for="" class="col-md-1 control-label">Bug相关的图片</label>
				<div class="col-md-10 tl th">
					<c:forEach items="${bug.bugImgUrls.split('-')}" var="imgUrl">
						<img src="${imgUrl}" />
					</c:forEach>
				</div>
			</div>

		</div>
		
		
	
	
	</div>
	</div>
</body>
<script type="text/javascript">
$(function(){
});
</script>
</html>