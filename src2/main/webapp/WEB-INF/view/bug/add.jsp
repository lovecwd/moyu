<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>提交bug</title>
	<link rel="shortcut icon" href="${ctx }/resources/images/favicon.png">
<script type="text/javascript" src="${ctx}/resources/js/jquery-3.1.0.min.js"></script>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/bootstrap.min.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/main.css"/>
<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/body.css"/>
	<link type="text/css" rel="stylesheet" href="${ctx }/resources/css/fileinput.min.css"/>
<script type="text/javascript" src="${ctx }/resources/js/jquery-3.1.0.min.js"></script>

	<script type="text/javascript" src="${ctx }/resources/js/fileinput.min.js"></script>
	<script type="text/javascript" src="${ctx }/resources/js/file-input-zh.js"></script>
<style type="text/css">
*{margin:0;padding:0;}
#updateForm{
	
}
 .formDiv{
 float:left;
	margin-left: 32px;
	margin-top:50px;
}
#submitBtn{
	margin-left: 28px;
}
.tableDiv{
	margin-left: 32px;
	margin-top: 22px;
}
table{
	background-color: #fff;
	border-radius:17px;
	table-layout: fixed;
}
td {
    word-break:break-all; word-wrap:break-word;
    color:#434d63;
}
/* td:HOVER {
	overflow: visible;
	width: auto;
} */
.headTr > td{
	color:#b1bcc0;
}
input{
	border-radius:10px;
	height:35px;
}
.lastUpdateMsg{
	float:left;
	margin-left: 32px;
	margin-top: 22px;
}
	.form-group{
		height: inherit;

	}
	.errorText{
		color: red;
		font-size: 2rem;
	}
</style>
</head>
<body>
<%-- <jsp:include page="../common/head.jsp"></jsp:include> --%>
	<div class="all">
		<div class="main">
			<div class="iconDiv">
				<div class="imageDetail"><img src="${ctx }/resources/images/logo.png"></div>
			</div>
			<div class="ulContent">
				<jsp:include page="../common/menu.jsp"></jsp:include>
			</div>
		</div>
	<div class="body">
	
	<jsp:include page="../common/bodyHead.jsp"></jsp:include>
		<div class="formDiv">
		<a class="btn btn-default" href="javascript:history.go(-1);">返回</a>
		<form:form commandName="bug" enctype="multipart/form-data" modelAttribute="bug" action="${ctx }/admin/bug/add" class="form-horizontal" role="form" method="post">
			  <div class="form-group">
			    <label for="submitterName" class="col-sm-2 control-label">提交者姓名</label>
			    <div class="col-sm-4">
					<form:select path="submitterName" class="form-control">
						<c:forEach items="${userNames}" var="username">
							<form:option value="${username}">${username}</form:option>
						</c:forEach>
					</form:select>

			      <%--<form:input path="submitterName"  class="form-control" id="lastname" placeholder="提交者姓名"/>--%>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="receiverName" class="col-sm-2 control-label">受理bug的人的姓名</label>
			    <div class="col-sm-4">
					<form:select path="receiverName" class="form-control">
						<c:forEach items="${userNames}" var="username">
							<form:option value="${username}">${username}</form:option>
						</c:forEach>
					</form:select>
			      <%--<form:input path="receiverName" type="text" class="form-control" placeholder="受理bug的人的姓名"/>--%>
			    </div>
			  </div>
			<div class="form-group">
				<label for="lastname" class="col-sm-2 control-label">Bug对应的APP版本类型</label>
				<div class="col-sm-4">
					<form:select class="form-control" path="versionType">
						<form:option value="ios">IOS版本</form:option>
						<form:option value="android">安卓版本</form:option>
					</form:select>
				</div>
			</div>
			<div class="form-group">
				<label for="lastname" class="col-sm-2 control-label">Bug对应的APP版本号</label>
				<div class="col-sm-4">

					<form:input type="text" path="versionNumber" class="form-control"/>
				</div>
			</div>
			<div class="form-group">
				<label for="bugDesc" class="col-sm-2 control-label">Bug的描述</label>
				<div class="col-sm-4">
					<form:textarea rows="5" path="bugDesc" class="form-control"/>
				</div>
			</div>

			<div class="form-group">
				<label for="lastname" class="col-sm-2 control-label">BUG描述的图片</label>
				<div class="col-sm-3">
					<input type="file" name="bugImage" class="file" value="${deal.image}" />
				</div>
				<div class="col-sm-3">
					<input type="file" name="bugImage" class="file" value="${deal.image}" />
				</div>
				<div class="col-sm-3">
					<input type="file" name="bugImage" class="file" value="${deal.image}" />
				</div>
			</div>
			<div>
				<label class="col-sm-2"></label>
				<div class="col-sm-4 errorText">
					${errorMsg}
				</div>
			</div>
			  <div class="form-group">
			    <div class="col-sm-offset-2 col-sm-10">
			      <button type="submit" class="btn btn-default">保存</button>
			    </div>
			  </div>
		</form:form>
		</div>
		
		
	
	
	</div>
	</div>
</body>
<script type="text/javascript">
$(function(){
});
</script>
</html>