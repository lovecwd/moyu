<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <script type="text/javascript" src="${ctx }/resources/js/bootstrap.min.js"></script>

<%-- <ul class="list-group">
		    <li class="list-group-item"><a href="${ctx }/admin/material/categoryList" >素材类别列表</a></li>
		    <li class="list-group-item"><a href="${ctx }/admin/material/update">素材更新列表</a></li>
		    <li class="list-group-item"><a href="${ctx }/admin/material/list">素材数据统计</a></li>
		    <li class="list-group-item">
		      <a href="${ctx }/admin/filter/list">滤镜数据统计</a>
		    </li>
		    <li class="list-group-item"><a href="${ctx }/admin/client/list">用户行为统计</a></li>
		    <li class="list-group-item"><a href="${ctx }/admin/client/shareData">分享数据统计</a></li>
		    <li class="list-group-item"><a href="${ctx }/admin/feedback/list">用户反馈统计</a></li>
		    <li class="list-group-item"><a href="${ctx }/admin/user/list">用户管理</a></li>
		    <li class="list-group-item"><a href="${ctx }/admin/material/listMaterials">素材列表管理</a></li>
		    <li class="list-group-item"><a href="${ctx }/admin/appVersion/list">APP版本管理</a></li>
		    <li class="list-group-item"><a href="${ctx }/admin/filter/manage">APP滤镜管理</a></li>
</ul> --%>

<ul id="main-nav" class="nav nav-tabs nav-stacked" style="">
	<li >
		<a href="${ctx }/admin/index">
		<!-- <i class="glyphicon glyphicon-th-large"></i> -->
		<i><img style="width: 15%;margin-top: -3%;" src="${ctx }/resources/images/shouye.png"/></i>
		首页 
		</a>
	</li>
	<li>
		<a href="#teamwork" class="nav-header collapsed" data-toggle="collapse">
			<!-- <i class="glyphicon glyphicon-th"></i> -->
			<i><img style="width: 15%;margin-top: -3%;" src="${ctx }/resources/images/team.png"/></i>
			团队协作
			<!-- <span class="pull-right glyphicon glyphicon-chevron-down"></span> -->
		</a>
		<ul id="teamwork" class="nav nav-list collapse secondmenu" style="height: 0px;">
			<li><a href="${ctx }/admin/teamResource/list">团队资源管理</a></li>
			<li><a href="${ctx }/admin/bug/list">BUG管理</a></li>
			<li><a href="${ctx }/admin/chatroom">团队交流室</a></li>
		</ul>
	</li>
	<li>
		<a href="#statisticInfo" class="nav-header collapsed" data-toggle="collapse">
		<!-- <i class="glyphicon glyphicon-th"></i> -->
		<i><img style="width: 15%;margin-top: -3%;" src="${ctx }/resources/images/tongyi.png"/></i>
		统计相关
		<!-- <span class="pull-right glyphicon glyphicon-chevron-down"></span> -->
		</a>
		<ul id="statisticInfo" class="nav nav-list collapse secondmenu" style="height: 0px;">
			<li><a href="${ctx }/admin/material/list">素材数据统计</a></li>
			<li><a href="${ctx }/admin/client/list">用户行为统计</a></li>
			<li><a href="${ctx }/admin/client/shareData">分享数据统计</a></li>
			<li><a href="${ctx }/admin/feedback/list">用户反馈统计</a></li>
			<li><a href="${ctx }/admin/filter/list">滤镜数据统计</a></li>
		</ul>
	</li>
	<li>
		<a href="#systemSetting" class="nav-header collapsed" data-toggle="collapse">
		<!-- <i class="glyphicon glyphicon-cog"></i> -->
		<i><img style="width: 15%;margin-top: -3%;" src="${ctx }/resources/images/xitong.png"/></i>
		系统管理
		<!-- <span class="pull-right glyphicon glyphicon-chevron-down"></span> -->
		</a>
		<ul id="systemSetting" class="nav nav-list collapse secondmenu" style="height: 0px;">
		<li><a href="${ctx }/admin/user/list">用户管理</a></li>
		<li><a href="${ctx }/admin/appVersion/list">版本管理</a></li>
		<li><a href="${ctx }/admin/filter/manage">滤镜管理</a></li>
		<li><a href="${ctx }/admin/cdn/list">CDN管理</a></li>
		<li><a href="${ctx }/admin/log/list">日志查看</a></li>
		</ul>
	</li>
	<li>
		<a href="#materialInfo" class="nav-header collapsed" data-toggle="collapse">
		<!-- <i class="glyphicon glyphicon-cog"></i> -->
		<i><img style="width: 15%;margin-top: -3%;" src="${ctx }/resources/images/sucai.png"/></i>
		素材管理
		<!-- <span class="pull-right glyphicon glyphicon-chevron-down"></span> -->
		</a>
		<ul id="materialInfo" class="nav nav-list collapse secondmenu" style="height: 0px;">
			<li><a href="${ctx }/admin/material/categoryList" >素材类别列表</a></li>
			<li><a href="${ctx }/admin/material/listMaterials">素材列表管理</a></li>

			<%--<li><a href="${ctx }/admin/material/update">素材更新列表</a></li>--%>
		</ul>
	</li>
</ul>