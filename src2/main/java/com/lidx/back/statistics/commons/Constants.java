package com.lidx.back.statistics.commons;

public class Constants {

	public static final String RESPONSE_ERROR = "失败";
	
	public static final String RESPONSE_SECCESS = "成功";
	
	public static final String QUERY_MATERIAL_DATA = "material";
	
	public static final String QUERY_FILTER_DATA = "filter";
	
	public static final String QUERY_CLIENT_DATA = "client";
	
	public static final String QUERY_PICTURE_SHARE_DATA = "picShare";
	
	public static final String APP_VERSION_ANDROID = "android";
	
	public static final String APP_VERSION_IOS = "ios";
	
	public static final String PERSON_SEX_MALE = "男";
	
	public static final String PERSON_SEX_FEMALE = "女";
	
	public static final String PERSON_SEX_FEMALE_CODE = "0";
	
	public static final String IMAGE_FILE_PATH = "d://pictures";
	
	public static final String FEEDBACKIMAGES_DIRECTORY = "feedbackImages";
	
	public static final String IIS_IMAGES_URL_PREFIX_CODE = "imageUrlPrefix";
	
	public static final String APP_CAN_USE_NO_UPDATE = "0";
	
	public static final String APP_CAN_USE_HAVE_UPDATE = "1";
	
	public static final String APP_NO_USE_NEED_UPDATE = "-1";
	
	public static final String REDIS_MATERIAL_LIST_KEY = "materialList";
	
	public static final String MATERIALS_DIRECTORY = "materials";

	public static final String SHARE_CHANNEL_WEIXIN = "wechat";//微信

	public static final String SHARE_CHANNEL_QQ = "qq";

	public static final String SHARE_CHANNEL_WEIBO = "weibo";

	public static final String SHARE_CHANNEL_WXPYQ = "moment";//微信朋友圈

	public static final String FILE_UPLOAD_PATH = "upload";

	public static final String FILE_DOWNLOAD_PATH = "download";

	public static final String FILE_BUG_PATH = "bugImgs";

	public static final String NGINX_URL_ROOT = "http://api.magicjoy.cn/";
	
}
