package com.lidx.back.statistics.entity.excel;

import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelTarget;

@ExcelTarget("client")
public class ClientExcel {

	@Excel(name="用户行为",needMerge=true)
	private String action;
	@Excel(name="设备版本信息")
	private String versionType;
	@Excel(name="相关数量")
	private String num;
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getVersionType() {
		return versionType;
	}
	public void setVersionType(String versionType) {
		this.versionType = versionType;
	}
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
	
}
