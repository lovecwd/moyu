package com.lidx.back.statistics.entity;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="material_new")
@Cache(usage =  CacheConcurrencyStrategy.READ_WRITE, region="materialPO") 
@DynamicUpdate
@DynamicInsert
public class MaterialPO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 679314106572912964L;
	// 记录的主键
	
	private Integer id;
	// 特定素材的唯一标示；同一素材的不同版本的该值应该相同。
	private String uuid;
	//推荐素材的顺序
	//private String recommendOrder;
	//类别顺序
	private Integer categoryOrder;
	// 素材对应的APP版本类型
	private String versionType;
	// 素材对应的APP版本号
	private String versionNumber;
	// 是否是推荐素材
	//private String isRecommend;
	// 图标文件名
	private String iconName;
	// 图标文件MD5
	private String iconMD5;
	// 图标文件下载路径
	private String iconDownloadUrl;

	// 素材文件名
	private String materialName;
	// 素材文件MD5
	private String materialMD5;
	// 素材文件下载路径
	private String materialDownloadUrl;

	/*//素材是否是内置的（0,1）
	private String isIconDownloaded;*/
	//素材类别id
	private Integer categoryId;
	//素材的性别取向(0:女 1:：男  -1：男女通吃) 此版本不需要
	//private String gender;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updateTime;
	//素材是否有声音
	private String audio;

	public String getAudio() {
		return audio;
	}

	public void setAudio(String audio) {
		this.audio = audio;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	/*public String getRecommendOrder() {
		return recommendOrder;
	}
	public void setRecommendOrder(String recommendOrder) {
		this.recommendOrder = recommendOrder;
	}*/

	public Integer getCategoryOrder() {
		return categoryOrder;
	}

	public void setCategoryOrder(Integer categoryOrder) {
		this.categoryOrder = categoryOrder;
	}

	public String getVersionType() {
		return versionType;
	}
	public void setVersionType(String versionType) {
		this.versionType = versionType;
	}
	public String getVersionNumber() {
		return versionNumber;
	}
	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

	/*public String getIsRecommend() {
		return isRecommend;
	}
	public void setIsRecommend(String isRecommend) {
		this.isRecommend = isRecommend;
	}*/
	public String getIconName() {
		return iconName;
	}
	public void setIconName(String iconName) {
		this.iconName = iconName;
	}
	public String getIconMD5() {
		return iconMD5;
	}
	public void setIconMD5(String iconMD5) {
		this.iconMD5 = iconMD5;
	}
	public String getIconDownloadUrl() {
		return iconDownloadUrl;
	}
	public void setIconDownloadUrl(String iconDownloadUrl) {
		this.iconDownloadUrl = iconDownloadUrl;
	}
	public String getMaterialName() {
		return materialName;
	}
	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}
	public String getMaterialMD5() {
		return materialMD5;
	}
	public void setMaterialMD5(String materialMD5) {
		this.materialMD5 = materialMD5;
	}
	public String getMaterialDownloadUrl() {
		return materialDownloadUrl;
	}
	public void setMaterialDownloadUrl(String materialDownloadUrl) {
		this.materialDownloadUrl = materialDownloadUrl;
	}
	/*public String getIsIconDownloaded() {
		return isIconDownloaded;
	}
	public void setIsIconDownloaded(String isIconDownloaded) {
		this.isIconDownloaded = isIconDownloaded;
	}*/
	/*public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}*/

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
}
