package com.lidx.back.statistics.entity;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="material_category")
//素材的分类，需要动态绑定到素材上
@Cache(usage =  CacheConcurrencyStrategy.READ_ONLY, region="materialCategory") 
public class MaterialCategory implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4671010457561635146L;

	
	private Integer categoryId;
	private String categoryName;
	private Integer categoryOrder;

	//private String gender;
	/*private String versionType;
	private String versionNum;*/
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Integer getCategoryOrder() {
		return categoryOrder;
	}

	public void setCategoryOrder(Integer categoryOrder) {
		this.categoryOrder = categoryOrder;
	}

	/*public String getGender() {
			return gender;
        }
        public void setGender(String gender) {
            this.gender = gender;
        }
	public String getVersionType() {
		return versionType;
	}
	public void setVersionType(String versionType) {
		this.versionType = versionType;
	}
	public String getVersionNum() {
		return versionNum;
	}
	public void setVersionNum(String versionNum) {
		this.versionNum = versionNum;
	}*/

	public MaterialCategory() {
	}

	public MaterialCategory(Integer categoryId, String categoryName, Integer categoryOrder) {
		this.categoryId = categoryId;
		this.categoryName = categoryName;
		this.categoryOrder = categoryOrder;
	}
}
