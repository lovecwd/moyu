package com.lidx.back.statistics.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrator on 2017/4/11.
 */
@Entity
@Table(name = "latest_material")
public class MaterialLatest {
    private Integer id;
    private Integer materialId;
    private String versionType;
    private String materialName;
    private Integer categoryId;
    private Integer categoryOrder;
    private Date createTime;

    public String getVersionType() {
        return versionType;
    }

    public void setVersionType(String versionType) {
        this.versionType = versionType;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMaterialId() {
        return materialId;
    }

    public void setMaterialId(Integer materialId) {
        this.materialId = materialId;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getCategoryOrder() {
        return categoryOrder;
    }

    public void setCategoryOrder(Integer categoryOrder) {
        this.categoryOrder = categoryOrder;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public MaterialLatest(MaterialPO m) {
        this.setVersionType(m.getVersionType());
        this.setMaterialName(m.getMaterialName());
        this.setMaterialId(m.getId());
        this.setCategoryId(m.getCategoryId());
        this.setCategoryOrder(m.getCategoryOrder());
        this.setCreateTime(new Date());
    }

    public MaterialLatest() {
    }
}
