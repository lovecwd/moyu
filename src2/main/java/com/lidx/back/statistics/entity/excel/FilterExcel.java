package com.lidx.back.statistics.entity.excel;

import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelTarget;

/**
 * Created by chen on 2017/2/10.
 */
@ExcelTarget("filterExcel")
public class FilterExcel {

    @Excel(name="滤镜名称")
    private String filterName;
    @Excel(name="APP版本类型",width = 20)
    private String versionType;
    @Excel(name="APP版本号",width = 20)
    private String versionNumber;
    @Excel(name="数量")
    private String number;

    public String getFilterName() {
        return filterName;
    }

    public void setFilterName(String filterName) {
        this.filterName = filterName;
    }

    public String getVersionType() {
        return versionType;
    }

    public void setVersionType(String versionType) {
        this.versionType = versionType;
    }

    public String getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(String versionNumber) {
        this.versionNumber = versionNumber;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
