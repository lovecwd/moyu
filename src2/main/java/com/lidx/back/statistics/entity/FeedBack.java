package com.lidx.back.statistics.entity;

import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelTarget;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="feedback_new")
@ExcelTarget("feedback")
public class FeedBack implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7776330298232940655L;
	
	private Integer id;
    /*@Excel(name="QQ号码",width=25)
    private String qq;
    @Excel(name="手机号码",width=25)
    private String phone;*/
    @Excel(name = "评论内容", width = 50)
    private String content;
    /*@Excel(name="用户反馈图片",type=2,width=50,height=100,isWrap=true,imageType=1)
    private String imageNames;*/
    @Excel(name = "版本类型", width = 25)
    private String versionType;
	@Excel(name="版本号",width=25)
	private String versionNum;
	@Excel(name="反馈时间",width=25)
	private Date feedbackTime;
	
	@Id
	@Column(name="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

    /*public String getQq() {
        return qq;
    }
    public void setQq(String qq) {
        this.qq = qq;
    }
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }*/
    public String getContent() {
        return content;
	}
	public void setContent(String content) {
		this.content = content;
	}

    /*public String getImageNames() {
        return imageNames;
    }
    public void setImageNames(String imageNames) {
        this.imageNames = imageNames;
    }*/
    public String getVersionType() {
        return versionType;
	}
	public void setVersionType(String versionType) {
		this.versionType = versionType;
	}
	public String getVersionNum() {
		return versionNum;
	}
	public void setVersionNum(String versionNum) {
		this.versionNum = versionNum;
	}
	public Date getFeedbackTime() {
		return feedbackTime;
	}
	public void setFeedbackTime(Date feedbackTime) {
		this.feedbackTime = feedbackTime;
	}
}
