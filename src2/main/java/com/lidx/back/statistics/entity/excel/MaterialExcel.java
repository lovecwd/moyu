package com.lidx.back.statistics.entity.excel;

import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelTarget;

@ExcelTarget("material")
public class MaterialExcel {

	/*@Excel(name="素材统计行为",needMerge=true)
	private String action;*/
	//private String action;
	@Excel(name="素材的名称")
	private String uuid;
	@Excel(name="设备版本信息")
	private String versionType;
	@Excel(name="APP版本信息")
	private String versionNumber;
	@Excel(name="相关数量")
	private String num;
	@Excel(name="渠道")
	private String channel;
	public String getUuid() {
		return uuid;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getVersionType() {
		return versionType;
	}
	public void setVersionType(String versionType) {
		this.versionType = versionType;
	}
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
	public String getVersionNumber() {
		return versionNumber;
	}
	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}
	
}
