package com.lidx.back.statistics.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by chen on 2017/2/18.
 */
@Entity
@Table(name="bug")
public class Bug {
    private int id;
    //Bug的描述
    private String bugDesc;
    //bug的描述图片
    private String bugImgUrls;
    //提交bug的人的名字
    private String submitterName;
    //接收问题的人的名字
    private String receiverName;
    private String versionType;
    private String versionNumber;
    private Date submitDate;
    private Date dealedDate;
    private String bugStatus;//0代表未解决，1代表已解决

    public Date getDealedDate() {
        return dealedDate;
    }

    public void setDealedDate(Date dealedDate) {
        this.dealedDate = dealedDate;
    }

    public String getBugStatus() {
        return bugStatus;
    }

    public void setBugStatus(String bugStatus) {
        this.bugStatus = bugStatus;
    }

    public Date getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(Date submitDate) {
        this.submitDate = submitDate;
    }

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBugDesc() {
        return bugDesc;
    }

    public void setBugDesc(String bugDesc) {
        this.bugDesc = bugDesc;
    }

    public String getBugImgUrls() {
        return bugImgUrls;
    }

    public void setBugImgUrls(String bugImgUrls) {
        this.bugImgUrls = bugImgUrls;
    }

    public String getSubmitterName() {
        return submitterName;
    }

    public void setSubmitterName(String submitterName) {
        this.submitterName = submitterName;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getVersionType() {
        return versionType;
    }

    public void setVersionType(String versionType) {
        this.versionType = versionType;
    }

    public String getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(String versionNumber) {
        this.versionNumber = versionNumber;
    }
}
