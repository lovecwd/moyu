package com.lidx.back.statistics.entity.hotUpdate;

public class MaterialUpdateBean {
	
	// 素材对应的APP版本类型
	private String versionType;
	// 素材对应的APP版本号
	private String versionNumber;
	// 是否是推荐素材
	private String isRecommend;
	// 特定素材的唯一标示；同一素材的不同版本的该值应该相同。
	private String uuid;
	//推荐素材的顺序
	private String recommendOrder;
	//类别顺序
	private String categoryOrder;
	private String gender;
	// 图标文件名
	private String iconName;
	// 素材文件名
	private String materialName;
	//素材类别
	private String category;
	public String getIsRecommend() {
		return isRecommend;
	}

	public void setIsRecommend(String isRecommend) {
		this.isRecommend = isRecommend;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getVersionType() {
		return versionType;
	}

	public void setVersionType(String versionType) {
		this.versionType = versionType;
	}

	public String getVersionNumber() {
		return versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getRecommendOrder() {
		return recommendOrder;
	}

	public void setRecommendOrder(String recommendOrder) {
		this.recommendOrder = recommendOrder;
	}

	public String getCategoryOrder() {
		return categoryOrder;
	}

	public void setCategoryOrder(String categoryOrder) {
		this.categoryOrder = categoryOrder;
	}

	public String getIconName() {
		return iconName;
	}

	public void setIconName(String iconName) {
		this.iconName = iconName;
	}

	public String getMaterialName() {
		return materialName;
	}

	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
	
}
