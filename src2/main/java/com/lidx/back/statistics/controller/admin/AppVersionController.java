package com.lidx.back.statistics.controller.admin;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.lidx.back.statistics.entity.SystemVersionConfig;
import com.lidx.back.statistics.rest.VersionBean;
import com.lidx.back.statistics.service.ISystemVersionService;

@RequestMapping(value="/admin/appVersion")
@Controller
public class AppVersionController {

	@Autowired
	private ISystemVersionService versionService;
	
	@RequestMapping(value="list",method=RequestMethod.GET)
	public ModelAndView list(){
		ModelAndView mav = new ModelAndView("appVersion/list");
		VersionBean versionBean = new VersionBean();
		try {
			List<SystemVersionConfig> versions = versionService.getVersionConfigList(versionBean);
			mav.addObject("versions",versions);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return mav;
		}
		return mav;
	}
	
	@RequestMapping(value="list",method=RequestMethod.POST)
	public ModelAndView listGo(@RequestParam String versionType,@RequestParam String versionNumber){
		ModelAndView mav = new ModelAndView("appVersion/list");
		VersionBean versionBean = new VersionBean();
		try {
			if(StringUtils.isNotBlank(versionType)){
				versionBean.setVersionType(versionType);
			}
			if(StringUtils.isNotBlank(versionNumber)){
				versionBean.setVersionNumber(versionNumber);
			}
			List<SystemVersionConfig> versions = versionService.getVersionConfigList(versionBean);
			mav.addObject("versions",versions);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return mav;
		}
		return mav;
	}
	
	@RequestMapping(value="add",method=RequestMethod.GET)
	public ModelAndView add(){
		ModelAndView mav = new ModelAndView("appVersion/add");
		return mav;
	}
	
	@RequestMapping(value="add",method=RequestMethod.POST)
	public ModelAndView addGo(@RequestParam String versionType,
			@RequestParam String versionNumber,
			@RequestParam String latestVersionNum,
			@RequestParam String versionDesc,
			@RequestParam String isAvailable){
		ModelAndView mav = new ModelAndView("redirect:/admin/appVersion/list");
		if(StringUtils.isNotBlank(versionNumber) && StringUtils.isNotBlank(versionType)
				&& StringUtils.isNotBlank(latestVersionNum)
				&& StringUtils.isNotBlank(versionDesc)
				&& StringUtils.isNotBlank(isAvailable)){
			SystemVersionConfig versionConfig = new SystemVersionConfig();
			versionConfig.setIsAvailable(isAvailable);
			versionConfig.setLatestVersionNum(latestVersionNum);
			versionConfig.setVersionNum(versionNumber);
			versionConfig.setVersionType(versionType);
			versionConfig.setVersionDesc(versionDesc);
			versionService.add(versionConfig);
		}else{
			mav = new ModelAndView("appVersion/add");
		}
		return mav;
	}
}
