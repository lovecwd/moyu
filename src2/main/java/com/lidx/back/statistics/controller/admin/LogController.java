package com.lidx.back.statistics.controller.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.lidx.back.statistics.commons.PageInfo;
import com.lidx.back.statistics.entity.Log;
import com.lidx.back.statistics.service.IlogService;
import com.lidx.back.statistics.utils.StringUtils;

@Controller
@RequestMapping(value="/admin/log")
public class LogController {
	
	@Autowired
	private IlogService logService;
	
	@RequestMapping(value="list",method=RequestMethod.GET)
	public String list(Model model){
		return "log/list";
	}
	
	@RequestMapping(value="list",method=RequestMethod.POST)
	public String listGo(Model model,@RequestParam(required=false) String from,
			@RequestParam(required=false) String to,
			@RequestParam(required=false,defaultValue="10") int pageSize,
			@RequestParam(required=false,defaultValue="1") int pageNo){
		if(StringUtils.isNotBlank(from) && StringUtils.isNotBlank(to)){
			model.addAttribute("from", from);
			model.addAttribute("to", to);
		}
		PageInfo pageInfo = logService.findLogPage(pageSize,pageNo,from, to);
		//List<Log> logs = logService.findLogList(from, to);
		model.addAttribute("pageInfo", pageInfo);
		return "log/list";
	}
}
