package com.lidx.back.statistics.controller;

import com.lidx.back.statistics.commons.Constants;
import com.lidx.back.statistics.commons.config.Global;
import com.lidx.back.statistics.utils.FileUtils;
import com.lidx.back.statistics.utils.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

/**
 * Created by chen on 2017/2/18.
 */
@Controller
public class FileController {

    @RequestMapping(value = "download/{fileName:.+}/{uploadDate}",method = RequestMethod.GET)
    public void downloadTeamResource(@PathVariable String fileName,
                                     @PathVariable String uploadDate,
                                             HttpServletRequest request,
                                             HttpServletResponse response){
        if(StringUtils.isNotBlank(fileName)){
            try {
                fileName = new String(fileName.getBytes("ISO-8859-1"),"utf-8");
                String filePath = Global.getConfig("nginx.root")
                        + Constants.FILE_UPLOAD_PATH + "/"+fileName;
                File downloadFile = new File(filePath);
                if(!downloadFile.exists()){
                    throw new RuntimeException("下载文件不存在");
                }
                FileUtils.downFile(new File(filePath),request,response);
            }catch (UnsupportedEncodingException ue){
                ue.printStackTrace();
            } catch (RuntimeException e) {
                e.printStackTrace();
            }

        }
    }
}
