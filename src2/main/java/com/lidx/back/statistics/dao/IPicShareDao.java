package com.lidx.back.statistics.dao;

import java.util.Map;

import com.lidx.back.statistics.entity.PictureShare;

public interface IPicShareDao {

	void insert(PictureShare ps);
	
	Map<String, Integer> getSharePicChannel(String versionType);
}
