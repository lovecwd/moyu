package com.lidx.back.statistics.dao.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.lidx.back.statistics.dao.IFilterRecordDao;
import com.lidx.back.statistics.entity.FilterRecord;
@Repository
public class FilterRecordDaoImpl implements IFilterRecordDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public Session getSession(){
		return sessionFactory.getCurrentSession();
	}
	
	public void insertRecord(FilterRecord record) {
		getSession().save(record);
		getSession().flush();
	}

	@SuppressWarnings("unchecked")
	public List<FilterRecord> getRecords() {
		return getSession().createQuery("from FilterRecord").list();
	}

	public List<Object[]> getRecordsWithParams(String action,
			String versionType,String versionNumber, String from, String to,String online,String order) {
		StringBuffer sb = new StringBuffer();
		sb.append("select f.filterName,fr.versionType,fr.versionNum,count(1) as totalNum,f.id from FilterRecord fr,Filter f where fr.filterId = f.id");
		if(StringUtils.isNoneBlank(action)){
			sb.append(" and fr.action = '").append(action).append("'");
		}
		if(StringUtils.isNotBlank(versionType)){
			sb.append(" and fr.versionType = '").append(versionType).append("'");
		}
		if(StringUtils.isNotBlank(versionNumber)){
			sb.append(" and fr.versionNum = '").append(versionNumber).append("'");
		}
		if (StringUtils.isNotBlank(online)){
			sb.append(" and fr.online = '").append(online).append("'");
		}
		if(StringUtils.isNotBlank(from) && StringUtils.isNotBlank(to)){
			sb.append(" and fr.recordTime between '").append(from).append("' and '").append(to).append("'");
		}
		sb.append(" group by f.id");
        if(StringUtils.isNotBlank(order)){
			sb.append(" order by totalNum " + order);
		}
		Query query = getSession().createQuery(sb.toString());
		return query.list();
	}

}
