package com.lidx.back.statistics.dao;

import java.util.List;

import com.lidx.back.statistics.entity.ClientInfo;

public interface IClientDao {
	void updateCloseSoundNum(ClientInfo clientInfo);
	
	void updateOpenFlashLightNum(ClientInfo clientInfo);
	
	void updateSwitchCameraNum(ClientInfo clientInfo); 
	
	void updateSwitchFilterNum(ClientInfo clientInfo);
	
	void updateRecordVideoTime(ClientInfo clientInfo);
	
	void updateLinkShareNum(ClientInfo clientInfo);
	
	void updateSaveVideoNum(ClientInfo clientInfo);
	
	void updateSavePictureNum(ClientInfo clientInfo);
	
	void updateShareVideoNum(ClientInfo clientInfo);
	
	void updateSharePicNum(ClientInfo clientInfo);
	
	List<ClientInfo> getClientInfos(String versionType);
}
