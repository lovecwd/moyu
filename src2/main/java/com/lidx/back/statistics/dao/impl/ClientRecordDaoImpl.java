package com.lidx.back.statistics.dao.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.FlushMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.lidx.back.statistics.dao.IClientRecordDao;
import com.lidx.back.statistics.entity.ClientRecord;

@Repository
public class ClientRecordDaoImpl implements IClientRecordDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public Session getSession(){
		return sessionFactory.getCurrentSession();
	}
	
	public void insertClientRecord(ClientRecord record) {
		getSession().save(record);
		getSession().flush();
	}

	public List<ClientRecord> getRecords() {
		return getSession().createQuery("from ClientRecord").list();
	}

	public List<Object[]> getRecordsWithParams(String action,
			String versionNumber,String versionType, String from, String to,String online) {
		StringBuffer sb = new StringBuffer();
		sb.append("select cr.action,cr.versionType,cr.versionNum,count(1) from ClientRecord cr where 1=1");
		if(StringUtils.isNotBlank(action)){
			sb.append(" and cr.action = '").append(action).append("'");
		}
		if(StringUtils.isNotBlank(versionType)){
			sb.append(" and cr.versionType = '").append(versionType).append("'");
		}
		if(StringUtils.isNotBlank(versionNumber)){
			sb.append(" and cr.versionNum = '").append(versionNumber).append("'");
		}
		if(StringUtils.isNotBlank(online)){
			sb.append(" and cr.online= '").append(online).append("'");
		}
		if(StringUtils.isNotBlank(from) && StringUtils.isNotBlank(to)){
			sb.append(" and cr.recordTime between '").append(from).append("' and '").append(to).append("'");
		}
		Query query = getSession().createQuery(sb.toString());
		return query.list();
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getSexPercentage(String versionType,
			String versionNumber, String from, String to,String online) {
		StringBuffer sb = new StringBuffer("select case sex when '0' then '女' when '1' then '男' "
				+ "end,versionType,versionNum, count(1) from client_info where 1 = 1");
		if(StringUtils.isNotBlank(versionType)){
			sb.append(" and versionType = '").append(versionType).append("'");
		}
		if(StringUtils.isNotBlank(versionNumber)){
			sb.append(" and versionNum = '").append(versionNumber).append("'");
		}
		if(StringUtils.isNotBlank(online)){
			sb.append(" and online = '").append(online).append("'");
		}
		if(StringUtils.isNotBlank(from) && StringUtils.isNotBlank(to)){
			sb.append(" and recordTime between '").append(from).append("' and '").append(to).append("'");
		}
		sb.append(" GROUP BY sex");
		return (List<Object[]>) getSession().createSQLQuery(sb.toString()).list();
	}

	public Object[] getVideoRecordInfo(String versionType,
			String versionNumber, String from, String to,String online) {
		StringBuffer sb = new StringBuffer("select count(1),sum(recordVideoTime),versionType,"
				+ "versionNum from client_record where action = 'recordVideo'");
		if(StringUtils.isNotBlank(versionType)){
			sb.append(" and versionType = '").append(versionType).append("'");
		}
		if(StringUtils.isNotBlank(versionNumber)){
			sb.append(" and versionNum = '").append(versionNumber).append("'");
		}
		if(StringUtils.isNotBlank(online)){
			sb.append(" and online= '").append(online).append("'");
		}
		if(StringUtils.isNotBlank(from) && StringUtils.isNotBlank(to)){
			sb.append(" and recordTime between '").append(from).append("' and '").append(to).append("'");
		}
		return (Object[]) getSession().createSQLQuery(sb.toString()).list().get(0);
	}

}
