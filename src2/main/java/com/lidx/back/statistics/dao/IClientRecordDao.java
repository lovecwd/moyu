package com.lidx.back.statistics.dao;

import java.util.List;

import com.lidx.back.statistics.entity.ClientRecord;

public interface IClientRecordDao {

	void insertClientRecord(ClientRecord record);
	
	List<ClientRecord> getRecords();

	List<Object[]> getRecordsWithParams(String action, String versionType,
			String versionNumber,String from, String to,String online);

	List<Object[]> getSexPercentage(String versionType, String versionNumber,String from,String to,String online);

	Object[] getVideoRecordInfo(String versionType, String versionNumber,String from,String to,String online);
}
