package com.lidx.back.statistics.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.lidx.back.statistics.dao.IAppInitDao;
import com.lidx.back.statistics.entity.ClientInfo;
@Repository
public class AppInitDaoImpl implements IAppInitDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public void insertClientInfo(ClientInfo info) throws Exception {
		try {
			Session session = sessionFactory.getCurrentSession();
			String insertSql = "insert into client_info(device_id,versionType,versionNum, recordTime) "
					+ "values(?,?,?,now())";
			Query query = session.createSQLQuery(insertSql)
					.setString(0, info.getDeviceId())
					.setString(1, info.getVersionType())
					.setString(2, info.getVersionNum());

					//.setDate(4, info.getRecordTime());
			query.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception("数据插入异常");
		}
		
	}

	public ClientInfo getClientInfo(String deviceId) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from ClientInfo where deviceId = :deviceId").setString("deviceId", deviceId);
		List list = query.setCacheable(true).setCacheRegion("clientInfo").list();
		if(list.size()>0)
			return (ClientInfo) list.get(0);
		return null;
	}

	public ClientInfo getClientInfo(ClientInfo info) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from ClientInfo where deviceId = :deviceId")
				.setString("deviceId", info.getDeviceId());
		List<ClientInfo> list = query.list();
		if(list.size()>0)
			return list.get(0);
		return null;
	}

}
