package com.lidx.back.statistics.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.lidx.back.statistics.dao.IClientDao;
import com.lidx.back.statistics.entity.ClientInfo;
@Repository
public class ClientDaoImpl implements IClientDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public Session getSession(){
		return sessionFactory.getCurrentSession();
	}
	
	public void updateCloseSoundNum(ClientInfo clientInfo) {
		Query query = getSession().createQuery("update ClientInfo set closeSoundNum = closeSoundNum + 1 where deviceId = :deviceId")
		.setString("deviceId", clientInfo.getDeviceId());
		query.executeUpdate();
	}

	public void updateOpenFlashLightNum(ClientInfo clientInfo) {
		Query query = getSession().createQuery("update ClientInfo set openFlashLightNum = openFlashLightNum + 1 where deviceId = :deviceId")
				.setString("deviceId", clientInfo.getDeviceId());
		query.executeUpdate();
	}

	public void updateSwitchCameraNum(ClientInfo clientInfo) {
		Query query = getSession().createQuery("update ClientInfo set switchCameraNum = switchCameraNum + 1 where deviceId = :deviceId")
				.setString("deviceId", clientInfo.getDeviceId());
		query.executeUpdate();
	}

	public void updateSwitchFilterNum(ClientInfo clientInfo) {
		Query query = getSession().createQuery("update ClientInfo set switchFilterNum = switchFilterNum + 1 where deviceId = :deviceId")
				.setString("deviceId", clientInfo.getDeviceId());
		query.executeUpdate();
	}

	public void updateRecordVideoTime(ClientInfo clientInfo) {
		/*StringBuffer sb = new StringBuffer();
		sb.append("update ClientInfo set recordVideoCounts = recordVideoCounts + 1,recordVideoTime = recordVideoTime + :time")
		.append(" where deviceId = :deviceId");
 		Query query = getSession().createQuery(sb.toString())
 				.setLong("time", clientInfo.getRecordVideoTime())
				.setString("deviceId", clientInfo.getDeviceId());
		query.executeUpdate();*/
	}

	public void updateLinkShareNum(ClientInfo clientInfo) {
		Query query = getSession().createQuery("update ClientInfo set linkShareNum = linkShareNum + 1 where deviceId = :deviceId")
				.setString("deviceId", clientInfo.getDeviceId());
		query.executeUpdate();
	}

	public void updateSaveVideoNum(ClientInfo clientInfo) {
		Query query = getSession().createQuery("update ClientInfo set saveVideoNum = saveVideoNum + 1 where deviceId = :deviceId")
				.setString("deviceId", clientInfo.getDeviceId());
		query.executeUpdate();
	}

	public void updateSavePictureNum(ClientInfo clientInfo) {
		Query query = getSession().createQuery("update ClientInfo set savePictureNum = savePictureNum + 1 where deviceId = :deviceId")
				.setString("deviceId", clientInfo.getDeviceId());
		query.executeUpdate();
	}

	public void updateShareVideoNum(ClientInfo clientInfo) {
		Query query = getSession().createQuery("update ClientInfo set shareVideoNum = shareVideoNum + 1 where deviceId = :deviceId")
				.setString("deviceId", clientInfo.getDeviceId());
		query.executeUpdate();
	}

	public void updateSharePicNum(ClientInfo clientInfo) {
		Query query = getSession().createQuery("update ClientInfo set sharePictureNum = sharePictureNum + 1 where deviceId = :deviceId")
				.setString("deviceId", clientInfo.getDeviceId());
		query.executeUpdate();
	}

	public List<ClientInfo> getClientInfos(String versionType) {
		Query query = getSession().createQuery("from ClientInfo where versionType = :versionType")
				.setString("versionType", versionType);
		return query.list();
	}

}
