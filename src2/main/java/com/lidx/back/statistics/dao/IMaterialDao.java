package com.lidx.back.statistics.dao;

import com.lidx.back.statistics.entity.MaterialPO;
import com.lidx.back.statistics.entity.MaterialUpdateRequestInfo;

import java.util.List;

public interface IMaterialDao {

	List<MaterialPO> getAllMaterialPOs(String versionType, String versionNumber);

    //List<MaterialPO> getRecommendMaterialPOs(String versionType, String versionNumber,String sex);

	void updateRequest(MaterialUpdateRequestInfo info);

	MaterialUpdateRequestInfo getRequestInfo();
	
	void deleteMaterials(String versionType, String versionNumber);
	
	void saveMaterial(MaterialPO materialPO);
	
	List<String> getMaterialNames(String versionType, String versionNumber);

	List<MaterialPO> getMaterialPOsBySex(String versionType,
			String versionNumber, String sex);

	void deleteMaterial(int id);
	
	void clearMaterialCacheRegion();

	List<MaterialPO> getMaterials(MaterialPO mp);

	void updateRecommendOrder(int size,String versionType,String versionNumber);
	
	void updateCategoryOrder(int size,String versionType,String versionNumber,String category);

	void saveMaterials(List<MaterialPO> ms);

	MaterialPO getMaterialById(int id);

	void updateMaterial(MaterialPO materialPO);

	void updateRecommendOrder(int i, String versionType, String versionNumber,
			int recommendOrder);

	void updateCategoryOrder(int i, String versionType, String versionNumber,
			String category, int categoryOrder);

	/*
	拿到每个素材顺序小于等于10的素材
	 */
	List<MaterialPO> getMaterialsFrontXCategory(int size);

    void deleteMaterialsByIds(String s);

    List<String> getMaterialUuids();
}
