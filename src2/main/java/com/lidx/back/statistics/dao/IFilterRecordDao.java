package com.lidx.back.statistics.dao;

import java.util.List;

import com.lidx.back.statistics.entity.FilterRecord;

public interface IFilterRecordDao {

	void insertRecord(FilterRecord record);
	
	List<FilterRecord> getRecords();

	List<Object[]> getRecordsWithParams(String action, String versionType,
			String versionNumber,String from, String to,String online,String order);
}
