package com.lidx.back.statistics.dao.impl;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.lidx.back.statistics.dao.ISysUserDao;
import com.lidx.back.statistics.entity.Role;
import com.lidx.back.statistics.entity.SysUser;
@Repository(value="userDao")
public class SysUserDaoImpl implements ISysUserDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public Session getSession(){
		return sessionFactory.getCurrentSession();
	}
	
	public int saveSysUser(SysUser user) {
		
		int userId = (Integer) getSession().save(user);
		/*for(int roleId : user.getRoleIdList()){
			getSession().createSQLQuery("insert into sys_user_role values (?,?)").setInteger(0, userId)
			.setInteger(1, roleId)
			.executeUpdate();
		}*/
		return userId;
	}

	public List<Role> getRoles() {
		return getSession().createQuery("from Role").list();
	}

	public List<SysUser> getUserList() {
		return getSession().createQuery("from SysUser").list();
	}

	public SysUser getUserByLoginName(String loginName) {
		List list =getSession().createQuery("from SysUser where loginName = :loginName")
				.setString("loginName", loginName)
				.list();
		if(list.size()>0 && list.get(0) != null){
			return (SysUser) list.get(0);
		}
		return null;
	}

	public SysUser getUserById(Integer id) {
		List list = getSession().createQuery("from SysUser where id = :id").setInteger("id", id).list();
		if(list.size()>0){
			return (SysUser) list.get(0);
		}
		return null;
	}

	public List<Role> getUserRoleList(String loginName) {
		List list = getSession().createSQLQuery("select distinct sr.id from sys_user u,sys_role sr,sys_user_role sur "
				+ "where u.id = sur.user_id and sur.role_id = sr.id and u.loginName = :loginName")
				.setString("loginName", loginName).list();
		if(list.size()>0){
			List<Role> roles = Lists.newArrayList();
			for(int i = 0;i<list.size();i++){
				Role role = new Role();
				role.setId(Integer.parseInt(list.get(i).toString()));
				roles.add(role);
			}
			return roles;
		}
		return null;
	}

	public int updateLoginInfo(String host, Date date, int userId) {
		// TODO Auto-generated method stub
		return getSession().createQuery("update SysUser set loginDate=:p1,loginIp=:p2 where id = :p3")
				.setDate("p1", date)
				.setString("p2", host)
				.setInteger("p3", userId)
				.executeUpdate();
	}

	public void deleteUser(int userId) {
		SysUser user = (SysUser) getSession().load(SysUser.class, userId);
		getSession().delete(user);
	}

	@Override
	public List<String> findUserNames() {
		return getSession().createSQLQuery("select DISTINCT NAME from sys_user").list();
	}

	@Override
	public void updateSysUser(SysUser user) {
		getSession().update(user);
	}

	@Override
	public SysUser getUserByName(String name) {
		return (SysUser) getSession().createQuery("from SysUser where name= :name")
				.setString("name",name).list().get(0);
	}

}
