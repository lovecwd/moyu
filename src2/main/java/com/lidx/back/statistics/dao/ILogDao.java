package com.lidx.back.statistics.dao;

import java.util.List;

import com.lidx.back.statistics.commons.PageInfo;
import com.lidx.back.statistics.entity.Log;

public interface ILogDao {
	void saveLog(Log log);
	
	List<Log> getLogList(String from,String to);

	PageInfo findLogPage(int pageSize, int pageNo, String from, String to);
}
