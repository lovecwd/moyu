package com.lidx.back.statistics.dao;

import com.lidx.back.statistics.entity.MaterialLatest;

import java.util.List;

/**
 * Created by Administrator on 2017/4/11.
 */
public interface ILatestMaterialDao {
    List<MaterialLatest> getLatestMaterials();

    void saveLatestMaterial(MaterialLatest materialLatest);

    void deleteLatestMaterial(int id);

    void deleteAll();

    void saveLatestMaterialList(List<MaterialLatest> materialLatests);
}
