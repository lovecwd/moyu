package com.lidx.back.statistics.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.lidx.back.statistics.dao.IFilterDao;
import com.lidx.back.statistics.entity.Filter;
@Repository
public class FilterDaoImpl implements IFilterDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public Session getSession(){
		return sessionFactory.getCurrentSession();
	}
	
	public void updateFilterUseNum(Filter filter) {
		Query query = getSession().createQuery("update Filter set useNum = useNum + 1 where id = :id")
				.setInteger("id", filter.getId());
		query.executeUpdate();
	}

	public void updateFilterMakeNum(Filter filter) {
		Query query = getSession().createQuery("update Filter set makeNum = makeNum + 1 where id = :id")
				.setInteger("id", filter.getId());
		query.executeUpdate();
	}

	public List<Filter> getFilters(String versionType) {
		Query query = getSession().createQuery("from Filter");
		return query.list();
	}

	public void addFilter(Filter filter) {
		getSession().save(filter);
	}

}
