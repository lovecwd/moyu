package com.lidx.back.statistics.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.lidx.back.statistics.commons.Constants;
import com.lidx.back.statistics.dao.IMaterialCountDao;
import com.lidx.back.statistics.entity.MaterialCount;
import com.lidx.back.statistics.entity.MaterialDownload;
import com.lidx.back.statistics.entity.MaterialMakeVideo;
@Repository
public class MaterialCountDaoImpl implements IMaterialCountDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public Session getSession(){
		return sessionFactory.getCurrentSession();
	}
	
	public void updateMaterialDownloadCount(MaterialCount count) {
		StringBuffer sql = new StringBuffer("update MaterialCount m set m.downloadNum = m.downloadNum + 1")
		.append(" where m.uuid = ? and m.versionType = ? and m.versionNum = ?");
		Query query = getSession().createQuery(sql.toString()).setString(0, count.getUuid())
				.setString(1, count.getVersionType())
				.setString(2, count.getVersionNum());
		query.executeUpdate();
	}

	public void updateMakeVideoNum(MaterialCount count) {
		StringBuffer sql = new StringBuffer("update MaterialCount m set m.makeVideoNum = m.makeVideoNum + 1")
		.append(" where m.uuid = ? and m.versionType = ? and m.versionNum = ?");
		Query query = getSession().createQuery(sql.toString()).setString(0, count.getUuid())
				.setString(1, count.getVersionType())
				.setString(2, count.getVersionNum());
		query.executeUpdate();
	}

	public void updatePhotographNum(MaterialCount count) {
		StringBuffer sql = new StringBuffer("update MaterialCount m set m.photographNum = m.photographNum + 1")
		.append(" where m.uuid = ? and m.versionType = ? and m.versionNum = ?");
		Query query = getSession().createQuery(sql.toString()).setString(0, count.getUuid())
				.setString(1, count.getVersionType())
				.setString(2, count.getVersionNum());
		query.executeUpdate();
	}

	public void updateVideoShareNum(MaterialCount count) {
		StringBuffer sql = new StringBuffer("update MaterialCount m set m.videoShareNum = m.videoShareNum + 1")
		.append(" where m.uuid = ? and m.versionType = ? and m.versionNum = ?");
		Query query = getSession().createQuery(sql.toString()).setString(0, count.getUuid())
				.setString(1, count.getVersionType())
				.setString(2, count.getVersionNum());
		query.executeUpdate();
	}

	public void updatePicShareNum(MaterialCount count) {
		StringBuffer sql = new StringBuffer("update MaterialCount m set m.picShareNum = m.picShareNum + 1")
		.append(" where m.uuid = ? and m.versionType = ? and m.versionNum = ?");
		Query query = getSession().createQuery(sql.toString()).setString(0, count.getUuid())
				.setString(1, count.getVersionType())
				.setString(2, count.getVersionNum());
		query.executeUpdate();
	}

	public List<MaterialCount> getCounts(String versionType) {
		Query query = getSession().createQuery("from MaterialCount where versionType = :versionType")
				.setString("versionType", versionType);
		return query.list();
	}

	public void saveMaterialDownload(MaterialDownload materialDownload) {
		getSession().save(materialDownload);
	}

	public void insertMaterialMakeVideo(MaterialMakeVideo makeVideo) {
		getSession().save(makeVideo);
	}

}
