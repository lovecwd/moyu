package com.lidx.back.statistics.dao;

import com.lidx.back.statistics.entity.ClientInfo;

public interface IAppInitDao {

	void insertClientInfo(ClientInfo info) throws Exception;
	
	ClientInfo getClientInfo(String deviceId);
	
	ClientInfo getClientInfo(ClientInfo info);
}
