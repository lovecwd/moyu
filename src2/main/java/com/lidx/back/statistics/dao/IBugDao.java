package com.lidx.back.statistics.dao;

import com.lidx.back.statistics.commons.PageInfo;
import com.lidx.back.statistics.entity.Bug;

import java.util.List;

/**
 * Created by chen on 2017/2/18.
 */
public interface IBugDao {

    void insertBug(Bug bug);

    List<Bug> findBugs();

    Bug getBugById(int id);

    boolean updateBug(Bug bug);

    PageInfo findBugsPage(int pageNo, int pageSize);

    void deleteBug(Bug bug);
}
