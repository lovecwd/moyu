package com.lidx.back.statistics.dao;

import com.lidx.back.statistics.commons.PageInfo;
import com.lidx.back.statistics.entity.TeamResource;

import java.util.List;

/**
 * Created by chen on 2017/2/17.
 */
public interface ITeamResourceDao {

    void insertTeamResource(TeamResource teamResource);

    List<TeamResource> findTeamResourceList();

    TeamResource getTeamResourceById(int id);

    void deleteTeamResourceById(int id);

    void updateTeamResource(TeamResource teamResource);

    PageInfo findTeamResourcePage(int pageNo, int pageSize,String resourceCategory);
}
