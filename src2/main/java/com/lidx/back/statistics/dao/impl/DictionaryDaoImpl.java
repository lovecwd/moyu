package com.lidx.back.statistics.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.lidx.back.statistics.dao.IDictionaryDao;
import com.lidx.back.statistics.entity.Dictionary;
@Repository
public class DictionaryDaoImpl implements IDictionaryDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public String getValueByCode(String code) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Dictionary where code = :code")
				.setCacheable(true)
				.setString("code", code);
		List list = query.list();
		Dictionary dictionary = (Dictionary) list.get(0);
		return dictionary.getValue();
	}

	public String getCodeByValue(String value) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Dictionary where value = :value")
				.setCacheable(true)
				.setCacheRegion("dictionary")
				.setString("value", value);
		List list = query.list();
		Dictionary dictionary = (Dictionary) list.get(0);
		return dictionary.getCode();
	}

}
