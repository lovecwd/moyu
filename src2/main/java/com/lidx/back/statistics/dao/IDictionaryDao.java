package com.lidx.back.statistics.dao;

public interface IDictionaryDao {

	String getValueByCode(String code);
	
	String getCodeByValue(String value);
}
