package com.lidx.back.statistics.dao;

import java.util.List;

import com.lidx.back.statistics.commons.PageInfo;
import com.lidx.back.statistics.entity.FeedBack;

public interface IFeedbackDao {

	void insertFeedback(FeedBack feedBack) throws Exception;

	List<FeedBack> getFeedBacks();

	PageInfo queryFeedbackPage(int pageNo, int pageSize);
	
	Long queryFeedbackPageCount();
}
