package com.lidx.back.statistics.dao;

import java.util.List;

import com.lidx.back.statistics.entity.MaterialRecord;

public interface IMaterialRecordDao {
	void insertRecord(MaterialRecord record);
	List<MaterialRecord> getRecords();
	List getRecordsWithParams(String action, String versionType,String versionNumber, String from,
							  String to,String online,String order);
	List<Object[]> getRecordsWithChannel(String action, String versionType,
			String versionNumber,String from, String to,String online);
	List<Object[]> getRecordsWithMaterialChannel(String action,
			String versionType, String versionNumber, String from, String to,String online);
}
