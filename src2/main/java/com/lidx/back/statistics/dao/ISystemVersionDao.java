package com.lidx.back.statistics.dao;

import java.util.List;

import com.lidx.back.statistics.entity.SystemVersionConfig;
import com.lidx.back.statistics.rest.VersionBean;

public interface ISystemVersionDao {

	SystemVersionConfig getConfig(VersionBean versionBean);
	
	String getLatestVersionNum(String versionType);

	List<SystemVersionConfig> getVersionList(VersionBean versionBean);

	void save(SystemVersionConfig versionConfig);

	List<String> getVersionNumbers();
}
