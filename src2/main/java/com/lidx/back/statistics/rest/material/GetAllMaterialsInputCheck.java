package com.lidx.back.statistics.rest.material;

import com.lidx.back.statistics.rest.RestRequest;
import org.springframework.util.StringUtils;

import com.lidx.back.statistics.rest.VersionBean;

public class GetAllMaterialsInputCheck {
	
	public void checkInput(RestRequest input) throws Exception{
		String deviceId = input.getDeviceId();
		if(StringUtils.isEmpty(deviceId)){
			throw new RuntimeException("deviceId不能为空");
		}
        if (StringUtils.isEmpty(input.getParameters().getCategoryId()) || Integer.parseInt(input.getParameters().getCategoryId()) <= 0) {
            throw new RuntimeException("categoryId不能为空且不能小于等于0");
        }
        VersionBean versionBean = input.getVersion();
        if(versionBean == null){
			throw new Exception("版本信息不能为空");
		}
		//String versionNum = versionBean.getVersionNumber();
		String versionType = versionBean.getVersionType();
		if(StringUtils.isEmpty(versionType))
			throw new Exception("版本类型不能为空");
	}
}
