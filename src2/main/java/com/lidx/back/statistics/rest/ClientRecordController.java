package com.lidx.back.statistics.rest;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lidx.back.statistics.commons.ClientAction;
import com.lidx.back.statistics.entity.ClientRecord;
import com.lidx.back.statistics.rest.client.ClientRequest;
import com.lidx.back.statistics.rest.client.ClientRequestWithRecordTime;
import com.lidx.back.statistics.rest.client.ClientRequestWithShareChannel;
import com.lidx.back.statistics.service.IClientRecordService;
import com.lidx.back.statistics.service.IClientService;
import com.lidx.back.statistics.service.IPicShareService;

@RestController
@RequestMapping(value="client")
public class ClientRecordController {

	@Autowired
	private IClientService clientService;
	@Autowired
	private IPicShareService picShareService;
	@Autowired
	private IClientRecordService recordService;;
	
	@RequestMapping(value="closeSound",method=RequestMethod.POST)
	public void closeSound(@RequestBody ClientRequest request){
		
		ClientRecord record = checkInput(request);
		if(null != record){
			record.setAction(ClientAction.CLIENT_CLOSE_SOUND);
			recordService.saveClientRecord(record);
		}
	}
	
	@RequestMapping(value="openFlashLight",method=RequestMethod.POST)
	public void openFlashLight(@RequestBody ClientRequest request){
		ClientRecord record = checkInput(request);
		if(null != record){
			record.setAction(ClientAction.CLIENT_OPEN_FLASH_LIGHT);
			recordService.saveClientRecord(record);
		}
	}
	
	@RequestMapping(value="switchCamera",method=RequestMethod.POST)
	public void switchCamera (@RequestBody ClientRequest request){
		ClientRecord record = checkInput(request);
		if(null != record){
			record.setAction(ClientAction.CLIENT_SWITCH_CAMERA);
			recordService.saveClientRecord(record);
		}
	}
	
	@RequestMapping(value="switchFilter",method=RequestMethod.POST)
	public void switchFilter (@RequestBody ClientRequest request){
		ClientRecord record = checkInput(request);
		if(null != record){
			record.setAction(ClientAction.CLIENT_SWITCH_FILTER);
			recordService.saveClientRecord(record);
		}
	}
	/**
	 * 录制视频
	 * @param request
	 */
	@RequestMapping(value="recordVideo",method=RequestMethod.POST)
	public void recordVideo (@RequestBody ClientRequestWithRecordTime request){
		ClientRecord record = null;
		if(null != request){
			VersionBean version = request.getVersion();
			String deviceId = request.getDeviceId();
			String online = request.getOnline();
			String versionNum = version.getVersionNumber();
			String versionType = version.getVersionType();
			Long time = Long.valueOf(request.getRecordTime());
			if(!StringUtils.isEmpty(deviceId) && !StringUtils.isEmpty(versionType) 
					&& !StringUtils.isEmpty(versionNum)
					&& !StringUtils.isEmpty(time)
					&& !StringUtils.isEmpty(online)){
				record = new ClientRecord();
				record.setDeviceId(deviceId);
				record.setAction(ClientAction.CLIENT_RECORD_VIDEO);
				record.setVersionNum(versionNum);
				record.setVersionType(versionType);
				record.setRecordVideoTime(time);
				record.setOnline(online);
				recordService.saveClientRecord(record);
			}
		}
				
				
	}
	
	/**
	 * 用户分享推荐链接数统计
	 * @param request
	 */
	@RequestMapping(value="linkShare",method=RequestMethod.POST)
	public void linkShare (@RequestBody ClientRequest request){
		ClientRecord record = checkInput(request);
		if(null != record){
			record.setAction(ClientAction.CLIENT_LINK_SHARE);
			recordService.saveClientRecord(record);
		}
	}
	
	/**
	 * 用户分享推荐链接数统计
	 * @param request
	 */
	@RequestMapping(value="linkOpenCount",method=RequestMethod.POST)
	public void linkOpenCount (@RequestBody ClientRequest request,HttpServletResponse response){
        response.setHeader("Access-Control-Allow-Origin", "*");
        ClientRecord record = checkInput(request);
        if(record == null){
        	record = new ClientRecord();
        }
		/*record.setVersionNum("");
		record.setVersionType("");*/
		record.setAction(ClientAction.CLIENT_LINK_OPEN);
		recordService.saveClientRecord(record);
	}
	
	/**
	 * 用户保存视频到本地的次数
	 * @param request
	 */
	@RequestMapping(value="saveVideo",method=RequestMethod.POST)
	public void saveVideo (@RequestBody ClientRequest request){
		ClientRecord record = checkInput(request);
		if(null != record){
			record.setAction(ClientAction.CLIENT_SAVE_VIDEO);
			recordService.saveClientRecord(record);
		}
	}
	
	@RequestMapping(value="savePicture",method=RequestMethod.POST)
	public void savePicture (@RequestBody ClientRequest request){
		ClientRecord record = checkInput(request);
		if(null != record){
			record.setAction(ClientAction.CLIENT_SAVE_PIC);
			recordService.saveClientRecord(record);
		}
	}
	
	@RequestMapping(value="shareVideo",method=RequestMethod.POST)
	public void shareVideo (@RequestBody ClientRequest request){
		ClientRecord record = checkInput(request);
		if(null != record){
			record.setAction(ClientAction.CLIENT_SHARE_VIDEO);
			recordService.saveClientRecord(record);
		}
	}
	
	@RequestMapping(value="sharePicture",method=RequestMethod.POST)
	public void sharePicture (@RequestBody ClientRequestWithShareChannel request){
		if(!StringUtils.isEmpty(request)){
			String deviceId = request.getDeviceId();
			String channel = request.getChannel();
			VersionBean vbBean = request.getVersion();
			String versionType = vbBean.getVersionType();
			String versionNum = vbBean.getVersionNumber();
			String online = request.getOnline();
			if(!StringUtils.isEmpty(deviceId) && !StringUtils.isEmpty(channel)
					&& !StringUtils.isEmpty(versionType) && !StringUtils.isEmpty(versionNum)
					&& !StringUtils.isEmpty(online)){
				ClientRecord record = new ClientRecord();
				record.setShareChannel(channel);
				record.setDeviceId(deviceId);
				record.setVersionNum(versionNum);
				record.setVersionType(versionType);
				record.setOnline(online);
				record.setAction(ClientAction.CLIENT_SHARE_PIC);
				recordService.saveClientRecord(record);
			}
		}
		
	}
	
	private ClientRecord checkInput(ClientRequest request){
		ClientRecord record = null;
		if(null != request){
			VersionBean version = request.getVersion();
			String deviceId = request.getDeviceId();
			String versionNum = version.getVersionNumber();
			String versionType = version.getVersionType();
			String online = request.getOnline();
			if(!StringUtils.isEmpty(deviceId) && !StringUtils.isEmpty(versionType) && !StringUtils.isEmpty(versionNum)
					&& !StringUtils.isEmpty(online)){
				record = new ClientRecord();
				record.setDeviceId(deviceId);
				record.setVersionNum(versionNum);
				record.setVersionType(versionType);
				record.setOnline(online);
			}
		}
		return record;
	}

}
