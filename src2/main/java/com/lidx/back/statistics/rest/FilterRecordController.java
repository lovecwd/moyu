package com.lidx.back.statistics.rest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lidx.back.statistics.commons.FilterAction;
import com.lidx.back.statistics.entity.FilterRecord;
import com.lidx.back.statistics.service.IFilterRecordService;
/**
 * 
 * @author chenweidong
 *
 */
@RestController
@RequestMapping(value="filter")
public class FilterRecordController {

	@Autowired
	private IFilterRecordService filterService;
	
	@RequestMapping(value="useFilter",method=RequestMethod.POST)
	public void useFilter(@RequestBody FilterRequest request){
		FilterRecord record = checkInput(request);
		if(null != record){
			record.setAction(FilterAction.FILTER_USE);
			filterService.saveRecord(record);
		}
	}
	
	@RequestMapping(value="makeFilter",method=RequestMethod.POST)
	public void makeFilter(@RequestBody FilterRequest request){
		FilterRecord record = checkInput(request);
		if(null != record){
			record.setAction(FilterAction.FILTER_MAKE);
			filterService.saveRecord(record);
		}
	}
	
	private FilterRecord checkInput(FilterRequest request){
		FilterRecord record = null;
		if(null != request){
			VersionBean version = request.getVersion();
			String deviceId = request.getDeviceId();
			String versionNum = version.getVersionNumber();
			String versionType = version.getVersionType();
			String filterId = request.getFilterId();
			String online =request.getOnline();
			if(!StringUtils.isEmpty(deviceId) && !StringUtils.isEmpty(versionType) 
					&& !StringUtils.isEmpty(versionNum)
					&& !StringUtils.isEmpty(filterId)
					&& !StringUtils.isEmpty(online)){
				record = new FilterRecord();
				record.setDeviceId(deviceId);
				record.setVersionNum(versionNum);
				record.setVersionType(versionType);
				record.setFilterId(filterId);
				record.setOnline(online);
			}
		}
		return record;
	}
	
}
