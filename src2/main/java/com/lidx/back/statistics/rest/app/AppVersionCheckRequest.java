package com.lidx.back.statistics.rest.app;

import com.lidx.back.statistics.rest.VersionBean;

public class AppVersionCheckRequest {

	private VersionBean version;

	public VersionBean getVersion() {
		return version;
	}

	public void setVersion(VersionBean version) {
		this.version = version;
	}
	
	
}
