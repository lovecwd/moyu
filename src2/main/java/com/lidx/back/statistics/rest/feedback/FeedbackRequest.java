package com.lidx.back.statistics.rest.feedback;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.lidx.back.statistics.rest.VersionBean;
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FeedbackRequest {

    /*private String qq;
    private String phone;*/
    private String content;
    //private List<String> images;
    private VersionBean version;

    /*public String getQq() {
        return qq;
    }
    public void setQq(String qq) {
        this.qq = qq;
    }
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<String> getImages() {
        return images;
    }
    public void setImages(List<String> images) {
        this.images = images;
    }*/
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public VersionBean getVersion() {
		return version;
	}
	public void setVersion(VersionBean version) {
		this.version = version;
	}
	
}
