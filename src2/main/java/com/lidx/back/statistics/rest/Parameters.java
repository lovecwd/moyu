package com.lidx.back.statistics.rest;

/**
 * Created by Administrator on 2017/4/12.
 */
public class Parameters {
    private String categoryId;
    private String content;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
