package com.lidx.back.statistics.rest.client;

import com.lidx.back.statistics.rest.VersionBean;

public class ClientRequest {
	private String deviceId;
	private String online;
	private VersionBean version;
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public VersionBean getVersion() {
		return version;
	}
	public void setVersion(VersionBean version) {
		this.version = version;
	}

	public String getOnline() {
		return online;
	}

	public void setOnline(String online) {
		this.online = online;
	}
}
