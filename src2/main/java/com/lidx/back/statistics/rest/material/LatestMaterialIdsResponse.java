package com.lidx.back.statistics.rest.material;

/**
 * Created by Administrator on 2017/4/11.
 */
public class LatestMaterialIdsResponse {
    private String rspMsg;
    private LatestMaterialIdsRspBody body;

    public String getRspMsg() {
        return rspMsg;
    }

    public void setRspMsg(String rspMsg) {
        this.rspMsg = rspMsg;
    }

    public LatestMaterialIdsRspBody getBody() {
        return body;
    }

    public void setBody(LatestMaterialIdsRspBody body) {
        this.body = body;
    }
}
