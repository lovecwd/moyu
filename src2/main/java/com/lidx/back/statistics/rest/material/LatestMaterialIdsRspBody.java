package com.lidx.back.statistics.rest.material;

/**
 * Created by Administrator on 2017/4/11.
 */
public class LatestMaterialIdsRspBody {

    private int[] materialIds;

    public int[] getMaterialIds() {
        return materialIds;
    }

    public void setMaterialIds(int[] materialIds) {
        this.materialIds = materialIds;
    }
}
