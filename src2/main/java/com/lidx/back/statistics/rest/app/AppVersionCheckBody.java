package com.lidx.back.statistics.rest.app;

public class AppVersionCheckBody {

	private boolean updateNeeded;

	private String latestVersionNum;

	public String getLatestVersionNum() {
		return latestVersionNum;
	}

	public void setLatestVersionNum(String latestVersionNum) {
		this.latestVersionNum = latestVersionNum;
	}

	public boolean isUpdateNeeded() {

		return updateNeeded;
	}

	public void setUpdateNeeded(boolean updateNeeded) {
		this.updateNeeded = updateNeeded;
	}
}
