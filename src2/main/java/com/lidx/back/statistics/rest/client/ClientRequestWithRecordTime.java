package com.lidx.back.statistics.rest.client;

import com.lidx.back.statistics.rest.VersionBean;

public class ClientRequestWithRecordTime {
	private String deviceId;
	private VersionBean version;
	private String recordTime;
	private String online;
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public VersionBean getVersion() {
		return version;
	}
	public void setVersion(VersionBean version) {
		this.version = version;
	}
	public String getRecordTime() {
		return recordTime;
	}
	public void setRecordTime(String recordTime) {
		this.recordTime = recordTime;
	}

	public String getOnline() {
		return online;
	}

	public void setOnline(String online) {
		this.online = online;
	}
}
