package com.lidx.back.statistics.rest.feedback;

import com.lidx.back.statistics.entity.FeedBack;
import com.lidx.back.statistics.rest.RestRequest;
import com.lidx.back.statistics.rest.VersionBean;
import org.springframework.util.StringUtils;
public class FeedbackInputCheck {
	

	public FeedBack checkInput(RestRequest request) throws Exception{
		String content = request.getParameters().getContent();
        //List<String> images = request.getImages();
        if (StringUtils.isEmpty(content))
            throw new Exception("评论内容不能为空");
		FeedBack feedBack = new FeedBack();
		feedBack.setContent(content);
        /*if(!StringUtils.isEmpty(request.getQq()))
            feedBack.setQq(request.getQq());
		if(!StringUtils.isEmpty(request.getPhone())){
			feedBack.setPhone(request.getPhone());
		}*/
        /*StringBuffer sb;
        File imageFile = new File(Constants.IMAGE_FILE_PATH);
		if(images.size()>0){
			
			if(!imageFile.exists()){
				imageFile.mkdir();
			}
			String[] imageNames =  new String[images.size()] ;
			int num = 0;
			String imgFilePath = "";
			//传进来base64的图片数据
			for(String imageBase64 : images){
				imgFilePath = Global.getConfig("nginx.root") + Constants.FEEDBACKIMAGES_DIRECTORY;
				imageFile = new File(imgFilePath);
				if(!imageFile.exists()){
					imageFile.mkdir();
				}
				imageNames[num] = System.currentTimeMillis()+".jpg";
				byte[] bytes = Base64.decodeBase64(imageBase64);
				imgFilePath = imgFilePath + "\\" + imageNames[num];
				imageFile = new File(imgFilePath);
				FileImageOutputStream fiout = new FileImageOutputStream(imageFile);
				fiout.write(bytes,0,bytes.length);
				fiout.close();
				*//*FileInputStream is = new FileInputStream(imageFile);
                MaterialUpdateConfig config = SpringContextHolder.getBean("materialUpdateConfig");
				FTPUtils.uploadFile(config.getIisFtpHost(), 21, config.getFtpUserName(), 
						config.getFtpPassword(), Constants.IIS_URL_FEEDBACKIMAGES_ROOT,
						imageNames[num], is);*//*
                num++;
			}
			sb = new StringBuffer();
			for(int i=0;i<imageNames.length;i++){
				if(i == imageNames.length-1){
					sb.append(imageNames[i]);
				}else{
					sb.append(imageNames[i]+",");
				}
			}
			feedBack.setImageNames(sb.toString());
		}*/
        VersionBean versionBean = request.getVersion();
		
		if(null != versionBean){
			String versionNumber = versionBean.getVersionNumber();
			String versionType = versionBean.getVersionType();
			if(StringUtils.isEmpty(versionNumber) || StringUtils.isEmpty(versionType)){
				throw new RuntimeException("versionNumber and versionType can't be null!");
			}
			feedBack.setVersionNum(versionNumber);
			feedBack.setVersionType(versionType);
		}
		return feedBack;
	}
	
	/*public static void main(String[] args) throws IOException {
		File file = new File("d://cheninfo//test2.jpg");
		FileImageInputStream fin = new FileImageInputStream(file); 
		byte[] bytes  = new byte[1024];  
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		int numBytesRead = 0;
      while ((numBytesRead = fin.read(bytes)) != -1) {
      output.write(bytes, 0, numBytesRead);
      }
      byte[] data = output.toByteArray();
      output.close();
      fin.close();
		String base64 = Base64.encodeBase64String(output.toByteArray());
		System.out.println(base64);
	}*/
}
