package com.lidx.back.statistics.rest.material;

import com.lidx.back.statistics.rest.VersionBean;


public class MaterialRequest {

	private String deviceId;
	
	private VersionBean version;

	//分类的id
	private Integer categoryId;

	public VersionBean getVersion() {
		return version;
	}

	public void setVersion(VersionBean version) {
		this.version = version;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
}
