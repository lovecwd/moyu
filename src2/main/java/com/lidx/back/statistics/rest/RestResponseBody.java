package com.lidx.back.statistics.rest;

public class RestResponseBody {
	private String detailedMsg;

	public String getDetailedMsg() {
		return detailedMsg;
	}

	public void setDetailedMsg(String detailedMsg) {
		this.detailedMsg = detailedMsg;
	}
}
