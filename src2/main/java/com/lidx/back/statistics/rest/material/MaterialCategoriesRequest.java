package com.lidx.back.statistics.rest.material;

import com.lidx.back.statistics.rest.VersionBean;


public class MaterialCategoriesRequest {

	private String deviceId;
	
	private VersionBean version;

	public VersionBean getVersion() {
		return version;
	}

	public void setVersion(VersionBean version) {
		this.version = version;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
}
