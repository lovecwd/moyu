package com.lidx.back.statistics.rest;

import com.fasterxml.jackson.annotation.JsonInclude;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class RestResponse {
	private String rspMsg;
	private RestResponseBody body;

	public String getRspMsg() {
		return rspMsg;
	}

	public void setRspMsg(String rspMsg) {
		this.rspMsg = rspMsg;
	}

	public RestResponseBody getBody() {
		return body;
	}

	public void setBody(RestResponseBody body) {
		this.body = body;
	}

	
	
}
