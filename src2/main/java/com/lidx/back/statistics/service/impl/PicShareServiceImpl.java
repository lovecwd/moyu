package com.lidx.back.statistics.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lidx.back.statistics.dao.IPicShareDao;
import com.lidx.back.statistics.entity.PictureShare;
import com.lidx.back.statistics.service.IPicShareService;
@Service
public class PicShareServiceImpl implements IPicShareService {

	@Autowired
	private IPicShareDao picShareDao;
	
	public void savePicShare(PictureShare ps) {
		picShareDao.insert(ps);
	}

	public Map<String, Integer> getSharePicChannel(String versionType) {
		return picShareDao.getSharePicChannel(versionType);
	}

}
