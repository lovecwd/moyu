package com.lidx.back.statistics.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lidx.back.statistics.dao.IFilterRecordDao;
import com.lidx.back.statistics.entity.FilterRecord;
import com.lidx.back.statistics.service.IFilterRecordService;
@Service
@Transactional
public class FilterRecordServiceImpl implements IFilterRecordService {

	@Autowired
	private IFilterRecordDao recordDao;
	
	@Transactional(readOnly=false)
	public void saveRecord(FilterRecord record) {
		record.setRecordTime(new Date());
		recordDao.insertRecord(record);
	}

	public List<FilterRecord> getRecords() {
		return recordDao.getRecords();
	}

	public List<Object[]> getRecordsWithParams(String action,
			String versionType, String versionNumber,String from, String to,String online,String order) {
		return recordDao.getRecordsWithParams(action,versionType,versionNumber,from,to,online,order);
	}

}
