package com.lidx.back.statistics.service;

import java.util.List;
import java.util.Map;

import com.lidx.back.statistics.entity.MaterialRecord;
import com.lidx.back.statistics.entity.record.MaterialShareRecord;

public interface IMaterialRecordService {
	void saveRecord(MaterialRecord record);
	List<MaterialRecord> getMaterialRecords();
	
	List getRecordsWithParams(String action, String versionType,String versionNumber, String from,
			String to,String online,String order);
	List<Object[]> getRecordsWithChannel(String action, String versionType,
			String versionNumber,String from, String to,String online);
	List<MaterialShareRecord> getRecordsWithMaterialChannel(String action, String versionType,
			String versionNumber, String from, String to, String online,String order);
	/*Map<String, List<Object[]>> getRecordsWithMaterialChannel(String action, String versionType,
			String versionNumber, String from, String to);*/
	
}
