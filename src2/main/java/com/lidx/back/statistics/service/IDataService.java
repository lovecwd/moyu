package com.lidx.back.statistics.service;

import java.util.List;

import com.lidx.back.statistics.entity.FeedBack;

public interface IDataService {
	List<FeedBack> getBacks();
}
