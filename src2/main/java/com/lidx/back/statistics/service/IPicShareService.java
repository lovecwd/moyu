package com.lidx.back.statistics.service;

import java.util.Map;

import com.lidx.back.statistics.entity.PictureShare;

public interface IPicShareService {

	void savePicShare(PictureShare ps);
	
	Map<String, Integer> getSharePicChannel(String versionType);
}
