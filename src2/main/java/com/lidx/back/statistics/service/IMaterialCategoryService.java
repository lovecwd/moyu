package com.lidx.back.statistics.service;

import com.lidx.back.statistics.entity.MaterialCategory;

import java.util.List;

public interface IMaterialCategoryService {

	void saveMaterialCategory(MaterialCategory category) throws Exception;
	
	List<MaterialCategory>  getCategories(String deviceId, String versionType, String versionNum) ;
	
	List<MaterialCategory>  getCategories2(String sex, String versionType, String versionNum) ;

	void deleteCategory(int id);
	
	List<String> getCategoryNames();

	MaterialCategory getCategoryById(int id);

    void updateCategory(MaterialCategory materialCategory);
}
