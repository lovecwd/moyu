package com.lidx.back.statistics.service;

import java.util.List;

import com.lidx.back.statistics.entity.ClientRecord;

public interface IClientRecordService {
	void saveClientRecord(ClientRecord record);
	List<ClientRecord> getClientRecords();
	List<Object[]> getRecordsWithParams(String action, String versionType,
			String versionNumber,String from, String to,String online);
	List<Object[]> getSexPercentage(String versionType, String versionNumber,String from,String to,String online);
	Object[] getVideoRecordInfo(String versionType, String versionNumber,String from,String to,String online);
}
