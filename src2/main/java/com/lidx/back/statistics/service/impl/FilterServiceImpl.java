package com.lidx.back.statistics.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lidx.back.statistics.dao.IFilterDao;
import com.lidx.back.statistics.entity.Filter;
import com.lidx.back.statistics.service.IFilterService;
@Service
@Transactional
public class FilterServiceImpl implements IFilterService {

	@Autowired
	private IFilterDao filterDao;
	
	public void callUseFilter(Filter filter) {
		filterDao.updateFilterUseNum(filter);
	}

	public void callMakeFilter(Filter filter) {
		filterDao.updateFilterMakeNum(filter);
	}

	public List<Filter> getFilters(String versionType) {
		return filterDao.getFilters(versionType);
	}

	public void addFilter(Filter filter) {
		filterDao.addFilter(filter);
	}

}
