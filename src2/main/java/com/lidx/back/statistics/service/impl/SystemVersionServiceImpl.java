package com.lidx.back.statistics.service.impl;

import com.lidx.back.statistics.commons.Constants;
import com.lidx.back.statistics.dao.ISystemVersionDao;
import com.lidx.back.statistics.entity.SystemVersionConfig;
import com.lidx.back.statistics.rest.VersionBean;
import com.lidx.back.statistics.rest.app.AppVersionCheckBody;
import com.lidx.back.statistics.service.ISystemVersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
@Service
@Transactional
public class SystemVersionServiceImpl implements ISystemVersionService {

	@Autowired
	private ISystemVersionDao systemVersionDao;

	@Override
	public AppVersionCheckBody getVersionCheck(VersionBean versionBean) throws Exception {
		AppVersionCheckBody body = new AppVersionCheckBody();
		String latestVersionNum = systemVersionDao.getLatestVersionNum(versionBean.getVersionType());
		body.setLatestVersionNum(latestVersionNum);
		if (compareVersion(latestVersionNum, versionBean.getVersionNumber()) > 0) {
			body.setUpdateNeeded(true);
		} else {
			body.setUpdateNeeded(false);
		}
		return body;
	}

	public SystemVersionConfig getVersionConfig(VersionBean versionBean) throws Exception {
		SystemVersionConfig config = systemVersionDao.getConfig(versionBean);
		//StringUtils.replace("", ".", "");
		if(null == config){
			String latestVersionNum = systemVersionDao.getLatestVersionNum(versionBean.getVersionType());
			if(compareVersion(latestVersionNum,versionBean.getVersionNumber())>0){
				config = new SystemVersionConfig();
				config.setIsAvailable(Constants.APP_CAN_USE_HAVE_UPDATE);
				config.setVersionNum(versionBean.getVersionNumber());
				config.setVersionType(versionBean.getVersionType());
				config.setLatestVersionNum(latestVersionNum);
			}else{
				config = new SystemVersionConfig();
				config.setIsAvailable(Constants.APP_CAN_USE_NO_UPDATE);
			}
		}
		return config;
	}
	
	public static int compareVersion(String version1, String version2) throws Exception {  
	    if (version1 == null || version2 == null) {  
	        throw new Exception("compareVersion error:illegal params.");  
	    }  
	    String[] versionArray1 = version1.split("\\.");//注意此处为正则匹配，不能用"."；  
	    String[] versionArray2 = version2.split("\\.");  
	    int idx = 0;  
	    int minLength = Math.min(versionArray1.length, versionArray2.length);//取最小长度值  
	    int diff = 0;  
	    while (idx < minLength  
	            && (diff = versionArray1[idx].length() - versionArray2[idx].length()) == 0//先比较长度  
	            && (diff = versionArray1[idx].compareTo(versionArray2[idx])) == 0) {//再比较字符  
	        ++idx;  
	    }  
	    //如果已经分出大小，则直接返回，如果未分出大小，则再比较位数，有子版本的为大；  
	    diff = (diff != 0) ? diff : versionArray1.length - versionArray2.length;  
	    return diff;  
	}

	public List<SystemVersionConfig> getVersionConfigList(
			VersionBean versionBean) {
		return systemVersionDao.getVersionList(versionBean);
	}

	public void add(SystemVersionConfig versionConfig) {
		systemVersionDao.save(versionConfig);
	}

	public List<String> getVersionNumbers() {
		return systemVersionDao.getVersionNumbers();
	}



}
