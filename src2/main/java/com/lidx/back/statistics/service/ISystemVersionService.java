package com.lidx.back.statistics.service;

import com.lidx.back.statistics.entity.SystemVersionConfig;
import com.lidx.back.statistics.rest.VersionBean;
import com.lidx.back.statistics.rest.app.AppVersionCheckBody;

import java.util.List;

public interface ISystemVersionService {
	
	SystemVersionConfig getVersionConfig(VersionBean versionBean) throws Exception;

	List<SystemVersionConfig> getVersionConfigList(VersionBean versionBean);
	
	void add(SystemVersionConfig versionConfig);
	
	List<String> getVersionNumbers();

    AppVersionCheckBody getVersionCheck(VersionBean versionBean) throws Exception;
}
