package com.lidx.back.statistics.service.impl;

import com.google.common.collect.Lists;
import com.lidx.back.statistics.dao.ILatestMaterialDao;
import com.lidx.back.statistics.dao.IMaterialDao;
import com.lidx.back.statistics.entity.MaterialLatest;
import com.lidx.back.statistics.entity.MaterialPO;
import com.lidx.back.statistics.service.ILatestMaterialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Administrator on 2017/4/11.
 */
@Service
@Transactional
public class LatestMaterialServiceImpl implements ILatestMaterialService {

    @Autowired
    private ILatestMaterialDao latestMaterialDao;

    @Autowired
    private IMaterialDao materialDao;

    public List<MaterialLatest> findLatestMaterials() {
        return latestMaterialDao.getLatestMaterials();
    }

    public int[] getLatestMaterialIds(String versionType, String versionNumber) {

        List<MaterialLatest> list = findLatestMaterials();
        if (list != null && list.size() > 0) {
            int[] ids = new int[list.size()];
            for (int i = 0; i < list.size(); i++) {
                ids[i] = list.get(i).getMaterialId();
            }
            return ids;
        } else {
            //初始是获取所有类别中前10个素材添加到最新内容中，后续将每个类别中新增添加进最新中
            return null;
        }

    }

    public void deleteLatestMaterial(int id) {
        latestMaterialDao.deleteLatestMaterial(id);
    }

    @Transactional(readOnly = false,rollbackFor = Exception.class)
    public boolean initLatestMaterials(int size) {
        List<MaterialPO> materialPOS = materialDao.getMaterialsFrontXCategory(size);
        List<MaterialLatest> materialLatests = Lists.newArrayListWithExpectedSize(materialPOS.size());
        for(MaterialPO materialPO : materialPOS){
            materialLatests.add(new MaterialLatest(materialPO));
        }
        latestMaterialDao.deleteAll();
        latestMaterialDao.saveLatestMaterialList(materialLatests);
        return false;
    }
}
