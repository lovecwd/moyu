package com.lidx.back.statistics.service;

import com.lidx.back.statistics.entity.ClientInfo;

public interface IAppInitService {

	void saveClientInfo(ClientInfo info) throws Exception;
	
	boolean checkInfoIfExists(ClientInfo info);
	
	ClientInfo getClientInfoByDeviceId(String deviceId);
	
	String getClientSexInfoByDeviceId(String deviceId);
}
