package com.lidx.back.statistics.service;

public interface IDictionaryService {

	String getCodeByValue(String value);
	String getValueByCode(String code);
}
