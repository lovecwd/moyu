package com.lidx.back.statistics.service;

import java.util.List;

import com.lidx.back.statistics.commons.PageInfo;
import com.lidx.back.statistics.entity.Log;

public interface IlogService {

	List<Log> findLogList(String from,String to);

	PageInfo findLogPage(int pageSize, int pageNo, String from, String to);
}
