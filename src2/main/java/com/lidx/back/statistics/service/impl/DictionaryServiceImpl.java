package com.lidx.back.statistics.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lidx.back.statistics.dao.IDictionaryDao;
import com.lidx.back.statistics.service.IDictionaryService;
@Service
public class DictionaryServiceImpl implements IDictionaryService {

	@Autowired
	private IDictionaryDao dictionaryDao;
	
	public String getCodeByValue(String value) {
		// TODO Auto-generated method stub
		return dictionaryDao.getCodeByValue(value);
	}

	public String getValueByCode(String code) {
		// TODO Auto-generated method stub
		return dictionaryDao.getValueByCode(code);
	}

}
