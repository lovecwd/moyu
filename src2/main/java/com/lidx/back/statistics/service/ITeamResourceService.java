package com.lidx.back.statistics.service;

import com.lidx.back.statistics.commons.PageInfo;
import com.lidx.back.statistics.entity.TeamResource;

import java.util.List;

/**
 * Created by chen on 2017/2/17.
 */
public interface ITeamResourceService {

    void saveTeamResource(TeamResource teamResource);

    List<TeamResource> findTeamResourceList();

    PageInfo findTeamResourcePage(int pageNo,int pageSize,String resourceCategory);

    TeamResource getTeamResourceById(int id);

    void deleteTeamResource(int id);

    void updateTeamResource(TeamResource teamResource);
}
