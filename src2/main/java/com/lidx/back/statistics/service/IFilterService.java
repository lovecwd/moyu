package com.lidx.back.statistics.service;

import java.util.List;

import com.lidx.back.statistics.entity.Filter;

public interface IFilterService {
		void callUseFilter(Filter filter);
		
		void callMakeFilter(Filter filter);
		
		List<Filter> getFilters(String versionType);
		
		void addFilter(Filter filter);
}
