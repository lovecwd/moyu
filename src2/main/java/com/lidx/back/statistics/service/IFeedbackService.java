package com.lidx.back.statistics.service;

import java.util.List;

import com.lidx.back.statistics.commons.PageInfo;
import com.lidx.back.statistics.entity.FeedBack;

public interface IFeedbackService {

	void saveFeedback(FeedBack feedBack) throws Exception;
	
	List<FeedBack> getFeedBacks();
	
	PageInfo queryFeedbacks(int pageNo,int pageSize);
}
