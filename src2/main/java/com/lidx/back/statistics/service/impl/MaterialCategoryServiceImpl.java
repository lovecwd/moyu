package com.lidx.back.statistics.service.impl;

import com.lidx.back.statistics.dao.IDictionaryDao;
import com.lidx.back.statistics.dao.IMaterialCategoryDao;
import com.lidx.back.statistics.entity.MaterialCategory;
import com.lidx.back.statistics.service.IAppInitService;
import com.lidx.back.statistics.service.IMaterialCategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@Transactional
public class MaterialCategoryServiceImpl implements IMaterialCategoryService {

	protected static final Logger LOGGER = LoggerFactory.getLogger(MaterialCategoryServiceImpl.class);
	
	@Autowired
	private IMaterialCategoryDao categoryDao;
	@Autowired
	private IAppInitService appInitService;
	@Autowired
	private IDictionaryDao dictionaryDao;
	
	@Transactional(readOnly=false,rollbackFor = Exception.class)
	public void saveMaterialCategory(MaterialCategory category) throws Exception {
		categoryDao.insertMaterialCategory(category);
		categoryDao.clearSecondQuery();
	}

	@Transactional(readOnly=true)
	public List<MaterialCategory> getCategories(String deviceId,String versionType, String versionNum) {
		//ClientInfo info = appInitService.getClientInfoByDeviceId(deviceId);
        /*String sex = "";
        if(StringUtils.isNotBlank(deviceId)){
			sex = appInitService.getClientSexInfoByDeviceId(deviceId);
			if(sex == null){
				sex = "0";
			}
		}*/
        return categoryDao.findCategories(null, versionType, versionNum);
    }
	
	
	public void deleteCategory(int id) {
		categoryDao.deleteCategory(id);
		categoryDao.clearSecondQuery();
	}

	public List<String> getCategoryNames() {
		return categoryDao.getCategoryNames();
	}

	public MaterialCategory getCategoryById(int id) {
		return categoryDao.getCategoryById(id);
	}

	public void updateCategory(MaterialCategory materialCategory) {
		/*MaterialCategory mc = categoryDao.getCategoryById(materialCategory.getCategoryId());
		mc.setCategoryName(materialCategory.getCategoryName());
		mc.setCategoryOrder(materialCategory.getCategoryOrder());*/
		categoryDao.updateCategory(materialCategory);
	}

	public List<MaterialCategory> getCategories2(String sex,
			String versionType, String versionNum) {
		return categoryDao.findCategories(sex,versionType,versionNum);
	}

}
