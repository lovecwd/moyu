package com.lidx.back.statistics.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lidx.back.statistics.dao.IClientDao;
import com.lidx.back.statistics.entity.ClientInfo;
import com.lidx.back.statistics.entity.UserActionMsg;
import com.lidx.back.statistics.service.IClientService;
@Service
public class ClientServiceImpl implements IClientService {

	@Autowired
	private IClientDao clientDao;
	
	public void callCloseSound(ClientInfo clientInfo) {
		clientDao.updateCloseSoundNum(clientInfo);
	}

	public void callOpenFlashLight(ClientInfo clientInfo) {
		clientDao.updateOpenFlashLightNum(clientInfo);
	}

	public void callSwitchCamera(ClientInfo clientInfo) {
		clientDao.updateSwitchCameraNum(clientInfo);
	}

	public void callSwitchFilter(ClientInfo clientInfo) {
		clientDao.updateSwitchFilterNum(clientInfo);
	}

	public void callRecordVideo(ClientInfo clientInfo) {
		clientDao.updateRecordVideoTime(clientInfo);
	}

	public void callLinkShare(ClientInfo clientInfo) {
		clientDao.updateLinkShareNum(clientInfo);
	}

	public void callSaveVideo(ClientInfo clientInfo) {
		clientDao.updateSaveVideoNum(clientInfo);
	}

	public void callSavePicture(ClientInfo clientInfo) {
		clientDao.updateSavePictureNum(clientInfo);
	}

	public void callShareVideo(ClientInfo clientInfo) {
		clientDao.updateShareVideoNum(clientInfo);
	}

	public void callSharePic(ClientInfo clientInfo) {
		clientDao.updateSharePicNum(clientInfo);
	}

	public List<ClientInfo> getClientInfos(String versionType) {
		return clientDao.getClientInfos(versionType);
	}

	/*public UserActionMsg dealClientInfos(List<ClientInfo> infos) {
		UserActionMsg msg = new UserActionMsg();
		int maleNum = 0;
		int femaleNum = 1;
		int closeSoundNum = 0;
		int openFlashLightNum = 0;
		int swithCameraNum = 0;
		int switchFilterNum = 0;
		Long recordVideoAllTime = 0L;
	    int recordVideoCounts = 0;
	    int linkShareNum = 0;
	    int saveVideoNum = 0;
	    int savePicNum = 0;
	    //int sharePicNum = 0;
	    int shareVideoNum = 0;
		for(ClientInfo info : infos){
			if("1".equals(info.getSex())){
				maleNum += 1;
			}else if("0".equals(info.getSex())){
				femaleNum += 1;
			}
			closeSoundNum += info.getCloseSoundNum();
			openFlashLightNum += info.getOpenFlashLightNum();
			swithCameraNum += info.getSwitchCameraNum();
			switchFilterNum += info.getSwitchFilterNum();
			recordVideoAllTime += info.getRecordVideoTime();
			recordVideoCounts += info.getRecordVideoCounts();
			linkShareNum += info.getLinkShareNum();
			saveVideoNum += info.getSaveVideoNum();
			savePicNum += info.getSavePictureNum();
			//sharePicNum += info.getSharePictureNum();
		}
		msg.setMaleNum(maleNum);
		msg.setFemaleNum(femaleNum);
		msg.setCloseSoundNum(closeSoundNum);
		msg.setOpenFlashLightNum(openFlashLightNum);
		msg.setSwitchCameraNum(swithCameraNum);
		msg.setSwitchFilterNum(switchFilterNum);
		double avgRecordVideoTime;
		if(recordVideoCounts == 0){
			avgRecordVideoTime = 0;
		}else{
			avgRecordVideoTime = (recordVideoAllTime/recordVideoCounts);
		}
		 
		msg.setAvgRecordVideoTime(avgRecordVideoTime);
		msg.setLinkShareNum(linkShareNum);
		msg.setSaveVideoNum(saveVideoNum);
		msg.setSavePictureNum(savePicNum);
		//msg.setSharePictureNum(sharePicNum);
		return msg;
	}*/

}
