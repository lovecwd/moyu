package com.lidx.back.statistics.service;

import com.lidx.back.statistics.entity.MaterialLatest;

import java.util.List;

/**
 * Created by Administrator on 2017/4/11.
 */
public interface ILatestMaterialService {
    List<MaterialLatest> findLatestMaterials();

    int[] getLatestMaterialIds(String versionType, String versionNumber);

    void deleteLatestMaterial(int id);

    boolean initLatestMaterials(int size);
}
