package com.lidx.back.statistics.service;

import com.lidx.back.statistics.entity.MaterialPO;
import com.lidx.back.statistics.entity.MaterialUpdateRequestInfo;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface IMaterialService {

	List<MaterialPO> findAllMaterialList(String versionType, String versionNumber);

    //List<MaterialPO> findRecommendMaterials(String versionType, String versionNumber,String deviceId);

	void updateMaterialUpdateRequest(MaterialUpdateRequestInfo info);

	MaterialUpdateRequestInfo getRequestInfo();
	
	void updateMaterial(String versionType, String versionNumber) throws Exception;
	
	List<String> getMaterialNames(String versionType, String versionNumber);

    /*List<MaterialPO> findAllMaterialListBySex(String versionType,
            String versionNumber, String sex);*/
    List<MaterialPO> findAllMaterialListByCategoryId(String versionType,
                                                     String versionNumber, int categoryId);

    MaterialPO getMaterialById(int id);

    void deleteMaterial(int id) throws Exception;

    void addMaterial(MaterialPO m, String savePath) throws Exception;

	List<MaterialPO> findAllMaterialList(MaterialPO mp);

	void addHotMaterials(List<MaterialPO> hotMaterials,String versionType,String versionNumber) throws IOException;

    //void addHotMaterials(Map<String, List<MaterialPO>> hotMaterials,String versionType,String versionNumber,int number) throws IOException;

	void updateMaterial(MaterialPO materialPO);

	Map<String, String> getMaterialMd5s(String iconName, String materialName,
			String versionType, String versionNumber) throws Exception ;

	void cleanMaterialCache();

    void deleteMaterialsByIds(int[] id) throws Exception;

    void addMaterials(List<MaterialPO> materialPOS);

    //int[] getFrontXMaterials(int frontSize,String versionType,String versionNumber);
}
