package com.lidx.back.statistics.service;

import java.util.List;

import com.lidx.back.statistics.entity.MaterialCount;
import com.lidx.back.statistics.entity.MaterialDownload;
import com.lidx.back.statistics.entity.MaterialMakeVideo;

public interface IMaterialCountService {
	
	void callDownloadMaterial(MaterialDownload materialDownload);
	
	void callMakeVideoWithMaterial(MaterialMakeVideo makeVideo);
	
	void callPhotographWithMaterial(MaterialCount count );
	
	void callPicShareWithMaterial(MaterialCount count);
	
	void callVideoShareWithMaterial(MaterialCount count);
	
	List<MaterialCount> getCounts(String versionType);
}
