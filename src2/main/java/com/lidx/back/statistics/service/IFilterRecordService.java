package com.lidx.back.statistics.service;

import java.util.List;

import com.lidx.back.statistics.entity.FilterRecord;

public interface IFilterRecordService {
	void saveRecord(FilterRecord record);
	
	List<FilterRecord> getRecords();

	List<Object[]> getRecordsWithParams(String action, String versionType,
			String versionNumber,String from, String to,String online,String order);
	
}
