package com.lidx.back.statistics.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lidx.back.statistics.commons.PageInfo;
import com.lidx.back.statistics.dao.ILogDao;
import com.lidx.back.statistics.entity.Log;
import com.lidx.back.statistics.service.IlogService;

@Service
@Transactional(readOnly=true)
public class LogServiceImpl implements IlogService {

	@Autowired
	private ILogDao logDao;
	
	public List<Log> findLogList(String from, String to) {
		return logDao.getLogList(from, to);
	}

	public PageInfo findLogPage(int pageSize, int pageNo, String from, String to) {
		// TODO Auto-generated method stub
		return logDao.findLogPage(pageSize,pageNo,from,to);
	}

}
