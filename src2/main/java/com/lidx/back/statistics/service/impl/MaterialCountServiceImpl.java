package com.lidx.back.statistics.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lidx.back.statistics.dao.IMaterialCountDao;
import com.lidx.back.statistics.entity.MaterialCount;
import com.lidx.back.statistics.entity.MaterialDownload;
import com.lidx.back.statistics.entity.MaterialMakeVideo;
import com.lidx.back.statistics.service.IMaterialCountService;
@Service
public class MaterialCountServiceImpl implements IMaterialCountService {

	@Autowired
	private IMaterialCountDao materialCountDao;
	
	public void callDownloadMaterial(MaterialDownload materialDownload) {
		materialDownload.setDownloadTime(new Date());
		//materialCountDao.updateMaterialDownloadCount(count);
		materialCountDao.saveMaterialDownload(materialDownload);
	}

	public void callMakeVideoWithMaterial(MaterialMakeVideo makeVideo) {
		makeVideo.setMakeVideoTime(new Date());
		materialCountDao.insertMaterialMakeVideo(makeVideo);
	}

	public void callPhotographWithMaterial(MaterialCount count) {
		materialCountDao.updatePhotographNum(count);
	}

	public void callVideoShareWithMaterial(MaterialCount count) {
		materialCountDao.updateVideoShareNum(count);
	}

	public void callPicShareWithMaterial(MaterialCount count) {
		materialCountDao.updatePicShareNum(count);
	}

	public List<MaterialCount> getCounts(String versionType) {
		return materialCountDao.getCounts(versionType);
	}
	

}
